/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.sql.Date;
import java.util.ArrayList;
import javax.ejb.EJBException;
import javax.ejb.Remote;

/**
 *
 * @author JamesAlan
 */
@Remote
public interface ACMECustomerSessionBeanRemote
{
    void removeBean();
    
    String addAccount(String C_ID) throws EJBException;

    boolean depositMoney(String C_ID, String accNum, double amt) throws EJBException;

    boolean withdrawMoney(String C_ID, String accNum, double amt) throws EJBException;    

    double getAccountBalance(String accNum) throws EJBException;
    
    ArrayList<String> getTransactions(String accNum, Date startDate, Date endDate);

    String applyAuthenticationResult(String C_ID, String PIN) throws EJBException;
    
    void pingMe();
}
