
package data_access;

/**
 * Floating class containing static variables common to the ACME banking system
 * 
 * @author JamesAlan
 */
public class DataConstants
{
    public static final char
       CUSTOMER_TYPE = 'C',
       ACCOUNT_TYPE = 'A',
       HLACCOUNT_TYPE = 'H',
       EMPLOYEE_TYPE = 'E',
       TRANSACTION_TYPE = 'T';
    
    public static final int
       EMPLOYEE_OPERATION_LIMIT = 10,
       CLIENT_LOGIN_LIMIT = 5;
    
    public static final int
        MAX_CUSTOMER_ACCOUNTS = 2,
        MIN_DEPOSITS_TO_OPEN_HOMELOAN = 3;
    
    public static final String
       IDTYPE_SEVEN = "%c%07d",
       IDTYPE_TEN = "%c%010d";
}
