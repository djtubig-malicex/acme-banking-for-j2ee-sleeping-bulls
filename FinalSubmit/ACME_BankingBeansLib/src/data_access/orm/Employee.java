package data_access.orm;

import data_access.DataConstants;
import java.sql.Date;
import java.io.Serializable;
import java.util.GregorianCalendar;

/**
 *
 * @author Hubert, JamesAlan, Chris
 */
public class Employee implements Serializable
{
    private int E_ID;
    private String firstName, lastName, address, PIN;
    private Date dateOfBirth, dateOfEmployment;
    
    /**
     * Constructor used for creating a new Employee
     * 
     * @param firstName First name of employee
     * @param lastName Surname of employee
     * @param dateOfBirth Birth date of employee
     * @param address Contact address of employee
     * @param PIN password
     */
    Employee(String firstName, String lastName, Date dateOfBirth, String address,
        Date dateOfEmployment, String PIN)
    {
        this.E_ID = -1;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.dateOfEmployment = dateOfEmployment;
        this.PIN = PIN;
    }
    
    /**
     * Constructor for a pre-existing Employee profile
     * 
     * @param E_ID Employee ID number
     * @param firstName First name of employee
     * @param lastName Surname of employee
     * @param dateOfBirth Birth date of employee
     * @param address Contact address of employee
     */
    Employee(int E_ID, String firstName, String lastName, Date dateOfBirth, 
        String address, Date dateOfEmployment, String PIN) throws IllegalAccessException
    {
        if (E_ID < 1) throw new IllegalAccessException();
        this.E_ID = E_ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.dateOfEmployment = dateOfEmployment;
        this.PIN = PIN;
    }
    
    /**
     * Returns the text-form ID of the Employee, defaulting with
     * prepended 'E' and possibly padded zeroes.
     * @return String form of Employee ID
     */
    public String getID()
    {
        return String.format(DataConstants.IDTYPE_SEVEN,
            DataConstants.EMPLOYEE_TYPE, E_ID);
    }
    
    /**
     * Returns first name of Employee.
     * @return First name as a String
     */
    public String getFirstName() 
    {
        return firstName;
    }
    
    /**
     * Returns last name of Employee.
     * @return Last name as a String
     */
    public String getLastName() 
    {
        return lastName;
    }
    
    /**
     * Returns the address of Employee.
     * @return address as a String
     */
    public String getAddress()
    {
        return address;
    }
    
    /**
     * Returns birth date of Employee.
     * @return date of birth
     */
    public Date getDateOfBirth()
    {
        return dateOfBirth;
    }
    
    /**
     * Returns commencing date of an Employee
     * @return date of employment
     */
    public Date getDateOfEmployment()
    {
        return dateOfEmployment;
    }
    /**
     * Get the numeric suffix of an employee
     * @return integer form of employee ID
     */
    public int getNumericID() 
    {
        return E_ID;
    }
    
    /**
     * Get password tied to the Employee.
     * NOTE: It is in plaintext
     * 
     * @return password string
     */
    public String getPIN()
    {
        return PIN;
    }
    
    /**
     * Custom toString implementation
     * 
     * @return "getID(),getFirstName(),getLastName(),getDateOfBirth(),getAddress()" 
     *      as a string
     */
    @Override
    public String toString()
    {
        return String.format("%s,%s,%s,%s,%s,%s,%s",
            getID(),
            getFirstName(),
            getLastName(),
            getAddress(),
            getDateOfBirth(),
            getDateOfEmployment(),
            getPIN()
        );
    }
    
    /**
     * Set unique ID of Employee provided it is a newly created profile.
     * 
     * @param generatedID Generated employee ID from database
     * @throws IllegalAccessException 
     */
    public void setID(int generatedID) throws IllegalAccessException
    {
        if (E_ID > 0 || generatedID < 1) throw new IllegalAccessException();
        this.E_ID = generatedID;
    }
    
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public void setDateOfBirth(Date dateOfBirth) 
    {
        this.dateOfBirth = dateOfBirth;
    }
    
    /**
     * Employee parser from string.
     * 
     * @param representation as per toString()
     * @return Employee object
     */
    public static Employee parseEmployee(String representation) {
        try 
        {
            String tokens[] = representation.split(","),
                   dateToken[] = tokens[3].split("-"),
                   empDateToken[] = tokens[5].split("-");
            
            return DataFactory.buildEmployee(
                    tokens[0],
                    tokens[1], 
                    tokens[2], 
                    new Date(new GregorianCalendar(
                            Integer.parseInt(dateToken[0]), 
                            Integer.parseInt(dateToken[1]),
                            Integer.parseInt(dateToken[2]))
                        .getTimeInMillis()),
                    tokens[4],
                    new Date(new GregorianCalendar(
                            Integer.parseInt(empDateToken[0]),
                            Integer.parseInt(empDateToken[1]),
                            Integer.parseInt(empDateToken[2]))
                        .getTimeInMillis()),
                    tokens[6]
            );
        }
        
        catch (IllegalAccessException ex) 
        {
            return null;
        }
    }
}
