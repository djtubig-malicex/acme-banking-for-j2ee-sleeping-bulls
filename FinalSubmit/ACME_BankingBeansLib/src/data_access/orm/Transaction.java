package data_access.orm;

import data_access.DataConstants;
import java.io.Serializable;
import java.sql.Date;

/**
 * Object representation of Transactions.
 * All transactions must have an Authorising Employee, as well as the associated
 * account which it relates to.  No information is mutable after creation.
 * 
 * @author JamesAlan, Chris
 */
public class Transaction implements Serializable 
{
    private int T_ID, authEmpID, accNum;
    //private Employee authorisingEmployee;
    //private Account associatedAccount;
    private double transactionAmt;
    private String transDesc;
    private Date transDate;
    
    /*** Constructors ***/
    
    /**
     * Constructor for a new transaction to be written.
     * 
     * @param E_ID Numeric employee ID authorising transaction
     * @param accID Numeric account ID the transaction corresponds to
     * @param amt Exact value of funds transferred in the transaction
     * @param description Transaction details
     * @param date Date of transaction
     */
    Transaction(int E_ID, int accID, double amt, String description, Date date)
    {
        //this.authorisingEmployee = null;
        this.T_ID = -1;
        this.authEmpID = E_ID;
        //this.associatedAccount = null;
        this.accNum = accID;
        this.transactionAmt = amt;
        this.transDesc = description;
        this.transDate = date;
    }
    /**
     * Constructor for a new transaction to be written.
     * 
     * @param E_ID Numeric employee ID authorising transaction
     * @param accID Numeric account ID the transaction corresponds to
     * @param amt Exact value of funds transferred in the transaction
     * @param description Transaction details
     */
    Transaction(int E_ID, int accID, double amt, String description)
    {
        this(E_ID, accID, amt, description, new Date(System.currentTimeMillis()));        
    }
    
    Transaction(int accID, double amt, String description)
    {
        //this.authorisingEmployee = null;
        this.T_ID = -1;
        //this.associatedAccount = null;
        this.authEmpID = 2; //hax
        this.accNum = accID;
        this.transactionAmt = amt;
        this.transDesc = description;
        this.transDate = new Date(System.currentTimeMillis());   
    }
    
    /**
     * Constructor for an existing transaction
     * 
     * @param T_ID Correspondjng Transaction ID
     * @param E_ID Numeric ID of authorising Employee
     * @param accID Account ID relating to this Transaction
     * @param amt Exact amount of funds transferred in the transaction
     * @param description Transaction details
     * @param date Date of transaction
     */
    Transaction(int T_ID, int E_ID, int accID, double amt,
        String description, Date date) throws IllegalAccessException
    {
        if (T_ID < 1) throw new IllegalAccessException();
        this.T_ID = T_ID;
        this.authEmpID = E_ID;
        this.accNum = accID;
        this.transactionAmt = amt;
        this.transDesc = description;
        this.transDate = date;
    }
    
    /*** Accessors ***/
    
    public String getID()
    {
        return String.format(DataConstants.IDTYPE_TEN, DataConstants.TRANSACTION_TYPE, T_ID);
    }
    public int getNumericID()
    {
        return T_ID;
    }
    public String getAuthEmpID()
    {
        return String.format(DataConstants.IDTYPE_SEVEN, DataConstants.EMPLOYEE_TYPE, authEmpID);
    }
    public int getNumericAuthEmpID()
    {
        return authEmpID;
    }
    public String getAccID()
    {
        return String.format(DataConstants.IDTYPE_SEVEN, DataConstants.ACCOUNT_TYPE, accNum);
    }
    public int getNumericAccID()
    {
        return accNum;
    }
    public Date getTransDate()
    {
        return transDate;
    }

    public String getTransDesc()
    {
        return transDesc;
    }

    public double getTransactionAmt()
    {
        return transactionAmt;
    }
    
    @Override
    /** Custom toString implementation 
     * 
     * @return "getID(),getAuthEmpID(),getAccID(),getTransDate(),getTransDesc(), getTransactionAmt()"
     *      as a String 
     */
    public String toString()
    {
        return String.format("%s,%s,%s,%s,%s,%s",
            getID(),
            getAuthEmpID(),
            getAccID(),
            getTransDate(),
            getTransDesc(),
            getTransactionAmt()
        );
    }
    
    /**
     * Set the Transaction ID, provided it is an empty Transaction object
     * @param generatedID Generated numeric ID of transaction from database
     * @throws IllegalAccessException 
     */
    public void setID(int generatedID) throws IllegalAccessException
    {
        if (T_ID > 0 || generatedID < 1) throw new IllegalAccessException();
        T_ID = generatedID;
    }
}
