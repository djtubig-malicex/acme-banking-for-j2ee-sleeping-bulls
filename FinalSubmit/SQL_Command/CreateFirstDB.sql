-- DB Name: ACME_DB
-- IMPORTANT: USERNAME AND PASSWORD MUST BE SET TO ACME
-- map to acmeDBConnectionPool and JNDI: jdbc/acmeDBDatasource

drop table ACME.TRANSACTIONS;
drop table ACME.ACCOUNTS;
drop table ACME.CUSTOMERS;
drop table ACME.EMPLOYEES;
drop schema ACME restrict;

create schema ACME;

create table ACME.Employees
(
   E_ID int not null primary key generated always as identity (start with 1, increment by 1),
   FirstName varchar(64),
   LastName varchar(64),
   Address varchar(128),
   DOB date,
   EmploymentDate date,
   PIN varchar(32) not null
);

create table ACME.Customers
(
   C_ID int not null primary key generated always as identity (start with 1, increment by 1),
   FirstName varchar(64),
   LastName varchar(64),
   Address varchar(128),
   DOB date,
   PIN varchar(32) not null
);

create table ACME.Accounts
(
   AccNum int not null primary key generated always as identity (start with 1, increment by 1),
   C_ID int, 
   Balance double,
   foreign key (C_ID) references ACME.Customers(C_ID)
);

create table ACME.Transactions
(
   T_ID int not null primary key generated always as identity (start with 1, increment by 1),
   E_ID int,
   AccNum int,
   Amount double,
   Description varchar(12),
   T_Time date,
   foreign key (E_ID) references ACME.Employees(E_ID),
   foreign key (AccNum) references ACME.Accounts(AccNum)
);


INSERT INTO ACME.EMPLOYEES(FIRSTNAME, LASTNAME, DOB, ADDRESS, EMPLOYMENTDATE, PIN) VALUES ('Chris', 'White', '1989-02-28' , '42 Someplace Rd', '2013-06-06', '1234');
INSERT INTO ACME.EMPLOYEES(FIRSTNAME, LASTNAME, DOB, ADDRESS, EMPLOYMENTDATE, PIN) VALUES ('James Alan', 'Nguyen', '1990-08-03' , '626 Yuleneva-findami Street', '2013-06-06', '666');
INSERT INTO ACME.EMPLOYEES(FIRSTNAME, LASTNAME, DOB, ADDRESS, EMPLOYMENTDATE, PIN) VALUES ('Hubert', 'Wu', '1988-01-01' , '123 evergreen terrace', '2013-06-06', '4321');