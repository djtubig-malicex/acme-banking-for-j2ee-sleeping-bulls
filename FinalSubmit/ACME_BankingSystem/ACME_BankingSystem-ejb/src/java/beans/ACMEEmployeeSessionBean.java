package beans;

import data_access.orm.Customer;
import data_access.orm.Employee;
import java.sql.Date;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.EJBAccessException;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;

/**
 * 
 * @author JamesAlan, Hubert
 */
@Stateful
@StatefulTimeout(value = 1, unit = TimeUnit.MINUTES)
public class ACMEEmployeeSessionBean implements ACMEEmployeeSessionBeanRemote
{
    @EJB
    private ACMEPersistenceBeanLocal acmePersistenceBean;
    
//    @Resource(lookup = "jdbc/acmeDBDatasource")
//    private DataSource dataSource;
//    
//    private Connection connection;

    private int operations;
    
    private Employee currentEmployee;
    
    @PostConstruct
    public void initialize() 
    {
        operations = 0;
    }

    @PreDestroy
    public void close()
    {
        operations = 0;
        currentEmployee = null;
    }
    
    /**
     * Get the number of operations thus far for the lifetime of the session
     * @return operation counter
     */
    @Override
    public int getOperations()
    {
       return operations;
    }
    
//    @Override
//    public void performOperations()
//    {
//       operations++;
//    }
    
    @Remove
    @Override
    public void removeBean() 
    {
        System.err.println("Employee Bean removed");
    }

    @Override
    public String addCustomer(String firstname, String lastname, Date dob,
        String address, String PIN) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.addCustomer(firstname, lastname, dob, address, PIN);
    }

    @Override
    public String addAccount(String C_ID) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        try 
        {
            return acmePersistenceBean.addAccount(C_ID);
        }
        
        catch (IllegalAccessException ex) 
        {
            Logger.getLogger(ACMEEmployeeSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    @Override
    public String addEmployee(String firstname, String lastname, Date dob, 
        String address, Date doe, String PIN) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.addEmployee(firstname, lastname, dob, 
            address, doe, PIN);
    }

    @Override
    public void updateCustomer(String C_ID, String firstname, String lastname,
        Date dob, String address, String PIN) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        acmePersistenceBean.updateCustomer(C_ID, firstname, lastname, dob,
            address, PIN);
    }

    @Override
    public void updateEmployee(String E_ID, String firstname, String lastname,
        Date dob, String address, Date doe, String PIN) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        acmePersistenceBean.updateEmployee(E_ID, firstname, lastname, dob,
            address, doe, PIN);
    }

    @Override
    public void deleteCustomer(String C_ID) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        acmePersistenceBean.deleteCustomer(C_ID);
    }

    @Override
    public void deleteEmployee(String E_ID) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        acmePersistenceBean.deleteEmployee(E_ID);
    }

    @Override
    public ArrayList<String> getAllCustomers() throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.getAllCustomers();
    }

    @Override
    public ArrayList<String> getAllEmployees() throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.getAllEmployees();
    }

    @Override
    public ArrayList<String> getAllAccounts() throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.getAllAccounts();
    }

    @Override
    public ArrayList<String> getCustomerAccounts(String C_ID) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.getCustomerAccounts(C_ID);
    }
    
    /**
     * Returns all recorded Transactions.
     * NOTE: !!!DO NOT!!! USE ON A PRODUCTION SYSTEM.  THIS IS FOR DEBUGGING ONLY
     * @return ArrayList of Transactions 
     */
    @Override
    public ArrayList<String> getAllTransactions() throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.getTransactionsBetweenDate(
                new Date(0), new Date(System.currentTimeMillis()));
    }

    @Override
    public ArrayList<String> getTransactions(String C_ID, String accNum, 
        Date startDate, Date endDate) throws EJBAccessException 
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.getTransactionsByAccount(C_ID, accNum, startDate, endDate);
    }
    
    

    @Override
    public boolean depositMoney(String C_ID, String accNum, double amt) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.depositMoney(
            currentEmployee.getID(), C_ID, accNum, amt);
    }

    @Override
    public boolean withdrawMoney(String C_ID, String accNum, double amt) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.withdrawMoney(
            currentEmployee.getID(), C_ID, accNum, amt);
    }

    @Override
    public String readCustomer(String C_ID, String pin) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        // not incrementing operation since we're verifying customer details
        Customer c = acmePersistenceBean.getCustomer(C_ID, pin);
        
        if (c == null) return "";
        else return c.toString();
    }
    
    

    @Override
    public double getAccountBalance(String C_ID, String accNum) throws EJBAccessException
    {
        if (currentEmployee == null) throw new EJBAccessException("Not Authenticated");
        
        operations++;
        return acmePersistenceBean.getAccountBalance(C_ID, accNum);
    }

    @Override
    public String applyAuthenticationResult(String E_ID, String PIN)
    {   
        try
        {
            currentEmployee = acmePersistenceBean.getEmployee(E_ID, PIN);
        }
        
        catch (Exception e)
        {
            System.err.println(e.getMessage());
            return "Unauthorised or Invalid Employee";
        }
        
        return String.format("%s %s", currentEmployee.getFirstName(), currentEmployee.getLastName());
    }
    
    /**
     * Helper method to keep session bean alive.
     */
    @Override
    public void pingMe()
    {
        System.out.println("PING");
    }
}
