
   COSC2353/2354 - Electronic Commerce and Enterprise Systems
   ACME Banking System Project (Sem 2 2013)
   
   s3235716 - James Alan Nguyen
   s3370045 - Hongtao "Hubert" Wu
   s3233820 - Christopher Adrian White

   
   
Project repository:
===================
https://bitbucket.org/djtubig-malicex/acme-banking-for-j2ee-sleeping-bulls


About
=====

This is the ACME Banking system written in J2EE with Glassfish Server 4.0.
Developed using NetBeans IDE 7.3.1


Changelog
=========

18th Oct 2013
-Second subsystem added (JPA, REST, JSF w/ PrimeFaces and JMS communication layers)

20th Aug 2013
-Initial release with console client



Information TBW


