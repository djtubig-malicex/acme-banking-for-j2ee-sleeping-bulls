/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JamesAlan
 */
@Entity
@Table(name = "CUSTOMERINFO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Customerinfo.findAll", query = "SELECT c FROM Customerinfo c"),
    @NamedQuery(name = "Customerinfo.findByCId", query = "SELECT c FROM Customerinfo c WHERE c.cId = :cId"),
    @NamedQuery(name = "Customerinfo.findByPhone", query = "SELECT c FROM Customerinfo c WHERE c.phone = :phone"),
    @NamedQuery(name = "Customerinfo.findByEmail", query = "SELECT c FROM Customerinfo c WHERE c.email = :email"),
    @NamedQuery(name = "Customerinfo.findByCompanyname", query = "SELECT c FROM Customerinfo c WHERE c.companyname = :companyname"),
    @NamedQuery(name = "Customerinfo.findByCompanyaddr", query = "SELECT c FROM Customerinfo c WHERE c.companyaddr = :companyaddr"),
    @NamedQuery(name = "Customerinfo.findByEmploytype", query = "SELECT c FROM Customerinfo c WHERE c.employtype = :employtype"),
    @NamedQuery(name = "Customerinfo.findByHoursperweek", query = "SELECT c FROM Customerinfo c WHERE c.hoursperweek = :hoursperweek"),
    @NamedQuery(name = "Customerinfo.findByJobtitle", query = "SELECT c FROM Customerinfo c WHERE c.jobtitle = :jobtitle"),
    @NamedQuery(name = "Customerinfo.findBySalary", query = "SELECT c FROM Customerinfo c WHERE c.salary = :salary"),
    @NamedQuery(name = "Customerinfo.findByEmploymentdate", query = "SELECT c FROM Customerinfo c WHERE c.employmentdate = :employmentdate"),
    @NamedQuery(name = "Customerinfo.findByPreferredcontact", query = "SELECT c FROM Customerinfo c WHERE c.preferredcontact = :preferredcontact")})
public class Customerinfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "C_ID")
    private Integer cId;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 10)
    @Column(name = "PHONE")
    private String phone;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 64)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 32)
    @Column(name = "COMPANYNAME")
    private String companyname;
    @Size(max = 80)
    @Column(name = "COMPANYADDR")
    private String companyaddr;
    @Column(name = "EMPLOYTYPE")
    private Character employtype;
    @Column(name = "HOURSPERWEEK")
    private Integer hoursperweek;
    @Size(max = 32)
    @Column(name = "JOBTITLE")
    private String jobtitle;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SALARY")
    private Double salary;
    @Column(name = "EMPLOYMENTDATE")
    @Temporal(TemporalType.DATE)
    private Date employmentdate;
    @Column(name = "PREFERREDCONTACT")
    private Character preferredcontact;
    @OneToMany(mappedBy = "cId", cascade={CascadeType.PERSIST},fetch=FetchType.LAZY)
    private Collection<Homeloanaccounts> homeloanaccountsCollection;

    public Customerinfo() {
    }

    public Customerinfo(Integer cId) {
        this.cId = cId;
    }

    public Integer getCId() {
        return cId;
    }

    public void setCId(Integer cId) {
        this.cId = cId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getCompanyaddr() {
        return companyaddr;
    }

    public void setCompanyaddr(String companyaddr) {
        this.companyaddr = companyaddr;
    }

    public Character getEmploytype() {
        return employtype;
    }

    public void setEmploytype(Character employtype) {
        this.employtype = employtype;
    }

    public Integer getHoursperweek() {
        return hoursperweek;
    }

    public void setHoursperweek(Integer hoursperweek) {
        this.hoursperweek = hoursperweek;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Date getEmploymentdate() {
        return employmentdate;
    }

    public void setEmploymentdate(Date employmentdate) {
        this.employmentdate = employmentdate;
    }

    public Character getPreferredcontact() {
        return preferredcontact;
    }

    public void setPreferredcontact(Character preferredcontact) {
        this.preferredcontact = preferredcontact;
    }

    @XmlTransient
    public Collection<Homeloanaccounts> getHomeloanaccountsCollection() {
        return homeloanaccountsCollection;
    }

    public void setHomeloanaccountsCollection(Collection<Homeloanaccounts> homeloanaccountsCollection) {
        this.homeloanaccountsCollection = homeloanaccountsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cId != null ? cId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customerinfo)) {
            return false;
        }
        Customerinfo other = (Customerinfo) object;
        if ((this.cId == null && other.cId != null) || (this.cId != null && !this.cId.equals(other.cId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.Customerinfo[ cId=" + cId + " ]";
    }
    
}
