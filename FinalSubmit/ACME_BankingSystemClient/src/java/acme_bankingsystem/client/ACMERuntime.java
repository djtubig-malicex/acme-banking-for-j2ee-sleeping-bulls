package acme_bankingsystem.client;

import beans.ACMELoginBeanRemote;
import data_access.DataConstants;
import java.io.Console;
import java.util.Scanner;
import javax.ejb.EJB;
import javax.ejb.NoSuchEJBException;

/**
 * Main application driver for ACME Banking System console client
 * @author JamesAlan, Chris
 */
public class ACMERuntime 
{
    private static ACMERuntime instance;
    
    @EJB
    private static ACMELoginBeanRemote loginBean;
    
//    @EJB
//    private static ACMEEmployeeSessionBeanRemote employeeBean;
//    
//    @EJB
//    private static ACMECustomerSessionBeanRemote customerBean;
    
    private char userType;
    private String currentID, currentPIN;
    
    private ACMERuntime()
    {
        // singleton
    }    
   
    public static ACMERuntime getInstance()
    {
        if (instance == null)
            instance = new ACMERuntime();
        
        return instance;
    }
    
    /** 
     * Accepts user input from console and attempts to authenticate via 
     * ACMEEmployeeSessionBeanRemote
     * 
     * @return true if the client was able to authenticate, false otherwise
     */
    public void auth()
    {
        Scanner in = new Scanner(System.in);
        Console con = System.console();
        String inputID, inputPIN;
        boolean userLogin;
        int attempts = 0;
        
        // Init super-awesome demoscene-style logo
        System.out.println("   _____  _________     _____  ___________");
        System.out.println("  /  _  \\ \\_   ___ \\   /     \\ \\_   _____/");
        System.out.println(" /  /_\\  \\/    \\  \\/  /  \\ /  \\ |    __)_"); 
        System.out.println("/    |    \\     \\____/    Y    \\|        \\");  
        System.out.println("\\____|__  /\\______  /\\____|__  /_______  /");
        System.out.println("         \\/        \\/         \\/        \\/"); 
        System.out.println("             Welcome to ACME Banking System.\n");
        
        while (attempts < DataConstants.CLIENT_LOGIN_LIMIT)
        {
            // Read employee authentication details
            System.out.print("\nPlease enter your Employee ID: ");
            while(!(inputID = in.nextLine()
                    .trim()
                    .toUpperCase())
                .startsWith(Character.toString(DataConstants.EMPLOYEE_TYPE)))
            {
                // Only actioned if invalid Employee ID is issued
                System.out.printf("Not a valid Employee ID. %d of %d attempts.\n",
                    ++attempts, DataConstants.CLIENT_LOGIN_LIMIT
                );
                
                // Keep track of invvalid attempts
                if (attempts == DataConstants.CLIENT_LOGIN_LIMIT) return;
                else System.out.print("Please enter your Employee ID: ");
            }
            
            System.out.print("Please enter your PIN: ");
            inputPIN = (con != null) ? 
                new String(con.readPassword()) :  // conceal input if possible
                in.nextLine();

            // Attempts to authenticate via ACMEEmployeeSessionBeanRemote
            userLogin = loginBean.login(inputID, inputPIN);
            
            if (userLogin)
            {
                currentID = inputID;
                currentPIN = inputPIN;
                
                System.out.println("You are now logged in!");
                //employeeBean = (ACMEEmployeeSessionBeanRemote) lookupAuthBeanRemote(EMPLOYEE_LOOKUP);
                
                switch(inputID.charAt(0))
                {
                    case DataConstants.EMPLOYEE_TYPE: // fallthrough deliberate
                    case DataConstants.CUSTOMER_TYPE:
                        userType = inputID.charAt(0);
                        return;
                        
                    default:
                        //should not go here
                        System.err.println("WARNING: ID SHOULD NOT HAVE AUTHENTICATED");
                }
            }
            else
            {
                System.out.printf("Access Denied. %d of %d attempts.\n", 
                    ++attempts, DataConstants.CLIENT_LOGIN_LIMIT);
            }   
        }   
    }
    
    /**
     * Entry point
     * @param args 
     */
    public static void main(String[] args) 
    {
        // Launch ACMERuntime
        ACMERuntime client = ACMERuntime.getInstance();
        
        // authenticate client session 
        client.auth();
        
        // display menu
        client.startMenu();
        
        /**System.err.println("Menu needs to be implemented!\nTest drivers are found in the Test package.");
        JOptionPane.showMessageDialog(null, 
                "Menu needs to be implemented!\nTest drivers are found in the Test package.",
                "ACME Banking System", JOptionPane.PLAIN_MESSAGE);
        **/
        
        System.out.println("Terminating ACME client...");
    }
    
    /**
     * Start Menu initiation for the event loop defined in their classes
     */
    private void startMenu()
    {
        try
        {
            switch (userType)
            {
                case DataConstants.CUSTOMER_TYPE:
                    //customerBean.applyAuthenticationResult(currentID, currentPIN);
                    //new CustomerMenu(this, customerBean).startMenu();
                    new CustomerMenu(currentID, currentPIN).startMenu();
                    break;

                case DataConstants.EMPLOYEE_TYPE:
                    //employeeBean.applyAuthenticationResult(currentID, currentPIN);
                    //new EmployeeMenu(this, employeeBean).startMenu();
                    new EmployeeMenu(currentID, currentPIN).startMenu();
                    break;

                default:
                    break;
            }
        }
        
        // Prompt this if session bean at any time, is no longer usable
        catch (NoSuchEJBException ex)
        {
            System.err.println("Your session has timed out and expired. Please restart the applciation.");
        }    
    }
    
}
