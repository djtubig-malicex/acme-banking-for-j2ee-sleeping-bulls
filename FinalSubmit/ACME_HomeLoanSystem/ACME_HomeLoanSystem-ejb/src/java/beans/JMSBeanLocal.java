/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.ArrayList;
import javax.ejb.Local;

/**
 *
 * @author Hubert
 */
@Local
public interface JMSBeanLocal {
    public boolean login(String CID, String pw);
    public ArrayList<String> getCustomerAccounts(String CID);
    public boolean requestForHLAccountCreation(String CID);
    public boolean repayment(String customerID, String accountID, String homeloanID, double amount);
    public double totalSavingsAccountsAmount();
}
