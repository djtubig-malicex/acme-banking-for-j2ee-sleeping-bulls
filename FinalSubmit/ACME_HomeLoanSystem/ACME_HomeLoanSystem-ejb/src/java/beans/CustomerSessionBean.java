/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import jpa.Customerinfo;
import jpa.Homeloanaccounts;
import data_access.orm.Account;
import java.util.Collection;

/**
 *
 * @author Hubert
 */
@Stateful
public class CustomerSessionBean implements CustomerSessionBeanRemote {
    @EJB
    private JMSBeanLocal jmsBean;
    
    @EJB
    private HomeLoanPersistenceBeanRemote homeLoanPersistenceBean;
    
    private String
            id, password;
    
    @Override
    public boolean login(String CID, String pw) 
    {
        System.out.println("Reach CustomerSessionBean!!!");
        if(jmsBean.login(CID, pw))
        {
            this.id = CID;
            this.password = pw;
            return true;
        }
        
        return false;
    }

    @Override
    public boolean openNewHomeLoan(Customerinfo cust, double amtBorrow) 
    {
        System.out.println("Opening new homeloan for " + cust.getCId() + " with amount borrowed $" + amtBorrow);
        return homeLoanPersistenceBean.createHomeLoanAccount(cust, amtBorrow);
    }

    @Override
    public Customerinfo getCustomerInfo(String id) 
    {
        System.out.println("Obtaining customer info...");
        return homeLoanPersistenceBean.getCustomerInfo(Integer.parseInt(id.substring(1)));
    }

    @Override
    public boolean setCustomerInfo(Customerinfo cust)
    {
        return homeLoanPersistenceBean.setCustomerInfo(cust);
    }


    @Override
    public Collection<Homeloanaccounts> getHomeLoanAccounts(String CID) {
        return homeLoanPersistenceBean.getHomeLoanAccounts(CID);
    }

    @Override
    public boolean isPaidOff(String hlAcc) {
        return homeLoanPersistenceBean.checkHomeLoanPaidOff(Integer.parseInt(hlAcc.substring(1)));
    }

    @Override
    public void repayMoney(String cid, String accNum, String hlAcc, double amount) {
        System.out.println("TEST: "+cid+ " "+accNum+" "+hlAcc+" "+amount);
        homeLoanPersistenceBean.repayMoney(Integer.parseInt(cid.substring(1)), Integer.parseInt(accNum.substring(1)),
                Integer.parseInt(hlAcc.substring(1)), amount);
    }
    

}
