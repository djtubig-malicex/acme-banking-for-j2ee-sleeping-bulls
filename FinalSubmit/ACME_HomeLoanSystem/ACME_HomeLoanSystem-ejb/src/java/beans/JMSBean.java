/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author Hubert
 */
@Stateless
public class JMSBean implements JMSBeanLocal {
    @Resource(name="jms/DBQueueFactory")
    private ConnectionFactory dbQueueFactory;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory clientQueueFactory;
    @Resource(name="jms/DBQueue")
    private Destination dbQueue;
    @Resource(name="jms/ClientQueue")
    private Destination clientQueue;
    
    Connection connection = null,clientConnection=null;
    Session session = null,clientSession=null;
    MessageProducer messageProducer;
 
    @PostConstruct
    public void initialize() 
    {
    }

    @PreDestroy
    public void close()
    {
        try {
            session.close();
            clientSession.close();
            connection.close();
            clientConnection.close();
        } catch (JMSException ex) {
            Logger.getLogger(JMSBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
    public boolean login(String CID, String password) {
        try {
            connection=dbQueueFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            clientConnection=clientQueueFactory.createConnection();
            clientConnection.start();
            clientSession = clientConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            messageProducer = session.createProducer(dbQueue);       
        } catch (JMSException ex) {
            Logger.getLogger(JMSBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("Reach JMSBean!!!");
        
        boolean loginSuccessful=false;
        try {
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("L:"+CID+" "+password);
            
            System.out.println("Sending Message: "+tm.getText());
            
            messageProducer.send(tm);
            MessageConsumer consumer = clientSession.createConsumer(clientQueue);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='L' && m.charAt(2)=='T')
                loginSuccessful=true;
        } catch (JMSException ex) {
            Logger.getLogger(JMSBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return loginSuccessful;
    }

    @Override
    public ArrayList<String> getCustomerAccounts(String CID) {
        ArrayList<String> strList=new ArrayList<String>();
        try {
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("C:"+CID);
            messageProducer.send(tm);
            MessageConsumer consumer = clientSession.createConsumer(clientQueue);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            //System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='C'){
                String[] strs=m.substring(2).split("||");
                for (String str : strs){
                    strList.add(str);
                }
            }
        } catch (JMSException ex) {
            Logger.getLogger(JMSBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return strList;
    }

    @Override
    public boolean requestForHLAccountCreation(String CID) {
        boolean Successful=false;
        try {
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("H:"+CID);
            messageProducer.send(tm);
            MessageConsumer consumer = clientSession.createConsumer(clientQueue);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            //System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='H' && m.charAt(2)=='T')
                Successful=true;
        } catch (JMSException ex) {
            Logger.getLogger(JMSBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Successful;
    }

    @Override
    public boolean repayment(String customerID, String accountID, String homeloanID, double amount) {
        boolean Successful=false;
        try {
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("R:"+customerID+" "+accountID+" "+homeloanID+" "+amount);
            messageProducer.send(tm);
            MessageConsumer consumer = clientSession.createConsumer(clientQueue);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            //System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='R' && m.charAt(2)=='T')
                Successful=true;
        } catch (JMSException ex) {
            Logger.getLogger(JMSBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Successful;
    }

    @Override
    public double totalSavingsAccountsAmount() {
        double amount=0;
        try {
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("I:investor data");
            messageProducer.send(tm);
            MessageConsumer consumer = clientSession.createConsumer(clientQueue);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            //System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='I')
                amount=Double.parseDouble(m.substring(2));
        } catch (JMSException ex) {
            Logger.getLogger(JMSBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return amount;
    }
}
