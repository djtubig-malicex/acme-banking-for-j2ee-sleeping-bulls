/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.controller;

import java.util.Set;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import jpa.Customerinfo;
import org.jboss.logging.Logger;

/**
 *
 * @author JamesAlan
 */
public class CustomerInfoDAO
{
    private EntityManagerFactory emf;
    
    public CustomerInfoDAO()
    {
        emf = Persistence.createEntityManagerFactory("ACME_HomeLoanSystem-ejbPU");
    }
    
    public boolean persist(Customerinfo newCust)
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try
        {
            System.out.printf("Writing %s to db...", newCust.getEmail());
            em.persist(newCust);
            em.getTransaction().commit();
        } catch (Exception e)
        {
            em.getTransaction().rollback();
            System.out.println("Not persisted");
            java.util.logging.Logger.getLogger(CustomerInfoDAO.class.getName()).log(Level.SEVERE, null, e);
            Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException)e).getConstraintViolations();
            for(ConstraintViolation t : constraintViolations)
                System.err.println(t.getMessage());
            return false;
        } finally
        {
            em.close();
        }
        
        System.out.println("persisted");
        return true;
    }

    public Customerinfo getCustomerInfo(int C_ID)
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Customerinfo cust = em.find(Customerinfo.class, C_ID);
        
        em.close(); 
        
//        return (cust != null) ?
//            String.format("%d\t%s\t%s\n",
//                cust.getCId(),
//                cust.getEmail(),
//                cust.getPreferredcontact()
//            ) :
//            "-- not found --";
//      
        return cust;
    }

    public void update(Customerinfo c) 
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(c);
        em.getTransaction().commit();
        em.close();
    }
}
