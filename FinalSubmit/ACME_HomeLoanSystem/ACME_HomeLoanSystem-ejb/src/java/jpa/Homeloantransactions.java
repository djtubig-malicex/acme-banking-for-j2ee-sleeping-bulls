/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JamesAlan
 */
@Entity
@Table(name = "HOMELOANTRANSACTIONS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Homeloantransactions.findAll", query = "SELECT h FROM Homeloantransactions h"),
    @NamedQuery(name = "Homeloantransactions.findByHltId", query = "SELECT h FROM Homeloantransactions h WHERE h.hltId = :hltId"),
    @NamedQuery(name = "Homeloantransactions.findByFromaccnum", query = "SELECT h FROM Homeloantransactions h WHERE h.fromaccnum = :fromaccnum"),
    @NamedQuery(name = "Homeloantransactions.findByAmount", query = "SELECT h FROM Homeloantransactions h WHERE h.amount = :amount"),
    @NamedQuery(name = "Homeloantransactions.findByTransdate", query = "SELECT h FROM Homeloantransactions h WHERE h.transdate = :transdate")})
public class Homeloantransactions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "HLT_ID")
    private Integer hltId;
    @Column(name = "FROMACCNUM")
    private Integer fromaccnum;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AMOUNT")
    private Double amount;
    @Column(name = "TRANSDATE")
    @Temporal(TemporalType.DATE)
    private Date transdate;
    @JoinColumn(name = "TOHL_ACC", referencedColumnName = "HL_ACC")
    @ManyToOne(cascade={CascadeType.PERSIST},fetch=FetchType.LAZY)
    private Homeloanaccounts tohlAcc;

    public Homeloantransactions() {
    }

    public Homeloantransactions(Integer hltId) {
        this.hltId = hltId;
    }

    public Integer getHltId() {
        return hltId;
    }

    public void setHltId(Integer hltId) {
        this.hltId = hltId;
    }

    public Integer getFromaccnum() {
        return fromaccnum;
    }

    public void setFromaccnum(Integer fromaccnum) {
        this.fromaccnum = fromaccnum;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getTransdate() {
        return transdate;
    }

    public void setTransdate(Date transdate) {
        this.transdate = transdate;
    }

    public Homeloanaccounts getTohlAcc() {
        return tohlAcc;
    }

    public void setTohlAcc(Homeloanaccounts tohlAcc) {
        this.tohlAcc = tohlAcc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hltId != null ? hltId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Homeloantransactions)) {
            return false;
        }
        Homeloantransactions other = (Homeloantransactions) object;
        if ((this.hltId == null && other.hltId != null) || (this.hltId != null && !this.hltId.equals(other.hltId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "jpa.Homeloantransactions[ hltId=" + hltId + " ]";
    }
    
}
