/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Chris
 */
public class SavingsAccount {
    private String accNum;
    private String accType;
    private String accBal;
    
    public SavingsAccount(String accNum, String accBal){
        this.accNum = accNum;
        this.accBal = accBal;
    }

    public String getAccNum() {
        return accNum;
    }

    public void setAccNum(String accNum) {
        this.accNum = accNum;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getAccBal() {
        return accBal;
    }

    public void setAccBal(String accBal) {
        this.accBal = accBal;
    }
    
}
