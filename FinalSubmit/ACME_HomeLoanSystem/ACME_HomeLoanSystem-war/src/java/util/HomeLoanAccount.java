/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Chris
 */
public class HomeLoanAccount {
    private String accNum;
    private double amountBorrowed;
    private double amountRepayed;

    public HomeLoanAccount(String accNum, double amountBorrowed, double amountRepayed) {
        this.accNum = accNum;
        this.amountBorrowed = amountBorrowed;
        this.amountRepayed = amountRepayed;
    }

    public String getAccNum() {
        return accNum;
    }

    public void setAccNum(String accNum) {
        this.accNum = accNum;
    }

    public double getAmountBorrowed() {
        return amountBorrowed;
    }

    public void setAmountBorrowed(double amountBorrowed) {
        this.amountBorrowed = amountBorrowed;
    }

    public double getAmountRepayed() {
        return amountRepayed;
    }

    public void setAmountRepayed(double amountRepayed) {
        this.amountRepayed = amountRepayed;
    }
    
    
}
