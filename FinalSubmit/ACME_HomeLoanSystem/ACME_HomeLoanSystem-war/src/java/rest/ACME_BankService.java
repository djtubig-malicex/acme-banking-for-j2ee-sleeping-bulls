/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rest;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.enterprise.context.RequestScoped;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.TextMessage;

/**
 * REST Web Service
 *
 * @author Hubert
 */
@Path("stats")
@RequestScoped
public class ACME_BankService {

    @Resource(name="jms/DBQueueFactory")
    private ConnectionFactory dbQueueFactory;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory clientQueueFactory;
    @Resource(name="jms/DBQueue")
    private Destination dbQueue;
    @Resource(name="jms/ClientQueue")
    private Destination clientQueue;
    /**
     * Creates a new instance of ACME_RestService
     */
    public ACME_BankService() {
    }

    /**
     * Retrieves representation of an instance of REST.getTotalBalance
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/plain")
    public String getText() {
        //TODO return proper representation object     
        JMSContext jmsContext=null;
        JMSContext clientContext=null;
        jmsContext = dbQueueFactory.createContext();
        double amount=0;
        
        clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();            
        
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("I:investor data");
            producer.send(dbQueue,tm);
            JMSConsumer consumer = clientContext.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.err.println("Receive Feedback: "+m);
            if (m.charAt(0)=='I')
                amount=Double.parseDouble(m.substring(2));
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return String.valueOf(amount);
    }
}
