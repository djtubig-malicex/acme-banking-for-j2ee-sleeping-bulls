/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.Homeloantransactions;

/**
 *
 * @author JamesAlan
 */
@Stateless
public class HomeloantransactionsFacade extends AbstractFacade<Homeloantransactions> {
    @PersistenceContext(unitName = "ACME_HomeLoanSystem-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HomeloantransactionsFacade() {
        super(Homeloantransactions.class);
    }
    
}
