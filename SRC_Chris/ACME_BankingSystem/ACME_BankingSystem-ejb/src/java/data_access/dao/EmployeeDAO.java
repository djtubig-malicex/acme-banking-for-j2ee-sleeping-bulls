package data_access.dao;

import data_access.orm.Employee;
import java.util.ArrayList;

/**
 *
 * @author Hubert
 */
public interface EmployeeDAO
{
    String createEmployee(Employee employee);
    Employee readEmployee(String E_ID, String PIN);
    Employee readEmployee(String CID);
    void updateEmployee(Employee employee);
    void deleteEmployee(String E_ID);
    ArrayList<Employee> getAllEmployees();
}
