package data_access.dao;

import data_access.DataConstants;
import data_access.DataConstantsPrivate;
import data_access.orm.DataFactory;
import data_access.orm.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Hubert, JamesAlan
 */
public class RDBEmployeeDAO implements EmployeeDAO{
    /*
     * SQL QUERIES
     */
    private static final String 
        SQL_READ_AND_LOGIN_EMPLOYEE = "SELECT E_ID, FIRSTNAME, LASTNAME, DOB, ADDRESS, EMPLOYMENTDATE, PIN FROM ACME.EMPLOYEES WHERE E_ID=(?) AND PIN=(?)",
        SQL_READ_EMPLOYEE = "SELECT E_ID, FIRSTNAME, LASTNAME, DOB, ADDRESS, EMPLOYMENTDATE, PIN FROM ACME.EMPLOYEES WHERE E_ID=(?)",
        SQL_UPDATE_EMPLOYEE_DETAILS = "UPDATE ACME.EMPLOYEES SET FIRSTNAME=(?), LASTNAME=(?), DOB=(?), ADDRESS=(?), PIN=(?) WHERE E_ID=(?)",
        SQL_ADD_NEW_EMPLOYEE = "INSERT INTO ACME.EMPLOYEES(FIRSTNAME, LASTNAME, DOB, ADDRESS, PIN) VALUES (?, ?, ?, ?, ?)",
        SQL_DELETE_EMPLOYEE = "DELETE FROM ACME.EMPLOYEES WHERE E_ID=(?)",
        SQL_GET_ALL_EMPLOYEES = "SELECT E_ID, FIRSTNAME, LASTNAME, DOB, ADDRESS, EMPLOYMENTDATE, PIN FROM ACME.EMPLOYEES";
    
    private Connection dbConnection = null;

    public RDBEmployeeDAO(Connection connection) 
    {
        this.dbConnection = connection;
    }
    @Override
    public String createEmployee(Employee employee) 
    {
        String generatedID="";  
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_ADD_NEW_EMPLOYEE,
                Statement.RETURN_GENERATED_KEYS
            );
            sqlStatement.setString(1, employee.getFirstName());
            sqlStatement.setString(2, employee.getLastName());
            sqlStatement.setDate(3, employee.getDateOfBirth());
            sqlStatement.setString(4, employee.getAddress());
            sqlStatement.setString(5, employee.getPIN());
            sqlStatement.executeUpdate();            
            ResultSet result = sqlStatement.getGeneratedKeys();
            result.next();
            generatedID=String.format(DataConstantsPrivate.IDTYPE_SEVEN, DataConstants.EMPLOYEE_TYPE,result.getInt(1));              
            employee.setID(result.getInt(1));
        }        
        catch (SQLException sqlException)
        {
            System.err.println("Could not add new employee.");
            sqlException.printStackTrace();
        }        
        catch (IllegalAccessException ex)
        {
            // should not go here
            System.err.println("An IllegalAccessException occurred for generating Employee ID");
        }
        return generatedID;
    }

    /**
     * Read an existing Employee from the database, which will only work if a
     * valid ID and its corresponding PIN provided.
     * 
     * @param E_ID A valid Employee ID
     * @param PIN password assigned to employee (used for authentication)
     * @return an Employee object or NULL if the object could not be located
     */
    @Override
    public Employee readEmployee(String E_ID, String PIN)
    {
        Employee employeeObj = null;
        
        if (E_ID.length() < 2 || PIN.length() == 0)
            return null;
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_READ_AND_LOGIN_EMPLOYEE,
                Statement.RETURN_GENERATED_KEYS
            );            
            // print what was parsed
            System.out.printf("ID = %s", E_ID);
            System.out.printf("PIN = %s", PIN);

            sqlStatement.setInt(1, Integer.valueOf(E_ID.substring(1)));
            //sqlStatement.setInt(1, Integer.valueOf(E_ID));
            sqlStatement.setString(2, PIN);
            sqlStatement.execute();
            ResultSet result = sqlStatement.getResultSet();
            
            if (result.next())
            {
                employeeObj = parseEmployee(result);
                return employeeObj;
            }
            else
            {
                return null;
            }
        }
        
        catch (SQLException sqle)
        {
            System.out.println("Could not read the employee.");
            sqle.printStackTrace();
        }
        
        catch (IllegalAccessException ex) 
        {
            // should not go here
            System.err.println(ex.getMessage());
        }
        
        catch (Exception e)
        {
            System.out.println("Unhandled exception occurred");
            e.printStackTrace();
        }
        
        return employeeObj;
    }

    @Override
    public void updateEmployee(Employee employee) 
    {
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_UPDATE_EMPLOYEE_DETAILS
            );
            sqlStatement.setString(1, employee.getFirstName());
            sqlStatement.setString(2, employee.getLastName());
            sqlStatement.setDate(3, employee.getDateOfBirth());
            sqlStatement.setString(4, employee.getAddress());
            sqlStatement.setString(5, employee.getPIN());
            sqlStatement.setInt(6, employee.getNumericID());
            sqlStatement.executeUpdate();
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not update employee." + employee.getID());
            sqlException.printStackTrace();
        }
    }

    @Override
    public void deleteEmployee(String EID) 
    {
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_DELETE_EMPLOYEE,
                Statement.RETURN_GENERATED_KEYS
            );
            sqlStatement.setInt(1, Integer.valueOf(EID.substring(1)));
            sqlStatement.executeUpdate();
//            System.out.println(result);
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not delete employee." + EID);
            sqlException.printStackTrace();
        }
    }
    
    @Override
    public ArrayList<Employee> getAllEmployees()
    {
        ArrayList<Employee> employeeList = null;
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_GET_ALL_EMPLOYEES
            );
            sqlStatement.execute();
            ResultSet result = sqlStatement.getResultSet();
            
            employeeList = new ArrayList<>();
            
            while(result.next())
                employeeList.add(parseEmployee(result));
            
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not read employees.");
            sqlException.printStackTrace();
        }
        
        catch (IllegalAccessException ex)
        {
            // Should not go here
            //Logger.getLogger(RDBEmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return employeeList;
    }
    
    private Employee parseEmployee(ResultSet result) throws IllegalAccessException, SQLException
    {
        return DataFactory.buildEmployee(
            String.format(DataConstantsPrivate.IDTYPE_SEVEN, 
                DataConstants.EMPLOYEE_TYPE, result.getInt("E_ID")),
            result.getString("FIRSTNAME"),
            result.getString("LASTNAME"),
            result.getDate("DOB"),
            result.getString("ADDRESS"),
            result.getDate("EMPLOYMENTDATE"),
            result.getString("PIN")
        );
    }

    @Override
    public Employee readEmployee(String ID) {
        Employee employeeObj = null;
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_READ_EMPLOYEE
            );
            sqlStatement.setInt(1, Integer.valueOf(ID.substring(1)));
            sqlStatement.execute();
            ResultSet result = sqlStatement.getResultSet();
            result.next();
            
            employeeObj = parseEmployee(result);
            
            //System.out.println(result);
        }
        
        catch (SQLException sqlException)
        {
            System.err.println("Could not read the customer.");
            sqlException.printStackTrace();
        } 
        
        catch (IllegalAccessException ex) 
        {
            // Should not go here
        }
        
        return employeeObj;
    }
    
}
