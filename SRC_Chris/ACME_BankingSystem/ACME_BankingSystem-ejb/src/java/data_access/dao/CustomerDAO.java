package data_access.dao;

import data_access.orm.Customer;
import java.util.ArrayList;

/**
 *
 * @author JamesAlan
 */
public interface CustomerDAO
{
    String createCustomer(Customer customer);
    Customer readCustomer(String CID);
    void updateCustomer(Customer customer);
    void deleteCustomer(String CID);
    
    ArrayList<Customer> getAllCustomers();
    Customer readCustomer(String ID, String PIN);
}
