package data_access.orm;

import data_access.DataConstants;
import data_access.DataConstantsPrivate;
import java.sql.Date;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 *
 * @author JamesAlan
 */
public class Customer implements Serializable
{
    private String firstName, lastName, address, PIN;
    private int C_ID;
    private Date dateOfBirth;
    private ArrayList<Account> custAccounts;
    
    
    /*** Constructors ***/
    
    /**
     * Constructor for an empty (new) Customer object.
     * 
     * @param firstName First name of customer
     * @param lastName Surname of customer
     * @param dateOfBirth Birth date of customer
     * @param address Contact and billing address of customer
     * @param PIN password
     */
    Customer(String firstName, String lastName, Date dateOfBirth, String address, String PIN)
    {
        this.C_ID = -1;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.PIN = PIN;
    }
    
    /**
     * Constructor for a pre-existing Customer
     * @param C_ID corresponding numeric ID of the Customer
     * @param firstName First name of customer
     * @param lastName surname of customer
     * @param dateOfBirth birth date of customer
     * @param address billing and contact address of customer
     */
    Customer(int C_ID, String firstName, String lastName, Date dateOfBirth, String address, String PIN) throws IllegalAccessException
    {
        if(C_ID < 1) throw new IllegalAccessException();
        this.C_ID = C_ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.PIN = PIN;
    }
    
    /*** Accessors ***/
    
    public String getFirstName()
    {
        return firstName;
    }

    public String getID() 
    {
        return String.format(DataConstantsPrivate.IDTYPE_SEVEN, DataConstants.CUSTOMER_TYPE, C_ID);
        
    }
    public int getNumericID()
    {
        return C_ID;
    }

    public String getAddress() 
    {
        return address;
    }

    public Date getDateOfBirth() 
    {
        return dateOfBirth;
    }

    public String getLastName() 
    {
        return lastName;
    }
    
    /*** Mutators ***/
    
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public void setDateOfBirth(Date dateOfBirth) 
    {
        this.dateOfBirth = dateOfBirth;
    }

    public void setFirstName(String firstName) 
    {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) 
    {
        this.lastName = lastName;
    }

    /**
     * Set unique ID of Customer provided it has not been set already.
     * 
     * @param generatedID Generated ID from database
     * @throws IllegalAccessException 
     */
    public void setID(int generatedID) throws IllegalAccessException
    {
        if (C_ID > 0 || generatedID < 1) throw new IllegalAccessException();
        this.C_ID = generatedID;
    }
    
    @Override
    public String toString()
    {
        return String.format("%s,%s,%s,%s,%s,%s",
            getID(),
            getFirstName(),
            getLastName(),
            getAddress(),
            getDateOfBirth(),
            getAddress()
        );
    }

    public String getPIN()
    {
        return PIN;
    }
    
    public static Customer parseCustomer(String representation) 
    {    
        try 
        {
            String tokens[] = representation.split(","),
                   dateToken[] = tokens[3].split("-");
            
            return DataFactory.buildCustomer(
                    tokens[0],
                    tokens[1], 
                    tokens[2], 
                    new Date(new GregorianCalendar(
                            Integer.parseInt(dateToken[0]), 
                            Integer.parseInt(dateToken[1]),
                            Integer.parseInt(dateToken[2]))
                        .getTimeInMillis()),
                    tokens[3],
                    tokens[4]
            );
        }
        
        catch (IllegalAccessException ex) 
        {
            return null;
        }
    }
}
