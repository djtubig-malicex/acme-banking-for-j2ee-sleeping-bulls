package data_access.orm;

import data_access.DataConstants;
import data_access.DataConstantsPrivate;
import java.io.Serializable;

/**
 *
 * @author JamesAlan
 */
public class Account implements Serializable
{
    //private Customer accOwner;
    //private String accOwnerID, accID;
    private int accNum, ownerID;
    private double balance;
    
    /**
     * Constructor for an empty new Account.
     * @param C_ID A defined valid customer ID
     */
    Account(int C_ID)
    {
        this.accNum = -1;
        this.ownerID = C_ID;
        this.balance = 0;
    }
    
    Account(int accID, int C_ID, double balance) throws IllegalAccessException
    {
        if (accID < 1) throw new IllegalAccessException();
        this.accNum = accID;
        this.ownerID = C_ID;
        this.balance = balance;
    }
    
    Account(String accID, String C_ID, double balance) throws IllegalAccessException
    {
        this(Integer.parseInt(accID.substring(1)), 
             Integer.parseInt(C_ID.substring(1)), balance);
    }
    
    public void deposit(double amount) throws IllegalAccessException
    {
        if (amount <= 0)
            throw new IllegalAccessException("You cannot deposit a negatie amount of dollars!");
        
        balance += amount;
    }

    public void withdraw(double amount) throws IllegalAccessException
    {
        if (amount > balance || amount <= 0)
            throw new IllegalAccessException("Withdrawal amount is invalid or above balance!");
        
        balance -= amount;
    }

    public double getBalance()
    {
        return balance;
    }
    
    public String getOwnerID()
    {
        // return (accOwner != null) ? accOwner.getID() : accOwnerID;
        return String.format(DataConstantsPrivate.IDTYPE_SEVEN, DataConstants.CUSTOMER_TYPE, ownerID);
    }
    
    public String getID()
    {
        return String.format(DataConstantsPrivate.IDTYPE_TEN, DataConstants.ACCOUNT_TYPE, accNum);
    }
    
    public int getNumericID()
    {
        return accNum;
    }
    
    public int getNumericOwnerID()
    {
        return ownerID;
    }
    
    @Override
    public String toString()
    {
        return String.format("%s,%s,%s",
            getID(),
            getOwnerID(),
            getBalance()
        );
    }
    
    /**
     * Set the account number for the new Account.
     * 
     * @param generatedID Generated account number from database
     * @throws IllegalAccessException 
     */
    public void setID(int generatedID) throws IllegalAccessException
    {
        if (accNum > 0 || generatedID < 1) throw new IllegalAccessException();
        accNum = generatedID;
    }
}
