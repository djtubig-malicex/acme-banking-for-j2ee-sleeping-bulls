/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data_access;

/**
 *
 * @author JamesAlan
 */
public class DataConstantsPrivate
{
    public static final int
        MAX_CUSTOMER_ACCOUNTS = 2;
    
    public static final String
       IDTYPE_SEVEN = "%c%07d",
       IDTYPE_TEN = "%c%010d";
}
