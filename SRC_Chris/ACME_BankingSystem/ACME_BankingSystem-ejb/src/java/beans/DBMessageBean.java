/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author Hubert
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/DBQueue")
})
public class DBMessageBean implements MessageListener {
    @EJB
    private ACMEPersistenceBeanLocal acmePersistenceBean;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory queueFactory;
    @Resource(name="jms/ClientQueue")
    private Destination queue;
    
    public DBMessageBean() {
    }
    
    @Override
    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        Session session = null;
        Connection connection = null;
        String text;
        try {
            text=textMessage.getText();
            char Type=text.charAt(0);
            text=text.substring(2);
            String[] strs=text.split(" ");
            switch (Type){
                case 'L':
                    boolean login = acmePersistenceBean.login(strs[0], strs[1]);
                    connection=queueFactory.createConnection();
                    connection.start();
                    session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                    MessageProducer producer = session.createProducer(message.getJMSReplyTo());  
                    String reply=login?"T":"F";
                    Message replyMessage = session.createTextMessage("L:"+reply);
                    replyMessage.setJMSCorrelationID(message.getJMSMessageID());
                    producer.send(replyMessage);  
                    break;
                case 'A':
                    break;
                default:
                    break;
            }
            System.out.println("Received message:" +
            textMessage.getText());
        } catch (JMSException ex) {
            Logger.getLogger(DBMessageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
