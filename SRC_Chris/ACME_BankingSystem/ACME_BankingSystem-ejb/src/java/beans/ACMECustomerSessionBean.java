/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import data_access.dao.RDBTransactionDAO;
import data_access.orm.Customer;
import java.sql.Date;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;

/**
 *
 * @author JamesAlan, Hubert
 */
@Stateful
@StatefulTimeout(value = 1, unit = TimeUnit.MINUTES)
public class ACMECustomerSessionBean implements ACMECustomerSessionBeanRemote 
{
    @EJB
    private ACMEPersistenceBeanLocal acmePersistenceBean;
    
    private Customer currentCustomer;
    
    @PostConstruct
    public void initialize() 
    {
        // ?
    }

    @PreDestroy
    public void close()
    {
        //operations = 0;
        currentCustomer = null;
    }
    
    @Remove
    @Override
    public void removeBean() 
    {
        System.err.println("Customer Bean removed");
    }
    
    @Override
    public String addAccount(String C_ID)
    {
        try 
        {
            return acmePersistenceBean.addAccount(C_ID);
        } 
        
        catch (IllegalAccessException ex) 
        {
            Logger.getLogger(ACMECustomerSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    @Override
    public boolean depositMoney(String C_ID, String accNum, double amt)
    {
        // TODO refactor
        System.err.println("WARNING: NEEDS EMPLOYEE AUTH ATM");
        return acmePersistenceBean.depositMoney(C_ID, C_ID, accNum, amt);
    }

    @Override
    public boolean withdrawMoney(String C_ID, String accNum, double amt) 
    {
        // TODO refactor
        System.err.println("WARNING: NEEDS EMPLOYEE AUTH ATM");
        return acmePersistenceBean.withdrawMoney(C_ID, C_ID, accNum, amt);
    }

    @Override
    public double getAccountBalance(String accNum) 
    {
        return acmePersistenceBean.getAccountBalance(currentCustomer.getID(), accNum);
    }
    
    @Override
    public String applyAuthenticationResult(String C_ID, String PIN)
    {
        try
        {
            currentCustomer = acmePersistenceBean.getCustomer(C_ID, PIN);
        }
        
        catch (Exception e)
        {
            System.err.println(e.getMessage());
            return "Unauthorised or Invalid Employee";
        }
        
        return String.format("%s %s", currentCustomer.getFirstName(), currentCustomer.getLastName());
    }
    
    /**
     * Helper method to keep session bean alive after client prompts
     */
    @Override
    public void pingMe()
    {
        System.out.println("PING");
    }

    @Override
    public ArrayList<String> getTransactions(String accNum, Date startDate, Date endDate)
    {
        return acmePersistenceBean.getTransactionsByAccount(currentCustomer.getID(), 
            accNum, startDate, endDate);
    }
}
