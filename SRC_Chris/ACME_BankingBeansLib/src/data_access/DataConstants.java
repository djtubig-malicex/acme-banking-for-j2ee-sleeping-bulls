
package data_access;

/**
 * Floating class containing static variables common to the ACME banking system
 * 
 * @author JamesAlan
 */
public class DataConstants
{
    public static final char
       CUSTOMER_TYPE = 'C',
       ACCOUNT_TYPE = 'A',
       EMPLOYEE_TYPE = 'E',
       TRANSACTION_TYPE = 'T';
    
    public static final int
       EMPLOYEE_OPERATION_LIMIT = 10,
       CLIENT_LOGIN_LIMIT = 5;
}
