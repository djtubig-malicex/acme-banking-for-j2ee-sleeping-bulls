/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.sql.Date;
import java.util.ArrayList;
import javax.ejb.EJBAccessException;
import javax.ejb.Remote;

/**
 * Remote interface for an Employee session bean, with relevant functionalities
 * valid for an Employee exposed.
 * 
 * @author JamesAlan
 */
@Remote
public interface ACMEEmployeeSessionBeanRemote 
{   
    String addCustomer(String firstname, String lastname, Date dob, 
        String address, String PIN) throws EJBAccessException;
    String addAccount(String C_ID) throws EJBAccessException;
    String addEmployee(String firstname, String lastname, Date dob,
        String address, Date doe, String PIN) throws EJBAccessException;
    
    void updateCustomer(String C_ID,String firstname, String lastname, 
        Date dob, String address, String PIN) throws EJBAccessException;
    void updateEmployee(String E_ID,String firstname, String lastname,
        Date dob, String address, Date doe, String PIN) throws EJBAccessException;
    
    void deleteCustomer(String C_ID) throws EJBAccessException;
    void deleteEmployee(String E_ID) throws EJBAccessException;
    ArrayList<String> getAllCustomers() throws EJBAccessException;
    ArrayList<String> getAllEmployees() throws EJBAccessException;
    ArrayList<String> getAllAccounts() throws EJBAccessException;
    ArrayList<String> getAllTransactions() throws EJBAccessException;
    ArrayList<String> getCustomerAccounts(String C_ID) throws EJBAccessException;
    ArrayList<String> getTransactions(String C_ID, String accNum, 
        Date startDate, Date endDate) throws EJBAccessException;
    
    boolean depositMoney(String C_ID, String accNum, double amt) throws EJBAccessException;
    boolean withdrawMoney(String C_ID, String accNum, double amt) throws EJBAccessException;

    double getAccountBalance(String C_ID, String accNum) throws EJBAccessException;

    //public void performOperations() throws EJBAccessException;
    int getOperations() throws EJBAccessException;
    
    void removeBean();
    String applyAuthenticationResult(String E_ID, String PIN);
    void pingMe();

    String readCustomer(String C_ID, String pin) throws EJBAccessException;
}
