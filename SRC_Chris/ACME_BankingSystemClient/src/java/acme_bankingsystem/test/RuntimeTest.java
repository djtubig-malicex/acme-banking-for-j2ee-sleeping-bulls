/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package acme_bankingsystem.test;

import beans.ACMEEmployeeSessionBeanRemote;
import beans.ACMELoginBeanRemote;
import java.io.Console;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JOptionPane;

/**
 *
 * @author Hubert
 */
public class RuntimeTest
{
    private static RuntimeTest instance;
    
    @EJB
    private static ACMELoginBeanRemote sessionBean;
    //@EJB
    private static ACMEEmployeeSessionBeanRemote employeeBean;
    
    private RuntimeTest()
    {
    }
    
    private ACMEEmployeeSessionBeanRemote lookupACMEEmployeeAuthBeanRemote() {
        try {
            Context c = new InitialContext();
            return (ACMEEmployeeSessionBeanRemote) c.lookup("java:global/ACME_BankingSystem-ejb/ACMEEmployeeAuthBean");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
/*
    private ACMESessionBeanRemote lookupACMESessionBeanRemote() {
        try {
            Context c = new InitialContext();
            return (ACMESessionBeanRemote) c.lookup("java:comp/env/ACMESessionBean");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }*/
    
    public static RuntimeTest getInstance()
    {
        if (instance == null)
            instance = new RuntimeTest();
        
        return instance;
    }
    
    public void startup()
    {
        Scanner in = new Scanner(System.in);
        Console con = System.console();
        String inputEID, inputPIN;
        // Init beans
        System.out.println("Welcome to ACME Banking System.\n");
        //sessionBean = lookupACMESessionBeanRemote();
        //authBean = lookupACMEEmployeeAuthBeanRemote();
        
        // Read employee authentication details
        System.out.print("Please enter your Employee ID: ");
        
        inputEID = in.nextLine();
        
        System.out.print("Please enter your PIN: ");
        inputPIN = (con != null) ? 
            new String(con.readPassword()) :  
            in.nextLine();

        // Process
        boolean login=sessionBean.login(inputEID, inputPIN);
        if (login) employeeBean=lookupACMEEmployeeAuthBeanRemote();
        System.err.printf("!%s!\n",
            login? "You are now logged in!" : "Access Denied."
        );
        while(true) {
            try{
                while (login){
                    //employeeBean.performOperations();
                    int num=employeeBean.getOperations();
                    System.out.println(num);
                    if (num>8 && num<11) 
                    try {
                        Thread.sleep(75000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(RuntimeTest.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (num>14) employeeBean.removeBean();
                }
            }catch(Exception e){
                employeeBean=lookupACMEEmployeeAuthBeanRemote();
                // Read employee authentication details
                System.out.print("Please enter your Employee ID: ");

                inputEID = in.nextLine();

                System.out.print("Please enter your PIN: ");
                inputPIN = (con != null) ? 
                    new String(con.readPassword()) :  
                    in.nextLine();

                // Process
                login=sessionBean.login(inputEID, inputPIN);
            }
        }

    }

    
    public static void main(String[] args)
    {
        // TODO Create your menu here
        RuntimeTest client = RuntimeTest.getInstance();
        client.startup();
        
        System.err.println("Menu needs to be implemented!\nTest drivers are found in the Test package.");
        JOptionPane.showMessageDialog(null, 
                "Menu needs to be implemented!\nTest drivers are found in the Test package.",
                "ACME Banking System", JOptionPane.PLAIN_MESSAGE);
        
    }
}
