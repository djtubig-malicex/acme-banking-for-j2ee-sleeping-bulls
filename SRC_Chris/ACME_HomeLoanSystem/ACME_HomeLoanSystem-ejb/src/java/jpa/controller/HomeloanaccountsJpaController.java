/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.controller;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import jpa.Homeloanaccounts;
import jpa.controller.exceptions.NonexistentEntityException;
import jpa.controller.exceptions.RollbackFailureException;

/**
 *
 * @author Hubert
 */
public class HomeloanaccountsJpaController implements Serializable {

    public HomeloanaccountsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Homeloanaccounts homeloanaccounts) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(homeloanaccounts);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Homeloanaccounts homeloanaccounts) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            homeloanaccounts = em.merge(homeloanaccounts);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = homeloanaccounts.getHlAcc();
                if (findHomeloanaccounts(id) == null) {
                    throw new NonexistentEntityException("The homeloanaccounts with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Homeloanaccounts homeloanaccounts;
            try {
                homeloanaccounts = em.getReference(Homeloanaccounts.class, id);
                homeloanaccounts.getHlAcc();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The homeloanaccounts with id " + id + " no longer exists.", enfe);
            }
            em.remove(homeloanaccounts);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Homeloanaccounts> findHomeloanaccountsEntities() {
        return findHomeloanaccountsEntities(true, -1, -1);
    }

    public List<Homeloanaccounts> findHomeloanaccountsEntities(int maxResults, int firstResult) {
        return findHomeloanaccountsEntities(false, maxResults, firstResult);
    }

    private List<Homeloanaccounts> findHomeloanaccountsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Homeloanaccounts.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Homeloanaccounts findHomeloanaccounts(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Homeloanaccounts.class, id);
        } finally {
            em.close();
        }
    }

    public int getHomeloanaccountsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Homeloanaccounts> rt = cq.from(Homeloanaccounts.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
