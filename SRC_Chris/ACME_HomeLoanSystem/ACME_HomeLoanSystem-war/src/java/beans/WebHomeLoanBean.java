/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import org.primefaces.event.FlowEvent;
import util.User;

/**
 *
 * @author Chris
 */
@Named(value = "webHomeLoanBean")
@ManagedBean
// available throughout the users session
@SessionScoped
public class WebHomeLoanBean implements Serializable {
    
    // general form variables
    private String error;
    private boolean isAuth;
    private User user = new User();  
    private boolean skip;
    
    private static final Logger logger = Logger.getLogger(WebHomeLoanBean.class.getName());  
    
    /** GUI methods **/
    
    public User getUser() {  
        return user;  
    }  
  
    public void setUser(User user) {  
        this.user = user;
    }  
      
    public void save(ActionEvent actionEvent) {  
        //Persist user  
        
        FacesMessage msg = new FacesMessage("Successful", "Welcome :" + user.firstName);  
        FacesContext.getCurrentInstance().addMessage(null, msg);  
    }  
    
    public boolean isSkip() {  
        return skip;  
    }  
  
    public void setSkip(boolean skip) {  
        this.skip = skip;  
    }  
      
    public String onFlowProcess(FlowEvent event) {  
        logger.info("Current wizard step:" + event.getOldStep());  
        logger.info("Next step:" + event.getNewStep());  
        
        if(skip) {  
            skip = false;   //reset in case user goes back  
            return "confirmation";  
        }  
        else {  
            return event.getNewStep();  
        }  
    }  
    
    /** Bean specific methods **/
    
    @PostConstruct
    public void initialize() 
    {
        isAuth = false;
        error = "";
    }

    @PreDestroy
    public void close()
    {
        isAuth = false;
    }
    
    public void validate(){
        
    }
    
    public void submit(){
        
    }
}
