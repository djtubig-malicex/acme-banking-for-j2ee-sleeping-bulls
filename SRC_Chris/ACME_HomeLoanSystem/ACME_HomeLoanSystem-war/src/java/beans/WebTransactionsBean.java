/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.PrePassivate;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import util.HomeLoanAccount;
import util.SavingsAccount;
import util.User;

/**
 *
 * @author Chris
 */
@Named(value = "webTransactionsBean")
@ManagedBean
// available throughout the users session
@SessionScoped
public class WebTransactionsBean implements Serializable {
    
    // general form variables
    private String error;
    private boolean isAuth;
    private ArrayList<SavingsAccount> sAccs;
    private ArrayList<HomeLoanAccount> hlAccs;
    private String toAccountID;
    private String fromAccountID;
    private double amount;
    
    private static final Logger logger = Logger.getLogger(WebAccountsBean.class.getName());  
    
    /** GUI methods **/
    public String getError() {
        return error;
    }
    
    public void setError(String error) {
        this.error = error;
    }
    
    public boolean isIsAuth() {
        return isAuth;
    }

    public void setIsAuth(boolean isAuth) {
        this.isAuth = isAuth;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public ArrayList<SavingsAccount> getsAccs() {
        return sAccs;
    }

    public void setsAccs(ArrayList<SavingsAccount> sAccs) {
        this.sAccs = sAccs;
    }

    
    public ArrayList<HomeLoanAccount> getHlAccs() {
        return hlAccs;
    }

    public void setHlAccs(ArrayList<HomeLoanAccount> hlAccs) {
        this.hlAccs = hlAccs;
    }

    public String getToAccountID() {
        return toAccountID;
    }

    public void setToAccountID(String toAccountID) {
        this.toAccountID = toAccountID;
    }

    public String getFromAccountID() {
        return fromAccountID;
    }

    public void setFromAccountID(String fromAccountID) {
        this.fromAccountID = fromAccountID;
    }
    
    /** Bean specific methods **/
    
    @PostConstruct
    public void initialize() 
    {
        sAccs = new ArrayList<SavingsAccount>();
        hlAccs = new ArrayList<HomeLoanAccount>();
        
        // call session bean method to return 
        // sAccs = webSessionBean.getSavingsAccounts();
        // hlAccs = webSessionBean.getHLAccounts();
        
        
        
    }
    
    @PrePassivate
    @PreDestroy
    public void close()
    {
        // do any cleanup here
        
    }
    
    /** GUI action methods **/
    
    public void validate(){
        
        
    }
    
    public String submit(){
        // validate atomicity of transaction
        boolean sufficientSavings = true;
        boolean homeloanpaid = false;
        
        // call method that makes changes in ACME_BankingSystem (but does not commit) - withdraws
        //sufficientSavings =
        
        if (!sufficientSavings){
            // display error message on page
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Insufficient funds", "More info"));
            return "transactions";
        }else {
            // call method to check if homeloan is paid off already and if not deduct amount using JPA
            // homeloanpaid = 
            
            if (homeloanpaid){
                // display error message
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"You do not owe money on this account", "More info"));
                return "transactions";
            }else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Desposit has been made!", "More info"));
                return "#";
            }
        }
    }
}
