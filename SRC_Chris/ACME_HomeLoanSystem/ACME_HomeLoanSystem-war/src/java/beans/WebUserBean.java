/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.PrePassivate;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author JamesAlan
 */
@Named(value = "webUserBean")
@ManagedBean
// available throughout the users session
@SessionScoped
public class WebUserBean implements Serializable
{
    private String username;
    
    public WebUserBean() 
    {
        //
    }
    
    @PostConstruct
    public void initialize()
    {
        
    }
    
    @PrePassivate
    public void passivate()
    {
        //?????
    }
}
