package beans;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author JamesAlan
 */
@Named(value = "webLoginBean")
@ManagedBean
// available throughout the users session
@SessionScoped
public class WebLoginBean implements Serializable
{
    //private static final long serialVersionUID = 661;
    @Resource(name="jms/DBQueueFactory")
    private ConnectionFactory dbQueueFactory;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory clientQueueFactory;
    @Resource(name="jms/DBQueue")
    private Destination dbQueue;
    @Resource(name="jms/ClientQueue")
    private Destination clientQueue;
    
    private String id, password, error;
    private boolean isAuth;
    
    /**
     * Creates a new instance of WebLoginBean
     */
    public WebLoginBean() 
    {
        
    }
    
    @PostConstruct
    public void initialize() 
    {
        isAuth = false;
        error = "";
    }

    @PreDestroy
    public void close()
    {
        id = null;
        password = null;
        isAuth = false;
    }
    
    public String getError() { return error; }
    public String getId() { return id; }
    public String getPassword() { return password; }

    public void isAuthenticated() throws IOException
    {
        if (!this.isAuth)
        {
            System.out.println("Authentication failed, redirecting");
            FacesContext.getCurrentInstance().getExternalContext().redirect("sessionTimeout.xhtml");
        }
    }
    
    public void setId(String id) { this.id = id.toUpperCase(); }
    public void setPassword(String password) { this.password = password; }
    
    
    public String getWelcomeMessage()
    {
        //TODO get actual person's name
        return String.format("You are logged  in as (%s)", id);
    }
    
    public String initMe() throws JMSException
    {
        this.error = "";
        Connection connection = null,clientConnection=null;
        Session session = null,clientSession=null;
        boolean loginSeccessful=false;
        
        
        /** JMS messaging stuff **/
        
        try{
            
            // create connection to database
            connection=dbQueueFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            clientConnection=clientQueueFactory.createConnection();
            clientConnection.start();
            
            // setup producer and consumer
            clientSession = clientConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(dbQueue);
            
            // create  and send the JMS message
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);
            tm.setText("L:"+this.id+" "+this.password);
            messageProducer.send(tm);
            
            MessageConsumer consumer = clientSession.createConsumer(clientQueue);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            //System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='L' && m.charAt(2)=='T'){
                loginSeccessful=true;
            }
            
        }finally{
            if (session!=null){
                try{
                    session.close();
                }catch(JMSException e){
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Cannot close session", e);
                }
            }
            if (clientSession!=null){
                try{
                    clientSession.close();
                }catch(JMSException e){
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Cannot close session", e);
                }
            }
            if(connection!=null){
                connection.close();
            }
            if(clientConnection!=null){
                clientConnection.close();
            }
        }
        
        /** Redirect to welcome page and setup session context variable if successful **/
        if (loginSeccessful)
        {    
            System.out.println("ACME LOGIN SUCCESS");
            
            //TODO use this?
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().put("loginid", this.id);
            
            this.isAuth = true;
            
            //session.setAttribute("loginid", this.id);
            return "accounts";
        }
        
        /** Display error message in the <p:messages> panel  **/
        System.out.println("ACME LOGIN FAILED");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid Username or Password", "More info"));
        
        this.error = "Invalid ID or password.  Please try again.";
        //FacesContext context = FacesContext.getCurrentInstance();
        //context.addMessage(errorMsg.getClientId(), new FacesMessage("FAIL"));
        return "login";
    }
    
    public String logout()
    {
        close();
        return "welcome";
    }
}
