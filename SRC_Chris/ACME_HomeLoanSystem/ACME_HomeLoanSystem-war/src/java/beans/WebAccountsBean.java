/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.PrePassivate;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import util.HomeLoanAccount;
import util.SavingsAccount;
import util.User;

/**
 *
 * @author Chris
 */
@Named(value = "webAccountsBean")
@ManagedBean
// available throughout the users session
@SessionScoped
public class WebAccountsBean implements Serializable {
    
    // general form variables
    private String error;
    private boolean isAuth;
    private ArrayList<SavingsAccount> sAccs;
    private ArrayList<HomeLoanAccount> hlAccs;
    
    private static final Logger logger = Logger.getLogger(WebAccountsBean.class.getName());  
    
    /** GUI methods **/
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isIsAuth() {
        return isAuth;
    }

    public void setIsAuth(boolean isAuth) {
        this.isAuth = isAuth;
    }

    public ArrayList<SavingsAccount> getsAccs() {
        return sAccs;
    }

    public void setsAccs(ArrayList<SavingsAccount> sAccs) {
        this.sAccs = sAccs;
    }

    public ArrayList<HomeLoanAccount> getHlAccs() {
        return hlAccs;
    }

    public void setHlAccs(ArrayList<HomeLoanAccount> hlAccs) {
        this.hlAccs = hlAccs;
    }
    
    /** Bean specific methods **/
    
    @PostConstruct
    public void initialize() 
    {
        sAccs = new ArrayList<SavingsAccount>();
        hlAccs = new ArrayList<HomeLoanAccount>();
        
        // call session bean method to return 
        // sAccs = webSessionBean.getSavingsAccounts();
        // hlAccs = webSessionBean.getHLAccounts();
        
        
    }
    
    @PrePassivate
    @PreDestroy
    public void close()
    {
        
    }
    
    /** GUI action methods **/
    
    public void validate(){
        
    }
    
    public void submit(){
        
    }
    
    
    
}
