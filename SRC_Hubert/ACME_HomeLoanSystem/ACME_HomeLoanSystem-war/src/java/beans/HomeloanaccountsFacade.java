/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.Homeloanaccounts;

/**
 *
 * @author JamesAlan
 */
@Stateless
public class HomeloanaccountsFacade extends AbstractFacade<Homeloanaccounts> {
    @PersistenceContext(unitName = "ACME_HomeLoanSystem-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HomeloanaccountsFacade() {
        super(Homeloanaccounts.class);
    }
    
}
