/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.Customerinfo;

/**
 *
 * @author JamesAlan
 */
@Stateless
public class CustomerinfoFacade extends AbstractFacade<Customerinfo> {
    @PersistenceContext(unitName = "ACME_HomeLoanSystem-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomerinfoFacade() {
        super(Customerinfo.class);
    }
    
}
