/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.PrePassivate;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.TextMessage;
import util.HomeLoanAccount;
import util.SavingsAccount;
import util.User;

/**
 *
 * @author Chris
 */
@Named(value = "webAccountsBean")
@ManagedBean
// available throughout the users session
@SessionScoped
public class WebAccountsBean implements Serializable {
    @Resource(name="jms/DBQueueFactory")
    private ConnectionFactory dbQueueFactory;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory clientQueueFactory;
    @Resource(name="jms/DBQueue")
    private Destination dbQueue;
    @Resource(name="jms/ClientQueue")
    private Destination clientQueue;
    // general form variables
    private String error;
    private boolean isAuth;
    private ArrayList<SavingsAccount> sAccs;
    private ArrayList<HomeLoanAccount> hlAccs;
    
    private static final Logger logger = Logger.getLogger(WebAccountsBean.class.getName());  
    
    /** GUI methods **/
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isIsAuth() {
        return isAuth;
    }

    public void setIsAuth(boolean isAuth) {
        this.isAuth = isAuth;
    }

    public ArrayList<SavingsAccount> getsAccs() {
        return sAccs;
    }

    public void setsAccs(ArrayList<SavingsAccount> sAccs) {
        this.sAccs = sAccs;
    }

    public ArrayList<HomeLoanAccount> getHlAccs() {
        return hlAccs;
    }

    public void setHlAccs(ArrayList<HomeLoanAccount> hlAccs) {
        this.hlAccs = hlAccs;
    }
    
    public ArrayList<String> getCustomerAccounts(String CID) {
        ArrayList<String> strList=new ArrayList<String>();
        JMSContext jmsContext=dbQueueFactory.createContext();
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();             
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("C:"+CID);
            producer.send(dbQueue,tm);
            JMSConsumer consumer = clientContext.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='C'){
                String[] strs=m.substring(2).split("||");
                strList.addAll(Arrays.asList(strs));
            }
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return strList;
    }
    
    public boolean repayment(String customerID, String accountID, String homeloanID, double amount) {
        boolean successful=false;
        JMSContext jmsContext=dbQueueFactory.createContext();
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();             
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("R:"+customerID+" "+accountID+" "+homeloanID+" "+amount);
            //messageProducer.send(tm);
            producer.send(dbQueue,tm);
            JMSConsumer consumer = clientContext.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            //System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='R' && m.charAt(2)=='T')
                successful=true;
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return successful;
    }
    
    /** Bean specific methods **/
    
    @PostConstruct
    public void initialize() 
    {
        sAccs = new ArrayList<SavingsAccount>();
        hlAccs = new ArrayList<HomeLoanAccount>();
        
        // call session bean method to return 
        // sAccs = webSessionBean.getSavingsAccounts();
        // hlAccs = webSessionBean.getHLAccounts();
        
        
    }
    
    @PrePassivate
    @PreDestroy
    public void close()
    {
        
    }
    
    /** GUI action methods **/
    
    public void validate(){
        
    }
    
    public void submit(){
        
    }
    
    
    
}
