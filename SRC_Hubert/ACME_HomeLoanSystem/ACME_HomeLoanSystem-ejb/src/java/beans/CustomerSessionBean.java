/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import jpa.Customerinfo;

/**
 *
 * @author Hubert
 */
@Stateful
public class CustomerSessionBean implements CustomerSessionBeanRemote {
    @EJB
    private JMSBeanLocal jmsBean;
    
    @EJB
    private HomeLoanPersistenceBeanRemote homeLoanPersistenceBean;
    
    @Override
    public boolean login(String CID, String pw) 
    {
        System.out.println("Reach CustomerSessionBean!!!");
        return jmsBean.login(CID, pw);
    }

    @Override
    public boolean openNewHomeLoan(Customerinfo cust, double amtBorrow) 
    {
        System.out.println("Opening new homeloan for " + cust.getCId() + " with amount borrowed $" + amtBorrow);
        return homeLoanPersistenceBean.createHomeLoanAccount(cust, amtBorrow);
    }

    @Override
    public Customerinfo getCustomerInfo(String id) 
    {
        System.out.println("Obtaining customer info...");
        return homeLoanPersistenceBean.getCustomerInfo(Integer.parseInt(id.substring(1)));
    }

    @Override
    public boolean setCustomerInfo(Customerinfo cust)
    {
        return homeLoanPersistenceBean.setCustomerInfo(cust);
    }
    

}
