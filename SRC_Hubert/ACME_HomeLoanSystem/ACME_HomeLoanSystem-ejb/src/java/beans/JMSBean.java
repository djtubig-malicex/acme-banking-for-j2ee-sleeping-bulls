/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author Hubert
 */
@Stateful
public class JMSBean implements JMSBeanLocal {
    @Resource(name="jms/DBQueueFactory")
    private ConnectionFactory dbQueueFactory;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory clientQueueFactory;
    @Resource(name="jms/DBQueue")
    private Destination dbQueue;
    @Resource(name="jms/ClientQueue")
    private Destination clientQueue;
    
    JMSContext jmsContext=null;
    @PostConstruct
    public void initialize() 
    {    
        jmsContext = dbQueueFactory.createContext();
    }

    @PreDestroy
    public void close()
    {
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
    public boolean login(String CID, String password) {
               
        System.out.println("Reach JMSBean!!!");
        boolean loginSuccessful=false;
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();            
        JMSConsumer consumer = clientContext.createConsumer(clientQueue); 
        try{            
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);  
            // create  and send the JMS message
            tm.setText("L:"+CID+" "+password);            
            System.out.println("Sending Message: "+tm.getText());
            producer.send(dbQueue,tm);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='L' && m.charAt(2)=='T')
                loginSuccessful=true;
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return loginSuccessful;
    }

    @Override
    public ArrayList<String> getCustomerAccounts(String CID) {
        ArrayList<String> strList=new ArrayList<String>();
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();            
        JMSConsumer consumer = clientContext.createConsumer(clientQueue); 
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("C:"+CID);
            producer.send(dbQueue,tm);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='C'){
                String[] strs=m.substring(2).split("||");
                for (String str : strs){
                    strList.add(str);
                }
            }
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return strList;
    }

    @Override
    public boolean requestForHLAccountCreation(String CID) {
        boolean successful=false;
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();            
        JMSConsumer consumer = clientContext.createConsumer(clientQueue);  
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("H:"+CID);
            producer.send(dbQueue,tm);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='H' && m.charAt(2)=='T')
                successful=true;
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return successful;
    }

    @Override
    public boolean repayment(String customerID, String accountID, String homeloanID, double amount) {
        boolean successful=false;
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();            
        JMSConsumer consumer = clientContext.createConsumer(clientQueue); 
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("R:"+customerID+" "+accountID+" "+homeloanID+" "+amount);
            producer.send(dbQueue,tm);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='R' && m.charAt(2)=='T')
                successful=true;
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return successful;
    }

    @Override
    public double totalSavingsAccountsAmount() {
        double amount=0;
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();            
        JMSConsumer consumer = clientContext.createConsumer(clientQueue); 
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("I:investor data");
            producer.send(dbQueue,tm);
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='I')
                amount=Double.parseDouble(m.substring(2));
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return amount;
    }
}
