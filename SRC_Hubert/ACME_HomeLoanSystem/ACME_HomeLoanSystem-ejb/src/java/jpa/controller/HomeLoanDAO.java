/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Customerinfo;
import jpa.Homeloanaccounts;

/**
 *
 * @author JamesAlan
 */
public class HomeLoanDAO
{
    private EntityManagerFactory emf;
    
    public HomeLoanDAO()
    {
        emf = Persistence.createEntityManagerFactory("ACME_HomeLoanSystem-ejbPU");
    }
    
    public boolean persist(Homeloanaccounts newHL)
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try
        {
            em.persist(newHL);
            em.getTransaction().commit();
        } catch (Exception e)
        {
            em.getTransaction().rollback();
            System.out.println("Not persisted");
            return false;
        } finally
        {
            em.close();
        }
        
        System.out.println("persisted");
        return true;
    }

    public Homeloanaccounts buildHomeLoanAccount(Customerinfo cust, double amtBorrow)
    {
        Homeloanaccounts newHLAcc = new Homeloanaccounts();
        
        newHLAcc.setCId(cust);
        newHLAcc.setAmountborrowed(amtBorrow);
        newHLAcc.setAmountrepayed(0.0);
        
        return newHLAcc;
    }
    
    public Homeloanaccounts getHomeLoanAccount(int HL_Acc)
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Homeloanaccounts hlacc = em.find(Homeloanaccounts.class, HL_Acc);
        
        em.close(); 
        
//        return (cust != null) ?
//            String.format("%d\t%s\t%s\n",
//                cust.getCId(),
//                cust.getEmail(),
//                cust.getPreferredcontact()
//            ) :
//            "-- not found --";
//      
        return hlacc;
    }
    
    public Collection<Homeloanaccounts> getCustomerHomeLoanAccounts(int C_ID)
    {
        Collection<Homeloanaccounts> hlCollection;
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        hlCollection = em.find(Customerinfo.class, C_ID).getHomeloanaccountsCollection();
        
        em.close(); 
        
        return hlCollection;
    }
}
