/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Customerinfo;

/**
 *
 * @author JamesAlan
 */
public class CustomerInfoDAO
{
    private EntityManagerFactory emf;
    
    public CustomerInfoDAO()
    {
        emf = Persistence.createEntityManagerFactory("ACME_HomeLoanSystem-ejbPU");
    }
    
    public boolean persist(Customerinfo newCust)
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try
        {
            em.persist(newCust);
            em.getTransaction().commit();
        } catch (Exception e)
        {
            em.getTransaction().rollback();
            System.out.println("Not persisted");
            return false;
        } finally
        {
            em.close();
        }
        
        System.out.println("persisted");
        return true;
    }

    public Customerinfo getCustomerInfo(int C_ID)
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Customerinfo cust = em.find(Customerinfo.class, C_ID);
        
        em.close(); 
        
//        return (cust != null) ?
//            String.format("%d\t%s\t%s\n",
//                cust.getCId(),
//                cust.getEmail(),
//                cust.getPreferredcontact()
//            ) :
//            "-- not found --";
//      
        return cust;
    }
}
