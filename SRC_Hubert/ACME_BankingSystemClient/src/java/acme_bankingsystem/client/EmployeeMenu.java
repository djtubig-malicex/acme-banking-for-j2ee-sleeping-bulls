package acme_bankingsystem.client;

import beans.ACMEEmployeeSessionBeanRemote;
import data_access.DataConstants;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.ejb.NoSuchEJBException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Main Employee administration menu
 * @author JamesAlan, Chris
 */
public class EmployeeMenu 
{
    //@EJB
    private ACMEEmployeeSessionBeanRemote employeeBean;
    
    private int empID;
    private String displayName;
    
    EmployeeMenu(String E_ID, String PIN) 
    {
        empID = Integer.parseInt(E_ID.substring(1)); 
        employeeBean = lookupACMEEmployeeSessionBeanRemote();
        displayName = employeeBean.applyAuthenticationResult(E_ID, PIN);
    }
    
    private ACMEEmployeeSessionBeanRemote lookupACMEEmployeeSessionBeanRemote()
    {
        try 
        {
            Context c = new InitialContext();
            return (ACMEEmployeeSessionBeanRemote) c.lookup("java:comp/env/ACMEEmployeeSessionBean");
            //return (ACMEEmployeeSessionBeanRemote) c.lookup("java:global/ACME_BankingSystem-ejb/ACMEEmployeeSessionBean");
        }
        
        catch (NamingException ne) 
        {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
    
    /*************************************************************************************
     *                          USER MENU FUNCTIONS
     **************************************************************************************/
    
    /**
     * Menu printout function.  Also will run checkBeanState()
     */
    public void displayMenu()
    {
        System.out.println("");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("                        Main Menu");
        System.out.printf("   Logged in as E%07d - %s\n", empID, displayName);
        System.out.printf("   No. operations: %d of %d\n", employeeBean.getOperations(), DataConstants.EMPLOYEE_OPERATION_LIMIT);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("");
        System.out.println("1. Create new customer account");
        System.out.println("2. Open Savings Account");
        System.out.println("3. Make deposit into savings account");
        System.out.println("4. Make withdrawal from savings account");
        System.out.println("5. View balance of savings account");
        System.out.println("6. View transactions of account");
        System.out.println("7. View all customers");
        System.out.println("8. View all employees");
        System.out.println("9. View all accounts");
        System.out.println("10. View all transactions");
        System.out.println("(0) Exit System");
        System.out.println("");
    }
    
    public void startMenu()
    {
        Scanner in = new Scanner(System.in);
        String inputOpt;
        int intOpt;
        boolean cont = true;
        
        while (cont)
        {
            displayMenu();
            System.out.print("Enter option: ");
            
            try 
            {
                inputOpt = in.nextLine();
                intOpt = Integer.parseInt(inputOpt);
                
                // Always check if exceeding operation limit after instruction
                checkBeanState();
                
                // check if operation is legal
                switch (intOpt)
                {
                    case 1 : createCustomer();
                        break;
                    case 2 : openSavingsAccount();
                        break;
                    case 3 : makeDeposit();
                        break;
                    case 4 : makeWithdrawal();
                        break;
                    case 5 : viewAccountBalance();
                        break;
                    case 6 : viewAccountTransactions();
                        break;
                    case 7 : viewAllCustomers();
                        break;
                    case 8 : viewAllEmployees();
                        break;
                    case 9 : viewAllAccounts();
                        break;
                    case 10 : viewAllTransactions();
                        break;
                    case 0 : cont = false;
                        break;
                    default : System.out.println("Invalid option");
                        break;
                }
                
                //if (intOpt == 0) return;
            } 
            
            catch (NumberFormatException nfe)
            {
                System.out.print("Please enter only integer values specified in the menu");
            }
            
            catch (IllegalStateException ise)
            {
                System.out.print("Input stream in unexpected state");
            }
        }
    }
    
    
    public void createCustomer()
    {
        String firstName, lastName, address, pin;
        Date dob;
        Scanner in = new Scanner(System.in);
        System.out.println("\nCreating a new Customer account");
        
        // get FirstName
        System.out.print("Enter the Customer's First Name: ");
        if ((firstName = ACMEClientUtility.getNameFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        
        // get Lastname
        System.out.print("Enter the Customer's Last Name: ");
        if ((lastName = ACMEClientUtility.getNameFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // get address
        System.out.print("Address: ");
        if ((address = in.nextLine()).trim().isEmpty()) return;
        employeeBean.pingMe();
        
        // get dob (dd-mm-yyyy)
        System.out.print("Date of Birth (dd-mm-yyyy): ");
        if ((dob = ACMEClientUtility.getDateFromUser()) == null) return;
        employeeBean.pingMe();
        
        // get pin
        System.out.print("Provide client PIN: ");
        if((pin = in.nextLine()).isEmpty()) return;
        employeeBean.pingMe();
        
        // call business method on bean
        String CID=employeeBean.addCustomer(firstName, lastName, dob, address, pin);
        if (CID.equals(""))
            System.out.println("Unable to add Customer account!");
        else
            System.out.printf("New Customer %s created!\n",CID);
    }
    
    public void openSavingsAccount()
    {
        String C_ID, pin;
        Scanner in = new Scanner(System.in);
        
        System.out.println("\nCreate new Savings account");
        
        // get C_ID
        if((C_ID = ACMEClientUtility.getCustomerIDFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // get customer pin
        System.out.print("Enter PIN: ");
        if ((pin = in.nextLine()).isEmpty()) return;
        
        // verify customer
        if (employeeBean.readCustomer(C_ID, pin).isEmpty())
        {
            System.out.println("Invalid customer details.");
            return;
        }
        
        // call business method on bean
        String AID=employeeBean.addAccount(C_ID);
        if (AID.equals(""))
            System.out.println("Unable to create Account!");
        else
            System.out.printf("Account %s created!\n",AID);
    }
    
    public void makeDeposit()
    {
        String C_ID, pin, accNum;
        Scanner in = new Scanner(System.in);
        double amt;

        System.out.println("\nMake a deposit");

        // get customer id
        if ((C_ID = ACMEClientUtility.getCustomerIDFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // get customer pin
        System.out.print("Enter PIN: ");
        if ((pin = in.nextLine()).isEmpty()) return;
        
        // verify customer
        if (employeeBean.readCustomer(C_ID, pin).isEmpty())
        {
            System.out.println("Invalid customer details.");
            return;
        }
        
        // get account id
        if ((accNum = ACMEClientUtility.getAccountNumberFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // get amount
        System.out.print("Deposit amount: ");        
        if ((amt = ACMEClientUtility.getDoubleFromUser()) <= 0)
        {
            System.out.println("Invalid input");
            return;
        }
        employeeBean.pingMe();
        
        // call business method on bean
        if (!employeeBean.depositMoney(C_ID, accNum, amt))
            System.out.println("Deposit failed!");
        else
            System.out.println("Deposit successful!");

    }
    
    public void makeWithdrawal()
    {
        String C_ID, accNum, pin;
        double amt;
        Scanner in = new Scanner(System.in);

        System.out.println("\nMake a withdrawal");
        
        // get customer id
        if ((C_ID = ACMEClientUtility.getCustomerIDFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // get customer pin
        System.out.print("Enter PIN: ");
        if ((pin = in.nextLine()).isEmpty()) return;
        
        // verify customer
        if (employeeBean.readCustomer(C_ID, pin).isEmpty())
        {
            System.out.println("Invalid customer details.");
            return;
        }
        
        // get account id
        if ((accNum = ACMEClientUtility.getAccountNumberFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // get amount
        System.out.print("Withdraw amount: ");        
        if ((amt = ACMEClientUtility.getDoubleFromUser()) <= 0)
        {
            System.out.println("Invalid input");
            return;
        }
        employeeBean.pingMe();
         
        // call business method on bean
        if (!employeeBean.withdrawMoney(C_ID, accNum, amt))
            System.out.println("Withdraw failed! Check your account info / balance");
        else
            System.out.println("Withdraw successful!");
    }
    
    /**
     * View account balance of a Customer's assigned account.
     */
    public void viewAccountBalance()
    {
        String accNum, C_ID, pin;
        double amt;
        Scanner in = new Scanner(System.in);
        
        // get customer id
        if((C_ID = ACMEClientUtility.getCustomerIDFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // get customer pin
        System.out.print("Enter PIN: ");
        if ((pin = in.nextLine()).isEmpty()) return;
        
        // verify customer
        if (employeeBean.readCustomer(C_ID, pin).isEmpty())
        {
            System.out.println("Invalid customer details.");
            return;
        }
        
        // get account id
        if((accNum = ACMEClientUtility.getAccountNumberFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // call business method on bean
        if((amt = employeeBean.getAccountBalance(C_ID, accNum)) >= 0)
            System.out.printf("\nYour Account Balance is: $%.2f\n", amt);
        else
            System.out.print("Invalid account details.");
    }
    
    /**
     * OTHER FUNCTIONS : not covered as requirements in assignment specs
     *  implementations already provided
     */
    
    /**
     * Debug function for viewing entire Accounts table
     */
    public void viewAllAccounts()
    {
        ArrayList<String> accs;
        
        // call business method on bean
        accs = employeeBean.getAllAccounts();
     
        System.out.println("\nAll Accounts");
        System.out.println("============");
        
        if (accs.isEmpty())
            System.out.println("<No Accounts in System>");
        else
        {
            System.out.printf("%-11s|%-10s|%-20s|\n", 
                "AccNum", "C_ID", "Balance"    
            );
            
            for (String s : accs)
            {
                String tok[] = s.split(",");
                System.out.printf("%11s|%10s|$%19.2f|\n",
                    tok[0], tok[1], Double.parseDouble(tok[2])
                );
            }
        }
    }
    
    /** 
     * Debug function for viewing entire Customers table
     *
     */
    public void viewAllCustomers()
    {
        ArrayList<String> custs;
        
        // call business method on bean
        custs = employeeBean.getAllCustomers();

        System.out.println("\nAll Customers");
        System.out.println("=============");
        
        if (custs.isEmpty())
            System.out.println("<No Customers in System>");
        else
        {
            System.out.printf("%-10s|%-20s|%-30s|%-10s|%-10s|\n", 
                "C_ID", "First + Last Name", "Address", "DOB", "PIN"    
            );
            
            for (String s : custs)
            {
                String tok[] = s.split(",");
                System.out.printf("%10s|%20s|%30s|%10s|%10s|\n",
                    tok[0], tok[1]+" "+tok[2], tok[3], tok[4], tok[5]
                );
            }
        }
    }
    
    /** 
     * Debug function for viewing entire Employees table
     */
    public void viewAllEmployees()
    {
        ArrayList<String> emps;
        
        // call business method on bean
        emps = employeeBean.getAllEmployees();
        
        System.out.println("\nAll Employees");
        System.out.println("=============");

        if (emps.isEmpty())
            System.out.println("<No Employees in System>");
        else
        {
            System.out.printf("%-10s|%-20s|%-30s|%-10s|%-10s|%-10s|\n", 
                "E_ID", "First + Last Name", "Address", "DOB", "Employedat", "PIN"    
            );
            
            for (String s : emps)
            {
                String tok[] = s.split(",");
                System.out.printf("%10s|%20s|%30s|%10s|%10s|%10s|\n",
                    tok[0], tok[1]+" "+tok[2], tok[3], tok[4], tok[5], tok[6]
                );
            }
        }
    }
    
    /** 
     * Debug function for viewing entire Transaction table
     * 
     * Developer notes from Chris W:
     *     "I don't even know what i was thinking with this... 
     *      viewTransactionHistory(E_ID/accNum) makes more sense"
     */
    public void viewAllTransactions()
    {
        ArrayList<String> transactions;
        
        // call business method on bean
        transactions = employeeBean.getAllTransactions();

        System.out.println("\nAll Transactions");
        System.out.println("=============");

        if (transactions.isEmpty())
            System.out.println("<No Transactions in System>");
        else
        {
            System.out.printf("%-12s|%-10s|%-10s|%-10s|%-20s|%-12s|\n", 
                "T_ID", "E_ID", "AccNum", "T_TIME", "Description", "Amount"    
            );
            
            for (String s : transactions)
            {
                String tok[] = s.split(",");
                System.out.printf("%12s|%10s|%10s|%10s|%20s|$%11.2f|\n",
                    tok[0], tok[1], tok[2], tok[3], tok[4], Double.parseDouble(tok[5])
                );
            }
        }
    }
    
    /**
     * Used to check if at any time the session has exceeded 10 successful
     * transactional operations, to force the 
     */
    public int checkBeanState()
    {
        int operations = employeeBean.getOperations();
        
        if (operations >= DataConstants.EMPLOYEE_OPERATION_LIMIT)
        {
            employeeBean.removeBean();
            employeeBean = null;
            System.out.println("You have exceeded the maximum number of operations. Take a rest break and close the application.");
            throw new NoSuchEJBException();
        }
        
        return operations;
    }

    public void viewAccountTransactions() 
    {
        String accNum, C_ID, pin;
        ArrayList<String> transactions;
        Scanner in = new Scanner(System.in);
        
        // get customer id
        if((C_ID = ACMEClientUtility.getCustomerIDFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // get customer pin
        System.out.print("Enter PIN: ");
        if ((pin = in.nextLine()).isEmpty()) return;
        
        // verify customer
        if (employeeBean.readCustomer(C_ID, pin).isEmpty())
        {
            System.out.println("Invalid customer details.");
            return;
        }
        
        // get account id
        if((accNum = ACMEClientUtility.getAccountNumberFromUser()).isEmpty()) return;
        employeeBean.pingMe();
        
        // call business method on bean
        if((transactions = employeeBean.getTransactions(C_ID, accNum,
            new Date(0), new Date(System.currentTimeMillis()))).isEmpty())
            System.out.println("<No Transactions found for the given details>");
        else
        {
            System.out.printf("%-12s|%-10s|%-12s|%-10s|%-20s|%-12s|\n", 
                "T_ID", "E_ID", "AccNum", "T_TIME", "Description", "Amount"    
            );
            
            for (String s : transactions)
            {
                String tok[] = s.split(",");
                System.out.printf("%12s|%10s|%12s|%10s|%20s|$%11.2f|\n",
                    tok[0], tok[1], tok[2], tok[3], tok[4], Double.parseDouble(tok[5])
                );
            }
        }
        
    }
    
}
