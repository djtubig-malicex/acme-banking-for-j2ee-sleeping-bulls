package acme_bankingsystem.client;

import data_access.DataConstants;
import java.sql.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Utility Functions
 * 
 * @author JamesAlan, Chris
 */
public class ACMEClientUtility 
{
    public static String getCustomerIDFromUser()
    {
        String C_ID;
        Scanner in = new Scanner(System.in);
        
        System.out.print("Enter customer ID: ");
        C_ID = in.nextLine().trim().toUpperCase();
        
        if (C_ID.length() < 2 || C_ID.charAt(0) != DataConstants.CUSTOMER_TYPE)
        {
            System.out.println("Not a valid customer ID");
            return "";
        }
        
        return C_ID;
    }

    public static String getEmployeeIDFromUser()
    {
        String E_ID;
        Scanner in = new Scanner(System.in);
        
        System.out.print("Enter Employee ID: ");
        E_ID = in.nextLine().trim().toUpperCase();
        
        if (E_ID.length() < 2 || E_ID.charAt(0) != DataConstants.EMPLOYEE_TYPE)
        {
            System.out.println("Not a valid employee ID");
            return "";
        }
        
        return E_ID;
    }
    
    public static String getAccountNumberFromUser()
    {
        String accNum;
        Scanner in = new Scanner(System.in);
        
        System.out.print("Enter Account Number: ");
        accNum = in.nextLine().trim().toUpperCase();
        
        if (accNum.length() < 2 || accNum.charAt(0) != DataConstants.ACCOUNT_TYPE)
        {
            System.out.println("Not a valid Account Number");
            return "";
        }
        
        return accNum;
    }
    
    public static String getTransactionID()
    {
        String T_ID;
        Scanner in = new Scanner(System.in);
        
        System.out.print("Enter employee ID: ");
        T_ID = in.nextLine().trim().toUpperCase();
        
        if (T_ID.length() < 2 || T_ID.charAt(0) != DataConstants.TRANSACTION_TYPE)
        {
            System.out.println("Not a valid Transaction ID");
            return "";
        }
        
        return T_ID;
    }
    
    /**
     * Takes as input from the command line a date in the form dd-mm-yyyy and returns a
     * Date object
     *
     * @return java.sql.Date representing the date entered by the user, or null if input
     * was invalid
     */
    public static Date getDateFromUser()
    {
        String input;
        Scanner in = new Scanner(System.in);
        Date date;
        GregorianCalendar cal;
        int year, month, day;
        
        input = in.nextLine();
        String[] tokens = input.split("-");
        
        try 
        {
            if (tokens.length != 3)
                throw new NumberFormatException();
            
            // split components to numeric variables
            day = Integer.parseInt(tokens[0]);
            month = Integer.parseInt(tokens[1]);
            year = Integer.parseInt(tokens[2]);
            
            // Validate that input date is indeed dd-mm-yyyy format
            if (month > 12 || day > 31 || year < 0 || month < 0 || day < 0 ||
                tokens[0].length() != 2 || tokens[1].length() != 2 ||
                tokens[2].length() != 4)
                throw new NumberFormatException();
            
            cal = new GregorianCalendar(year, month, day);
            
        } 
        
        catch (NumberFormatException nfe)
        {
            System.out.println("Please enter date as integers only in the form dd-mm-yyyy");
            return null;
        }
        
        date = new Date(cal.getTimeInMillis());
        return date;
    }

    /**
     * Prompts the user for a double from the command line and returns the value as
     * a double
     *
     * @return the double value representation of the users input, returns 0 on invalid input
     */
    public static double getDoubleFromUser()
    {
        String input;
        Scanner in = new Scanner(System.in);
        input = in.nextLine();
        
        try 
        {
            return Double.parseDouble(input);
        }
        
        catch (NumberFormatException nfe) 
        {
            System.out.println("Not a valid number.");
            return 0.0;
        }
    }

    /**
     * Prompts the user for a integer from the command line and returns the value as
     * an integer
     *
     * @return the integer value representation of the users input, returns 0 on invalid input
     */
    public static int getIntFromUser()
    {
        String input;
        Scanner in = new Scanner(System.in);
        
        input = in.nextLine();
        
        try 
        {
            return Integer.parseInt(input);
        }
        
        catch (NumberFormatException nfe) 
        {
            System.out.println("Not a valid integer.");
            return ~0;
        }
    }
    
    /**
     * Utility class to enforce basic validation of name entry
     * @return a String representing a name, or blank string if invalid
     */
    public static String getNameFromUser()
    {
        Pattern p = Pattern.compile("[^a-zA-Z\\s\\-']");
        Scanner in = new Scanner(System.in);
        String str;
        
        if (p.matcher((str = in.nextLine().trim())).find())
        {
            System.out.println("Invalid name.");
            str = "";
        }
        return str;
    }
            
}
