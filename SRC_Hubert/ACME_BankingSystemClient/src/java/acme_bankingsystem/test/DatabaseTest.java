package acme_bankingsystem.test;

import beans._deprecated__ACMEPersistenceBeanRemote;
import java.sql.Date;
import java.util.ArrayList;
import javax.ejb.EJB;

/**
 * TODO: Refactor to only use CustomerSessionBean and EmployeeSessionBean
 * 
 * @author Hubert, JamesAlan
 */
public class DatabaseTest
{
    private static String currentCustID, currentEmpID;
    
    @EJB
    private static _deprecated__ACMEPersistenceBeanRemote acmeSessionBean;
    
    public static boolean dbTest1()
    {
        ArrayList<String> strs;
        
        System.out.println("Running database interfacing test...");
        
        System.out.println("Adding a customer...");
        acmeSessionBean.addCustomer("First Name", "Last Name", 
            new Date(System.currentTimeMillis()), "Address", "1234");
        
        System.out.println("List all customers...");
        strs = acmeSessionBean.getAllCustomers();
        for (String s:strs)
        {
            currentCustID = s.split(",")[0];
            System.out.println(s);
        }
        
        System.out.println("Adding an employee...");
        acmeSessionBean.addEmployee("First Name", "Last Name", 
            new Date(System.currentTimeMillis()), "Address", "1234");
        
        System.out.println("List all employees...");
        strs = acmeSessionBean.getAllEmployees();
        for (String s:strs)
        {
            currentEmpID = s.split(",")[0];
            System.out.println(s);
        }
        
        System.out.println("Updating a customer...");
        acmeSessionBean.updateCustomer(currentCustID,"Quack", "Quack",
            new Date(System.currentTimeMillis()), "Address", "1234");
        
        System.out.println("List all customers...");
        strs = acmeSessionBean.getAllCustomers();
        for (String s:strs)
            System.out.println(s);
        
        System.out.println("Updating an employee...");
        acmeSessionBean.updateEmployee(currentEmpID,"Mak", "Mak", new Date(System.currentTimeMillis()), "Address", "1234");
        
        System.out.println("List all employees...");
        strs = acmeSessionBean.getAllEmployees();
        for (String s:strs)
            System.out.println(s);
        
        return true;
    }
    
    public static boolean dbTest2()
    {
        ArrayList<String> strs;
        
        System.out.println("Adding two accounts...");
        acmeSessionBean.addAccount(currentCustID);
        acmeSessionBean.addAccount(currentCustID);
        
        System.out.println("List all accounts...");
        strs = acmeSessionBean.getAllAccounts();
        for (String s:strs)
            System.out.println(s);
        
        System.out.println("Add one too many account");
        acmeSessionBean.addAccount(currentCustID);
        System.out.println("List all accounts... (should not have three)");
        strs=acmeSessionBean.getAllAccounts();
        for (String s:strs)
            System.out.println(s);
        
        return true;
    }
    
    public static boolean dbTest3()
    {
        // TODO transaction tests
        ArrayList<String> strs;
        System.err.println("NOTE: This test is not finished!");
        
        // TODO acount tests
        System.out.println("Deposit $100 into first account...");
        acmeSessionBean.depositMoney(currentCustID, "A0000000001", 100);
        
        System.out.println("Deposit $1 into second account...");
        acmeSessionBean.depositMoney(currentCustID, "A0000000001", 100);
        
        System.out.println("Withdraw $10 from second account... (should fail)");
        acmeSessionBean.withdrawMoney(currentCustID, "A0000000002", 10);
        
        for(int i = 0; i < 10; ++i)
        {
            System.out.println("Withdraw $1 from first account...ten times");
            acmeSessionBean.withdrawMoney(currentCustID, "A0000000001", 1);
        }
        
        System.out.println("List transactions...");
        strs = acmeSessionBean.getAllAccounts();
        for (String s:strs)
            System.out.println(s);
        
        return false;
    }
    
    
    public static boolean dbTest4()
    {
        ArrayList<String> strs;
        
        System.err.println("NOTE: This test is not finished!");
        
        // TODO When deleting customers, should their transactions and accounts
        // also be deleted?
        System.out.println("Deleting a customer...");
        acmeSessionBean.deleteCustomer(currentCustID);
        
        System.out.println("List all customers... (should not show " + currentCustID +" on list)");
        strs = acmeSessionBean.getAllCustomers();
        for (String s:strs)
        {
            System.out.println(s);
            if (s.split(",")[0].equals(currentCustID)) 
                return false;
        }
        System.out.println("Deleting an employee...");
        acmeSessionBean.deleteEmployee(currentEmpID);
        
        System.out.println("List all employees... (should not show " + currentEmpID + ")");
        strs = acmeSessionBean.getAllEmployees();
        for (String s:strs)
            System.out.println(s);
        
        return true;
    }
    
    public static void main(String args[])
    {
        System.out.printf("RESULTS:\n "
            + "Customers and Employees: %s\n"
            + "Accounts: %s\n"
            + "Transactions: %s\n"
            + "Teardown: %s\n",

            (dbTest1()) ? "PASS" : "FAIL",
            (dbTest2()) ? "PASS" : "FAIL",
            (dbTest3()) ? "PASS" : "FAIL",
            (dbTest4()) ? "PASS" : "FAIL");
    }
}
