/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.ejb.Remote;

/**
 *
 * @author Hubert
 */
@Remote
public interface ACMELoginBeanRemote 
{
    
    /**
     * Authentication function used for client frontend.
     * @param ID
     * @param PIN
     * @return true if authentication is valid. 
     */
    boolean login(String ID, String PIN);
    
}
