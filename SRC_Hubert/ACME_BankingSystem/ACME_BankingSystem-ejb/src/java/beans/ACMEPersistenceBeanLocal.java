/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import data_access.orm.Account;
import data_access.orm.Customer;
import data_access.orm.Employee;
import data_access.orm.Transaction;
import java.sql.Date;
import java.util.ArrayList;
import javax.ejb.Local;

/**
 * Persistence Bean which will only ever be accessed through other wrapper beans
 * @author Hubert, JamesAlan
 */
@Local
public interface ACMEPersistenceBeanLocal {

    public String addCustomer(String firstname, String lastname, Date dob, String address, String PIN);
    public String addAccount(String C_ID) throws IllegalAccessException;
    public String addEmployee(String firstname, String lastname, Date dob, String address, Date doe, String PIN);
    
    public void updateCustomer(String C_ID,String firstname, String lastname, Date dob, String address, String PIN);
    public void updateEmployee(String E_ID,String firstname, String lastname, Date dob, String address, Date doe, String PIN);
    
    public void deleteCustomer(String C_ID);
    public void deleteEmployee(String E_ID);
    
    /**
     * 
     * @return ArrayList of String representations of each Customer 
     */
    public ArrayList<String> getAllCustomers();
    
    /**
     * 
     * @return ArrayList of String representations of each Employee
     */
    public ArrayList<String> getAllEmployees();
    
    /**
     * 
     * @return ArrayList of String representations of each Account
     */
    public ArrayList<String> getAllAccounts();
    public ArrayList<Account> getAllAccountObjects();
    
    /**
     * 
     * @return ArrayList of String representations of a Customer's Account(s)
     */
    public ArrayList<String> getCustomerAccounts(String C_ID);
    public ArrayList<Account> getCustomerAccountObjects(String C_ID);

    /**
     * Get any transactions between date
     * @return ArrayList of Transactions
     */
    public ArrayList<String> getTransactionsBetweenDate(Date startDate, Date endDate);
    
    /**
     * Get an account's transactions between date
     * 
     * @return ArrayList of Transactions
     */
    public ArrayList<String> getTransactionsByAccount(String C_ID, String accNum, Date startDate, Date endDate);
    public ArrayList<Transaction> getTransactionsByCustomer(String C_ID);
    /**
     * Get an employee's transactions between date
     * @return ArrayList of Transactions
     */
    public ArrayList<String> getTransactionsByEmployee(String E_ID, Date startDate, Date endDate);
    
    public boolean depositMoney(String E_ID, String C_ID, String accNum, double amt);
    public boolean withdrawMoney(String E_ID, String C_ID, String accNum, double amt);
    public boolean repayMoney(String C_ID, String accNum, String HL_Acc, double amt);
    
    public double getAccountBalance(String C_ID, String accNum);
    
    public Customer parseCustomer(String representation);
    public Employee parseEmployee(String representation);

    public Employee getEmployee(String E_ID, String PIN);

    public Customer getCustomer(String C_ID, String PIN);
    public boolean login(String ID, String PIN); 

    public int getCustomerDepositCount(String customerID);

    public Customer getCustomer(String customerID);
}
