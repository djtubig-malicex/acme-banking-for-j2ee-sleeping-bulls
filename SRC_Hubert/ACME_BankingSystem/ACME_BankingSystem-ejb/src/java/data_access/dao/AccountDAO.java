package data_access.dao;

import data_access.orm.Customer;
import data_access.orm.Account;
import java.util.ArrayList;

/**
 *
 * @author JamesAlan
 */
public interface AccountDAO
{
    String createAccount(Account account) throws IllegalAccessException;
    Account readAccount(String C_ID, String accNum) throws IllegalAccessException;
    void updateAccount(Account account) throws IllegalAccessException;
    void deleteAccount(Account account) throws IllegalAccessException;
    double getAccountBalance(String C_ID, String accNum);
    ArrayList<Account> getCustomerAccounts(Customer c) throws IllegalAccessException;
    ArrayList<Account> getCustomerAccounts(String CID) throws IllegalAccessException;
    ArrayList<Account> getAllAccounts() throws IllegalAccessException;
}
