package data_access.dao;

import data_access.DataConstants;
import data_access.orm.Customer;
import data_access.orm.Account;
import data_access.orm.DataFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JamesAlan
 */
public class RDBAccountDAO implements AccountDAO
{
    /*
     * SQL QUERIES
     */
    private static final String 
        SQL_DELETE_CUSTOMER_ACCOUNT = "DELETE FROM ACME.ACCOUNTS WHERE ACCNUM=(?)",
        SQL_GET_CUSTOMER_ACCOUNT_NUM = "SELECT COUNT(ACCNUM) FROM ACME.ACCOUNTS WHERE C_ID=(?)",
        SQL_ADD_NEW_ACCOUNT = "INSERT INTO ACME.ACCOUNTS(C_ID, BALANCE) VALUES (?, ?)",
        //SQL_ADD_NEW_ACCOUNT = "INSERT INTO ACME.ACCOUNTS(C_ID, BALANCE) VALUES (?, ?) (SELECT COUNT(ACCNUM) AS NUMACCS FROM ACME.ACCOUNTS WHERE C_ID=(?) AND NUMACCS<(?))",
        SQL_SELECT_ACCOUNT_BY_ID = "SELECT ACCNUM, C_ID, BALANCE FROM ACME.ACCOUNTS WHERE ACCNUM=(?) AND C_ID=(?)",
        SQL_SELECT_ACCOUNTS_BY_CID = "SELECT ACCNUM, C_ID, BALANCE FROM ACME.ACCOUNTS WHERE C_ID=(?)",
        SQL_UPDATE_ACCOUNT = "UPDATE ACME.ACCOUNTS SET BALANCE=(?) WHERE ACCNUM=(?)",
        SQL_GET_ALL_ACCOUNTS = "SELECT ACCNUM, C_ID, BALANCE FROM ACME.ACCOUNTS";
    
    private Connection dbConnection = null;
    
    public RDBAccountDAO(Connection connection) 
    {
        this.dbConnection = connection;
    }
    
    @Override
    public String createAccount(Account account) throws IllegalAccessException
    {
        String generatedID=""; 
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement
            (
                SQL_GET_CUSTOMER_ACCOUNT_NUM
            );
            sqlStatement.setInt(1, account.getNumericOwnerID());
            sqlStatement.execute();
            ResultSet result = sqlStatement.getResultSet();
            result.next();
            
            if (result.getInt(1) >= DataConstants.MAX_CUSTOMER_ACCOUNTS) 
                throw new IllegalAccessException("The customer has created 2 account already!!");
            
            sqlStatement = dbConnection.prepareStatement
            (
                SQL_ADD_NEW_ACCOUNT,
                Statement.RETURN_GENERATED_KEYS
            );
            sqlStatement.setInt(1, account.getNumericOwnerID());
            sqlStatement.setDouble(2, account.getBalance());
            // sqlStatement.setInt(3, account.getNumericOwnerID());
            // sqlStatement.setInt(4, DataConstants.MAX_CUSTOMER_ACCOUNTS);
            sqlStatement.executeUpdate();
            result = sqlStatement.getGeneratedKeys();
            result.next();
            generatedID=String.format(DataConstants.IDTYPE_TEN, DataConstants.ACCOUNT_TYPE,result.getInt(1));
            account.setID(result.getInt(1));
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not add new account.");
            sqlException.printStackTrace();
            throw new IllegalAccessException();
        }
        
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
            if (ex instanceof IllegalAccessException)
                throw ex;
            else throw new IllegalAccessException("Could not add new account.");
            
        }
        return generatedID;
    }

    @Override
    public Account readAccount(String C_ID, String accNum)
    {
        Account accountObj = null;
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_SELECT_ACCOUNT_BY_ID
            );
            sqlStatement.setInt(1, Integer.valueOf(accNum.substring(1)));
            sqlStatement.setInt(2, Integer.parseInt(C_ID.substring(1)));
            sqlStatement.execute();
            
            ResultSet result = sqlStatement.getResultSet();
            result.next();
            
            accountObj = parseAccount(result);
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not add new account.");
            sqlException.printStackTrace();
        } 
        
        catch (IllegalAccessException ex) 
        {
            //Logger.getLogger(RDBAccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            //Should not go here
        }
        
        return accountObj;
    }

    @Override
    public void updateAccount(Account account) 
    {
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement
            (
                SQL_UPDATE_ACCOUNT
            );
            sqlStatement.setDouble(1, account.getBalance());
            sqlStatement.setInt(2, account.getNumericID());
            sqlStatement.executeUpdate();
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not update account " + account.getID());
            sqlException.printStackTrace();
        }
    }
    
    @Override
    public void deleteAccount(Account account) 
    {
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_DELETE_CUSTOMER_ACCOUNT
            );
            sqlStatement.setInt(1, account.getNumericID());
            //sqlStatement.setInt(2, account.getNumericOwnerID());
            sqlStatement.executeUpdate();
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not delete account." + account.getID());
            sqlException.printStackTrace();
        }
    }
    
    /**
     * Private method that retrieves all associated accounts
     * belonging to a Customer.
     * 
     * Note: Business rules for the implementation state that any single
     *       Customer should have at most, two accounts.
     * 
     * @param C_ID the associated Customer ID
     * @return ArrayList collection containing at most two Account instances
     */
    private ArrayList<Account> getCustomerAccounts(int C_ID) 
    {
        ArrayList<Account> accountList = null;
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement
            (
                SQL_SELECT_ACCOUNTS_BY_CID
            );
            
            sqlStatement.setInt(1, C_ID);
            sqlStatement.execute();
            ResultSet result = sqlStatement.getResultSet();
            
            accountList = readAccountResults(result);
        }
        
        catch (IllegalAccessException ex) 
        {
            // Should not go here
            
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not read account list of customer " 
                    + C_ID);
            sqlException.printStackTrace();
        }
        
        return accountList; 
    }
    
    
    @Override
    public ArrayList<Account> getCustomerAccounts(Customer c) 
    {
        return getCustomerAccounts(c.getNumericID());
    }
    
    @Override
    public ArrayList<Account> getAllAccounts() 
    {
        ArrayList<Account> accountList = null;
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_GET_ALL_ACCOUNTS
            );
            
            sqlStatement.execute();
            ResultSet result = sqlStatement.getResultSet();
            
            accountList = readAccountResults(result);
        }
        
        catch (IllegalAccessException ex) 
        {
            // Should not go here
            
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not read entire account list");
            sqlException.printStackTrace();
        }
        
        return accountList;
    }
    
    /**
     * Private helper method for collecting several Accounts to a List type
     * Note: Should only be used in conjunction with a query that is expected 
     *       to output several rows.
     * 
     * @param result is a ResultSet instance
     * @return ArrayList collection of Accounts
     * @throws SQLException 
     */
    private ArrayList<Account> readAccountResults(ResultSet result)
        throws SQLException, IllegalAccessException
    {
        ArrayList accountList = new ArrayList<>();
            
        while(result.next())
            accountList.add(parseAccount(result));
        
        return accountList;
    }
    
    private Account parseAccount(ResultSet result) throws SQLException,
        IllegalAccessException
    {
        return DataFactory.buildAccount(
            String.format(DataConstants.IDTYPE_TEN, DataConstants.ACCOUNT_TYPE, result.getInt("ACCNUM")),
            String.format(DataConstants.IDTYPE_SEVEN, DataConstants.CUSTOMER_TYPE, result.getInt("C_ID")),
            result.getDouble("BALANCE")
        );
    }

    @Override
    public ArrayList<Account> getCustomerAccounts(String C_ID) throws IllegalAccessException
    {
        if (C_ID.charAt(0) != DataConstants.ACCOUNT_TYPE) throw new IllegalAccessException();
        
        return getCustomerAccounts(Integer.parseInt(C_ID.substring(1)));
    }

    @Override
    public double getAccountBalance(String C_ID, String accNum) 
    {
        return readAccount(C_ID, accNum).getBalance();
    }
}
