package data_access.dao;

import data_access.orm.Transaction;
import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author JamesAlan
 */
public interface TransactionDAO
{
    void createTransaction(Transaction transaction);
    void deleteTransaction(int TID);
    
    ArrayList<Transaction> getTransactions(String ID);
    public ArrayList<String> getTransactionsBetweenDate(Date startDate, Date endDate);

    public ArrayList<String> getTransactionsByAccount(String C_ID, String accNum, Date startDate, Date endDate);

    public ArrayList<String> getTransactionsByEmployee(String E_ID, Date startDate, Date endDate);

    public int getNumDeposits(String C_ID);
    
}
