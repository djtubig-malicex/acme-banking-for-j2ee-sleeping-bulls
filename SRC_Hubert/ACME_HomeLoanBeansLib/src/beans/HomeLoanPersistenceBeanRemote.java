/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.sql.Date;
import java.util.ArrayList;
import javax.ejb.Remote;
import jpa.Customerinfo;
import jpa.Homeloanaccounts;

/**
 *
 * @author JamesAlan
 */
@Remote
public interface HomeLoanPersistenceBeanRemote
{
    public boolean createCustomerInfo(int C_ID, String phone, String email, String companyName, String companyAddr,
            char employType, int hoursPerWeek, String jobTitle, double salary, Date employmentDate, 
            char preferredContact);
    public boolean createHomeLoanAccount(Customerinfo cust, double amtBorrow);
    public boolean makeRepayment(int HL_Acc, double amt, int accNum);
    public Homeloanaccounts getHomeLoan(int HL_Acc);
    public ArrayList<String> getHomeLoanAccounts(String C_ID);

    public Customerinfo getCustomerInfo(int C_ID);

    public boolean setCustomerInfo(Customerinfo cust);
}
