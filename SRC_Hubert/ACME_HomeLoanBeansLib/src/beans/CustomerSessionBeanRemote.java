/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import javax.ejb.Remote;
import jpa.Customerinfo;

/**
 *
 * @author Hubert
 */
@Remote
public interface CustomerSessionBeanRemote 
{
    public boolean login(String CID, String pw);
    public boolean openNewHomeLoan(Customerinfo cust, double amtBorrow);

    public Customerinfo getCustomerInfo(String id);

    public boolean setCustomerInfo(Customerinfo cust);
}
