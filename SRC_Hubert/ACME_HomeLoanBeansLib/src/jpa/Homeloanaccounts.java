/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JamesAlan
 */
@Entity
@Table(name = "HOMELOANACCOUNTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Homeloanaccounts.findAll", query = "SELECT h FROM Homeloanaccounts h"),
    @NamedQuery(name = "Homeloanaccounts.findByHlAcc", query = "SELECT h FROM Homeloanaccounts h WHERE h.hlAcc = :hlAcc"),
    @NamedQuery(name = "Homeloanaccounts.findByAmountborrowed", query = "SELECT h FROM Homeloanaccounts h WHERE h.amountborrowed = :amountborrowed"),
    @NamedQuery(name = "Homeloanaccounts.findByAmountrepayed", query = "SELECT h FROM Homeloanaccounts h WHERE h.amountrepayed = :amountrepayed")})
public class Homeloanaccounts implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "HL_ACC")
    private Integer hlAcc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AMOUNTBORROWED")
    private Double amountborrowed;
    @Column(name = "AMOUNTREPAYED")
    private Double amountrepayed;
    @OneToMany(mappedBy = "tohlAcc")
    private Collection<Homeloantransactions> homeloantransactionsCollection;
    @JoinColumn(name = "C_ID", referencedColumnName = "C_ID")
    @ManyToOne
    private Customerinfo cId;

    public Homeloanaccounts() {
    }

    public Homeloanaccounts(Integer hlAcc) {
        this.hlAcc = hlAcc;
    }

    public Integer getHlAcc() {
        return hlAcc;
    }

    public void setHlAcc(Integer hlAcc) {
        this.hlAcc = hlAcc;
    }

    public Double getAmountborrowed() {
        return amountborrowed;
    }

    public void setAmountborrowed(Double amountborrowed) {
        this.amountborrowed = amountborrowed;
    }

    public Double getAmountrepayed() {
        return amountrepayed;
    }

    public void setAmountrepayed(Double amountrepayed) {
        this.amountrepayed = amountrepayed;
    }

    @XmlTransient
    public Collection<Homeloantransactions> getHomeloantransactionsCollection() {
        return homeloantransactionsCollection;
    }

    public void setHomeloantransactionsCollection(Collection<Homeloantransactions> homeloantransactionsCollection) {
        this.homeloantransactionsCollection = homeloantransactionsCollection;
    }

    public Customerinfo getCId() {
        return cId;
    }

    public void setCId(Customerinfo cId) {
        this.cId = cId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hlAcc != null ? hlAcc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Homeloanaccounts)) {
            return false;
        }
        Homeloanaccounts other = (Homeloanaccounts) object;
        if ((this.hlAcc == null && other.hlAcc != null) || (this.hlAcc != null && !this.hlAcc.equals(other.hlAcc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.Homeloanaccounts[ hlAcc=" + hlAcc + " ]";
    }
    
}
