-- DB Name: ACME_LENDERS
-- IMPORTANT: USERNAME AND PASSWORD MUST BE SET TO ACME
-- map to acmeLendersConnectionPool and JNDI: jdbc/acmeLendersDatasource

drop table ACME.HomeLoanTransactions;
drop table ACME.HomeLoanAccounts;
drop table ACME.CustomerInfo;
drop schema ACME restrict;

create schema ACME;

create table ACME.CustomerInfo
(
   C_ID int primary key,
   phone varchar(10),
   email varchar(64),
   companyName varchar(32),
   companyAddr varchar(80),
   employType char,
   hoursPerWeek int,
   jobTitle varchar(32),
   salary double,
   employmentDate date,
   preferredContact char
);

create table ACME.HomeLoanAccounts
(
   HL_Acc int not null primary key generated always as identity (start with 1, increment by 1),
   C_ID int,
   AmountBorrowed double, 
   AmountRepayed double,
   foreign key (C_ID) references ACME.CustomerInfo(C_ID)
   --foreign key (AccNum) references ACME.Accounts(AccNum)
);

create table ACME.HomeLoanTransactions
(
   HLT_ID int not null primary key generated always as identity (start with 1, increment by 1),
   FromAccNum int,
   ToHL_Acc int,
   Amount double,
   TransDate date,
   
   foreign key (ToHL_Acc) references ACME.HomeLoanAccounts(HL_Acc)
);
