/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package acme_bankingsystem.client;

import beans.ACMECustomerSessionBeanRemote;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * NOTE: This class is not used
 * 
 * @author JamesAlan
 */
public class CustomerMenu 
{
    //@EJB
    private ACMECustomerSessionBeanRemote customerBean;
    
    private String displayName;
    private int custID;

    CustomerMenu(String C_ID, String PIN) 
    {
        // TODO save loaded customer string
        custID = Integer.parseInt(C_ID.substring(1));
        customerBean = lookupACMECustomerSessionBeanRemote();
        displayName = customerBean.applyAuthenticationResult(C_ID, PIN);
    }
    
    private ACMECustomerSessionBeanRemote lookupACMECustomerSessionBeanRemote()
    {
        try 
        {
            Context c = new InitialContext();
            //return (ACMECustomerSessionBeanRemote) c.lookup("java:global/ACME_BankingSystem-ejb/ACMECustomerSessionBean");
            return (ACMECustomerSessionBeanRemote) c.lookup("java:comp/env/ACMECustomerSessionBean");
        } 
        
        catch (NamingException ne)
        {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
    
    /*************************************************************************************
     *                          USER MENU FUNCTIONS
     **************************************************************************************/
    
    public void displayMenu()
    {
        System.out.println("");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("                        Main Menu");
        System.out.printf("   Logged in as C%07d - %s\n", custID, displayName);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("");
        System.out.println("1. Make deposit into savings account");
        System.out.println("2. Make withdrawal from savings account");
        System.out.println("3. View balance of savings account");
        System.out.println("4. View Transaction history");
        System.out.println("5. Update your customer details");
        System.out.println("(0) Exit System");
        System.out.println("");
    }
    
    public void startMenu()
    {
        Scanner in = new Scanner(System.in);
        String inputOpt;
        int intOpt;
        boolean cont = true;
        
        while (cont)
        {
            displayMenu();
            System.out.print("Enter option: ");
            
            try 
            {
                inputOpt = in.nextLine();
                intOpt = Integer.parseInt(inputOpt);
                
                // check if operation is legal
                
                switch (intOpt)
                {
                    case 1 : openSavingsAccount();
                        break;
                    case 2 : makeDeposit();
                        break;
                    case 3 : makeWithdrawal();
                        break;
                    case 4 : viewAccountBalance();
                        break;
                    case 5 : viewTransactionHistory();
                        break;
                    case 6 : updateDetails();
                        break;
                    case 0 : cont = false;
                        break;
                    default : System.out.println("Invalid option");
                        break;
                }
                
                //if (intOpt == 0) return;
                
            } 
            
            catch (NumberFormatException nfe)
            {
                System.out.print("Please enter only integer values specified in the menu");
            }
            
            catch (IllegalStateException ise)
            {
                System.out.print("Input stream in unexpected state");
            }
        }
    }
    
    public void openSavingsAccount()
    {
        String C_ID;
        Scanner in = new Scanner(System.in);
        
        System.out.println("\nCreate new Savings account");
        
        // get C_ID
        System.out.print("Enter customer ID: ");
        C_ID = in.nextLine();
        
        // call business method on bean
        String AID=customerBean.addAccount(C_ID);
        if (AID.equals(""))
        {
            System.out.println("Unable to create Account!");
        }
        else 
        {
            
            System.out.printf("Account %s created!\n",AID);
        }

    }
    
    public void makeDeposit()
    {
        String C_ID, accNum;
        double amt;
        Scanner in = new Scanner(System.in);

        System.out.println("\nMake a deposit");

        // get customer id
        System.out.print("Enter customer ID: ");
        C_ID = in.nextLine();
        
        // get account id
        System.out.print("Enter account ID: ");
        accNum = in.nextLine();
        
        // get amount
        System.out.print("Deposit amount: ");        
        if ((amt = ACMEClientUtility.getDoubleFromUser()) == 0)
        {
            System.out.println("Invalid input");
            return;
        }
        
        // call business method on bean
        if (!customerBean.depositMoney(C_ID, accNum, amt))
        {
            System.out.println("Deposit failed!");
        }
        else
        {

            System.out.println("Deposit successful!");
        }
    }
    
    public void makeWithdrawal()
    {
        String C_ID, accNum;
        double amt;
        Scanner in = new Scanner(System.in);

        System.out.println("\nMake a withdrawal");
        
        // get customer id
        System.out.print("Enter customer ID: ");
        C_ID = in.nextLine();
        
        // get account id
        System.out.print("Enter account ID: ");
        accNum = in.nextLine();
        
        // get amount
        System.out.print("Withdraw amount: ");        
        if ((amt = ACMEClientUtility.getDoubleFromUser()) == 0)
        {
            System.out.println("Invalid input");
            return;
        }
            
        // call business method on bean
        if (!customerBean.withdrawMoney(C_ID, accNum, amt))
        {
            System.out.println("Withdraw failed! Check your account info / balance");
        }
        
        else
        {

            System.out.println("Withdraw successfull!");
        }
        
    }
    
    public void viewAccountBalance()
    {
        String accNum;
        Scanner in = new Scanner(System.in);
        double amt;
        
        // get account id
        System.out.print("\nEnter account ID: ");
        accNum = in.nextLine();
        
        try
        {
            // call business method on bean
            amt = customerBean.getAccountBalance(accNum);
            System.out.printf("Account Balance: $%.2f\n",amt);
        }
        
        catch (IllegalArgumentException iae)
        {
            System.err.println(iae);
        }
    }
    
    /**
     * OTHER FUNCTIONS : not covered as requirements in assignment specs
     *  implementations already provided
     */
    
    /**
     * Sub-menu which allows users to check their transaction history
     */
    public void viewTransactionHistory()
    {
        // TODO
    }
    
    /** I don't even know what i was thinking with this... viewTransactionHistory(E_ID/ accNum) makes
     * more sense
     */
    public void viewAllTransactions()
    {
        System.out.println("Not Implemented");
    }
    
    /*
    public void checkBeanState()
    {
        if (customerBean.getOperations() >= 10)  
        {
            customerBean.removeBean();
            customerBean=null;
        }
    }
    */
    
    /**
     * 
     */
    private void updateDetails() 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
