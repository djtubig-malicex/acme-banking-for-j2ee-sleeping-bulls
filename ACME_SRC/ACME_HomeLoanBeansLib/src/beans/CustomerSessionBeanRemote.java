/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.Collection;
import javax.ejb.Remote;
import jpa.Customerinfo;
import jpa.Homeloanaccounts;

/**
 *
 * @author Hubert
 */
@Remote
public interface CustomerSessionBeanRemote 
{
    public boolean login(String CID, String pw);
    public boolean openNewHomeLoan(Customerinfo cust, double amtBorrow);

    public Customerinfo getCustomerInfo(String id);

    public boolean setCustomerInfo(Customerinfo cust);
    
    public Collection<Homeloanaccounts> getHomeLoanAccounts(String CID);

    public boolean isPaidOff(String hlAcc);

    public void repayMoney(String id, String accNum, String hlAcc, double amount);
}
