package data_access.orm;

import data_access.DataConstants;
import java.sql.Date;

/**
 * Factory class for instantiating ORM object instances
 * 
 * @author JamesAlan
 */
public class DataFactory
{
   public static Account buildEmptyAccount(String C_ID) throws IllegalAccessException
   {
       if (C_ID.charAt(0) != DataConstants.CUSTOMER_TYPE)
           throw new IllegalAccessException();
       
       return new Account(Integer.parseInt(C_ID.substring(1)));
   }
    
   public static Account buildAccount(String accNum, String C_ID,
       double balance) throws IllegalAccessException
   {
       if (C_ID.charAt(0) != DataConstants.CUSTOMER_TYPE ||
           accNum.charAt(0) != DataConstants.ACCOUNT_TYPE)
           throw new IllegalAccessException();
       
       return new Account(accNum, C_ID, balance);
   }
   
   public static Customer buildEmptyCustomer(String fName, String lName,
       Date dob, String address, String PIN)
   {
       return new Customer(fName, lName, dob, address, PIN);
   }
   
   public static Customer buildCustomer(String C_ID, String fName, String lName,
       Date dob, String address, String PIN) throws IllegalAccessException
   {
       if (C_ID.charAt(0) != DataConstants.CUSTOMER_TYPE)
           throw new IllegalAccessException();
       
       return new Customer(Integer.parseInt(C_ID.substring(1)),
               fName, lName, dob, address, PIN);
   }
   
   public static Employee buildEmptyEmployee(String fName, String lName,
       Date dob, String address, Date doe, String PIN)
   {
       return new Employee(fName, lName, dob, address, doe, PIN);
   }
   
   public static Employee buildEmployee(String E_ID, String fName, String lName,
       Date dob, String address, Date doe, String PIN) throws IllegalAccessException
   {
       if (E_ID.charAt(0) != DataConstants.EMPLOYEE_TYPE)
           throw new IllegalAccessException();
       
       return new Employee(Integer.parseInt(E_ID.substring(1)), fName, lName,
           dob, address, doe, PIN);
   }
   
   public static Transaction buildEmptyTransaction(String E_ID, String accNum,
       double amount, String description) throws IllegalAccessException
   {
       if (E_ID.charAt(0) != DataConstants.EMPLOYEE_TYPE ||
           accNum.charAt(0) != DataConstants.ACCOUNT_TYPE)
           throw new IllegalAccessException();
       
       return new Transaction(Integer.parseInt(E_ID.substring(1)), 
           Integer.parseInt(accNum.substring(1)), amount, description);
   }
   
   public static Transaction buildRepayTransaction(String accNum,
       double amount, String description) throws IllegalAccessException
   {
       if (accNum.charAt(0) != DataConstants.ACCOUNT_TYPE)
           throw new IllegalAccessException();
       
       return new Transaction(Integer.parseInt(accNum.substring(1)), amount, description);
   }
   
   public static Transaction buildEmptyTransaction(String E_ID, String accNum,
       double amount, String description, Date transDate) throws IllegalAccessException
   {
       if (E_ID.charAt(0) != DataConstants.EMPLOYEE_TYPE ||
           accNum.charAt(0) != DataConstants.ACCOUNT_TYPE)
           throw new IllegalAccessException();
       
       return new Transaction(Integer.parseInt(E_ID.substring(1)), 
           Integer.parseInt(accNum.substring(1)), amount, description, transDate);
   }
   
   public static Transaction buildTransaction(String T_ID, String E_ID, String accNum,
       double amount, String description, Date transDate) throws IllegalAccessException
   {
       if (T_ID.charAt(0) != DataConstants.TRANSACTION_TYPE ||
           E_ID.charAt(0) != DataConstants.EMPLOYEE_TYPE ||
           accNum.charAt(0) != DataConstants.ACCOUNT_TYPE)
           throw new IllegalAccessException();
       
       return new Transaction(Integer.parseInt(T_ID.substring(1)),
           Integer.parseInt(E_ID.substring(1)), 
           Integer.parseInt(accNum.substring(1)), amount, description, transDate);
   }
}
