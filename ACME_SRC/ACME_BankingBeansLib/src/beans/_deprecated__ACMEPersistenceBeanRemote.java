/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.sql.Date;
import java.util.ArrayList;
import javax.ejb.Remote;

/**
 * NOTE: SET TO BE DELETED
 * 
 * @deprecated
 * @author Hubert
 */
@Remote
public interface _deprecated__ACMEPersistenceBeanRemote
{
    
    public boolean addCustomer(String firstname, String lastname, Date dob, String address, String PIN);
    public boolean addAccount(String C_ID);
    public boolean addEmployee(String firstname, String lastname, Date dob, String address, String PIN);
    
    public void updateCustomer(String C_ID,String firstname, String lastname, Date dob, String address, String PIN);
    public void updateEmployee(String E_ID,String firstname, String lastname, Date dob, String address, String PIN);
    
    public void deleteCustomer(String C_ID);
    public void deleteEmployee(String E_ID);
    
    /**
     * 
     * @return ArrayList of String representations of each Customer 
     */
    public ArrayList<String> getAllCustomers();
    
    /**
     * 
     * @return ArrayList of String representations of each Employee
     */
    public ArrayList<String> getAllEmployees();
    
    /**
     * 
     * @return ArrayList of String representations of each Account
     */
    public ArrayList<String> getAllAccounts();
    
    /**
     * 
     * @return ArrayList of String representations of a Customer's Account(s)
     */
    public ArrayList<String> getCustomerAccounts(String C_ID);

    public boolean depositMoney(String C_ID, String accNum, double amt);
    public boolean withdrawMoney(String C_ID, String accNum, double amt);
    
    public double getAccountBalance(String accNum)throws IllegalArgumentException;
   
}
