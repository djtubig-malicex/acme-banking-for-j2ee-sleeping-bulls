/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import data_access.dao.CustomerDAO;
import data_access.DataConstants;
import data_access.dao.EmployeeDAO;
import data_access.dao.RDBCustomerDAO;
import data_access.dao.RDBEmployeeDAO;
import data_access.orm.Customer;
import data_access.orm.Employee;
import java.sql.Connection;
import java.sql.SQLException;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.sql.DataSource;

/**
 *
 * @author Hubert, JamesAlan
 */
@Singleton
public class ACMELoginBean implements ACMELoginBeanRemote {
    @Resource(lookup = "jdbc/acmeDBDatasource")
    private DataSource dataSource;
    private Connection connection;
    
    @PostConstruct
    public void initialize() 
    {
        try 
        {
            connection = dataSource.getConnection();
        }

        catch (SQLException sqle) 
        {
            sqle.printStackTrace();
        }
    }

    @PreDestroy
    public void close()
    {
        try
        {
            connection.close();
        }

        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
    }
    
    /**
     * Login function after getting user input
     * 
     * @param ID a valid user ID
     * @param PIN a valid user password correspondent to the ID
     * @return true if valid details have been verified in the database
     */
    @Override
    public boolean login(String ID, String PIN) 
    {
        if(ID.length() < 2)
            return false;
        
        switch (ID.charAt(0))
        {
            case DataConstants.EMPLOYEE_TYPE:
                EmployeeDAO eDao = new RDBEmployeeDAO(connection);
                Employee employee = eDao.readEmployee(ID, PIN);
                return (employee == null) ? false : true;
                
            case DataConstants.CUSTOMER_TYPE:
                CustomerDAO cDao = new RDBCustomerDAO(connection);
                Customer customer = cDao.readCustomer(ID, PIN);
                return (customer == null) ? false : true;
                
            default:
                return false;
        }
    }
}
