/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;
 
import data_access.DataConstants;
import data_access.orm.Account;
import data_access.orm.Customer;
import data_access.orm.Transaction;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage; 

/**
 *
 * @author Hubert
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/DBQueue")
})
public class DBMessageBean implements MessageListener {
    @EJB
    private ACMEPersistenceBeanLocal acmePersistenceBean;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory queueFactory;
    @Resource(name="jms/ClientQueue")
    private Destination queue;
    
    public DBMessageBean() {
    }
    
    @Override
    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        Session session = null;
        Connection connection = null;
        String text;
        try {
            System.out.println("Received message:" + textMessage.getText());
            connection=queueFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(message.getJMSReplyTo());  
            Message replyMessage;
            
            text=textMessage.getText();
            char Type=text.charAt(0);
            text=text.substring(2);
            String[] strs;
            String reply,customerID;
            boolean success=false;
            switch (Type){
                case 'L'://Login: "L:userID password" return "L:[T,F]"
                    strs=text.split(" ");
                    success = acmePersistenceBean.login(strs[0], strs[1]);
                    reply=success?"T":"F";
                    replyMessage = session.createTextMessage("L:"+reply);                     
                    replyMessage.setJMSCorrelationID(message.getJMSMessageID());
                    producer.send(replyMessage); 
                    break;
                case 'C'://get customer's accounts: "C:customerID" return "C:accountID balance||accountID balance||"
                    customerID=text;
                    ArrayList<Account> customerAccList = acmePersistenceBean.getCustomerAccountObjects(customerID);
//                    StringBuilder sb=new StringBuilder();                  
//                    for (Account acc : customerAccList){
//                        sb.append(acc.getID()+" "+acc.getBalance()+"||");
//                    }
//                    reply=sb.toString();
//                    replyMessage = session.createTextMessage("C:"+reply);                     
                    replyMessage = session.createObjectMessage(customerAccList);
                    replyMessage.setJMSCorrelationID(message.getJMSMessageID());
                    producer.send(replyMessage); 
                    break;
                    
                case 'D'://get customer details
                    //strs=text.split(" ");
                    customerID=text;
                    //Customer cust = acmePersistenceBean.getCustomer(strs[0], strs[1]);
                    Customer cust = acmePersistenceBean.getCustomer(customerID);
                    replyMessage = (cust != null) ? 
                            session.createObjectMessage(cust) 
                          : session.createTextMessage("D:-- not found --");
                    //replyMessage = session.createTextMessage("C:"+reply);                     
                    replyMessage.setJMSCorrelationID(message.getJMSMessageID());
                    producer.send(replyMessage); 
                    
                    
                case 'H'://create homeloan account: "H:customerID" return "H:[T,F]"
                    customerID=text;
                    int numDeposits = acmePersistenceBean.getCustomerDepositCount(customerID);
                    reply=numDeposits>=DataConstants.MIN_DEPOSITS_TO_OPEN_HOMELOAN?"T":"F";
                    replyMessage = session.createTextMessage("H:"+reply);                     
                    replyMessage.setJMSCorrelationID(message.getJMSMessageID());
                    producer.send(replyMessage); 
                    break;
                    
                case 'R'://repayment: "R:customerID accountID homeloanID amount" return "R:[T,F]"
                    strs=text.split(" ");
                    double amount = acmePersistenceBean.getAccountBalance(strs[0], strs[1]);
                    double repayAmount=Double.parseDouble(strs[3]);
                    if (amount>=repayAmount){
                        success=acmePersistenceBean.repayMoney(strs[0], strs[1], strs[2], repayAmount);
                    }
                    reply=success?"T":"F";
                    replyMessage = session.createTextMessage("R:"+reply);                     
                    replyMessage.setJMSCorrelationID(message.getJMSMessageID());
                    producer.send(replyMessage); 
                    break;
                    
                case 'I'://investor data: "I:investor data" return "I:amount"
                    ArrayList<Account> accList = acmePersistenceBean.getAllAccountObjects();
                    double total=0;
                    for (Account acc : accList){
                        total+=acc.getBalance();
                    }
                    replyMessage = session.createTextMessage("I:"+total);                     
                    replyMessage.setJMSCorrelationID(message.getJMSMessageID());
                    producer.send(replyMessage); 
                    break;
                    
                default:
                    break;
            }            
            
        } catch (JMSException ex) {
            Logger.getLogger(DBMessageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
