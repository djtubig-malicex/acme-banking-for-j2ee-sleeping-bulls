/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import data_access.DataConstants;
import data_access.dao.AccountDAO;
import data_access.dao.CustomerDAO;
import data_access.dao.EmployeeDAO;
import data_access.dao.RDBAccountDAO;
import data_access.dao.RDBCustomerDAO;
import data_access.dao.RDBEmployeeDAO;
import data_access.dao.RDBTransactionDAO;
import data_access.dao.TransactionDAO;
import data_access.orm.Account;
import data_access.orm.Customer;
import data_access.orm.DataFactory;
import data_access.orm.Employee;
import data_access.orm.Transaction;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.sql.DataSource;

/**
 *
 * @author Hubert, JamesAlan
 */
@Singleton
public class ACMEPersistenceBean implements ACMEPersistenceBeanLocal 
{
    @Resource(lookup = "jdbc/acmeDBDatasource")
    private DataSource dataSource;
    private Connection connection;
    
    @PostConstruct
    public void initialize() 
    {        
        try 
        {
            connection = dataSource.getConnection();
        }
        
        catch (SQLException sqle) 
        {
            sqle.printStackTrace();
        }
    }
    
    @PreDestroy
    public void close()
    {
        try
        {
            connection.close();
        }
        
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
    }
    
    @Override
    @Lock(LockType.WRITE)
    public String addCustomer(String firstName, String lastName, Date dob, 
        String address, String PIN) 
    {
        CustomerDAO dao = new RDBCustomerDAO(connection);
        Customer customer = DataFactory.buildEmptyCustomer(
            firstName, lastName, dob, address, PIN);
        return dao.createCustomer(customer);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    @Override
    @Lock(LockType.WRITE)
    public void updateCustomer(String C_ID, String firstname, String lastname,
        Date dob, String address, String PIN)
    {
        try 
        {
            CustomerDAO dao = new RDBCustomerDAO(connection);
            Customer customer = DataFactory.buildCustomer(C_ID, firstname, 
                lastname, dob, address, PIN);
            dao.updateCustomer(customer);
        }
        
        catch (IllegalAccessException ex) 
        {
            System.err.println("Error building updated Customer object");
        }
    }

    @Override
    @Lock(LockType.WRITE)
    public String addEmployee(String firstname, String lastname, Date dob,
        String address, Date doe, String PIN) 
    {
        EmployeeDAO dao = new RDBEmployeeDAO(connection);
        Employee employee = DataFactory.buildEmptyEmployee(firstname, lastname,
                dob, address, doe, PIN);
        return dao.createEmployee(employee);
    }

    @Override
    @Lock(LockType.WRITE)
    public void updateEmployee(String E_ID, String firstName, String lastName,
        Date dob, String address, Date doe, String PIN)
    {
        try 
        {
            EmployeeDAO dao = new RDBEmployeeDAO(connection);
            dao.updateEmployee(DataFactory.buildEmployee(E_ID, firstName, 
                lastName, dob, address, doe, PIN));
        }
        
        catch (IllegalAccessException ex) 
        {
            System.err.println("Error building updated Employee object");
        }
    }

    @Override
    @Lock(LockType.WRITE)
    public void deleteCustomer(String C_ID)
    {
        CustomerDAO dao = new RDBCustomerDAO(connection);
        dao.deleteCustomer(C_ID);
    }

    @Override
    @Lock(LockType.WRITE)
    public void deleteEmployee(String E_ID)
    {
        EmployeeDAO dao = new RDBEmployeeDAO(connection);
        dao.deleteEmployee(E_ID);
    }

    @Override
    @Lock(LockType.READ)
    public ArrayList<String> getAllCustomers()
    {
        CustomerDAO dao = new RDBCustomerDAO(connection);
        ArrayList<Customer> customers;
        ArrayList<String> strs = new ArrayList<>();
        
        customers = dao.getAllCustomers();
        for (Customer c : customers)
            strs.add(c.toString());
        
        return strs;
    }
    
    @Override
    @Lock(LockType.READ)
    public ArrayList<String> getAllEmployees()
    {
        EmployeeDAO dao = new RDBEmployeeDAO(connection);
        ArrayList<Employee> employees;
        ArrayList<String> strs = new ArrayList<>();
        
        employees=dao.getAllEmployees();
        for (Employee e : employees)
            strs.add(e.toString());
        
        return strs;
    }
    
    @Override
    @Lock(LockType.WRITE)
    public String addAccount(String C_ID)
    {   String A_ID="";
        try 
        {
            AccountDAO accDAO = new RDBAccountDAO(connection);
            A_ID=accDAO.createAccount(DataFactory.buildEmptyAccount(C_ID));
        }
        
        catch (IllegalAccessException ex) 
        {
            System.err.println("Invalid customer ID, could not create Account");
            //Logger.getLogger(ACMEPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return A_ID;
    }

    @Override
    @Lock(LockType.READ)
    public ArrayList<String> getAllAccounts()
    {
        AccountDAO dao = new RDBAccountDAO(connection);
        ArrayList<Account> accounts;
        ArrayList<String> strs = new ArrayList<>();
                
        try
        {
             
            accounts = dao.getAllAccounts();
            for (Account a : accounts)
                strs.add(a.toString());
        }
        
        catch (IllegalAccessException ex) 
        {
            Logger.getLogger(ACMEPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return strs;
    }

    @Override
    @Lock(LockType.READ)
    public ArrayList<String> getCustomerAccounts(String C_ID) 
    {
        AccountDAO dao = new RDBAccountDAO(connection);
        ArrayList<Account> accounts;
        ArrayList<String> strs = new ArrayList<>();
            
        try 
        {    
            accounts = dao.getCustomerAccounts(C_ID);
            for (Account a : accounts)
                strs.add(a.toString());   
        } 
        
        catch (IllegalAccessException ex) 
        {
            System.err.println("Error occurred attempting to get Customer Accounts");
            Logger.getLogger(ACMEPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return strs;
    }

    /** Deposits 'amt' into the account 'accNum'
     * 
     * @param C_ID
     * @param accNum
     * @param amt
     * @return true on success, false if account could not be located.
     */
    @Override
    @Lock(LockType.WRITE)
    public boolean depositMoney(String E_ID, String C_ID, String accNum, double amt)
    {
        try 
        {
            AccountDAO accDAO = new RDBAccountDAO(connection);
            Account acc = accDAO.readAccount(C_ID, accNum);
            
            if (acc == null) throw new IllegalAccessException("Invalid account");
            acc.deposit(amt);
            accDAO.updateAccount(acc);
            
            // Add transaction
            Transaction trans = DataFactory.buildEmptyTransaction(E_ID, accNum, amt, "Deposit");
            TransactionDAO transDAO = new RDBTransactionDAO(connection);
            transDAO.createTransaction(trans);
        }
        
        catch (IllegalAccessException ex) 
        {
            Logger.getLogger(ACMEPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /** Withdraws 'amt' from 'accNum'
     * 
     * @param C_ID
     * @param accNum
     * @param amt
     * @return true on success, false if the account does not exist or 
     *  there is insufficient funds in the account
     */
    @Override
    @Lock(LockType.WRITE)
    public boolean withdrawMoney(String E_ID, String C_ID, String accNum, double amt)
    {        
        try
        {
            AccountDAO accDAO = new RDBAccountDAO(connection);
            Account acc = accDAO.readAccount(C_ID, accNum);
            
            acc.withdraw(amt);
            accDAO.updateAccount(acc);
            
            // Add transaction
            Transaction trans = DataFactory.buildEmptyTransaction(E_ID, accNum, amt, "Withdraw");
            TransactionDAO transDAO = new RDBTransactionDAO(connection);
            transDAO.createTransaction(trans);
            
        }
        
        catch (IllegalAccessException ex)
        {
            Logger.getLogger(ACMEPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /** Fetches the account balance for the given accNum.
     * 
     * @param C_ID customer ID associated with the account
     * @param accNum of the account to query
     * @return the current balance for the given account
     * @throws IllegalArgumentException when the accNum specified does not exist.
     */
    @Override
    @Lock(LockType.READ)
    public double getAccountBalance(String C_ID, String accNum)
    {
        Account account;
        AccountDAO dao = new RDBAccountDAO(connection);
        
        try
        {
            if ((account = dao.readAccount(C_ID, accNum)) == null || 
                account.getNumericOwnerID() != Integer.parseInt(C_ID.substring(1)))
            {
                // System.err.printf("The account with ID: %s was not found!\n", accNum);
                throw new IllegalAccessException("The specified Account Number is invalid");
            }
            return (account.getBalance());
        } 
        
        catch (IllegalAccessException ex) 
        {
            Logger.getLogger(ACMEPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
            return ~0;
        }
    }    

    @Override
    public Customer parseCustomer(String representation) 
    {
        return Customer.parseCustomer(representation);
    }

    @Override
    public Employee parseEmployee(String representation)
    {
        return Employee.parseEmployee(representation);
    }

    @Override
    public Employee getEmployee(String E_ID, String PIN) 
    {
        EmployeeDAO dao = new RDBEmployeeDAO(connection);
        return dao.readEmployee(E_ID, PIN);
    }

    @Override
    public Customer getCustomer(String C_ID, String PIN)
    {
        CustomerDAO dao = new RDBCustomerDAO(connection);
        return dao.readCustomer(C_ID, PIN);
    }

    @Override
    @Lock(LockType.READ)
    public ArrayList<String> getTransactionsBetweenDate(Date startDate, Date endDate) 
    {
        TransactionDAO dao = new RDBTransactionDAO(connection);
        return dao.getTransactionsBetweenDate(startDate, endDate);
    }

    @Override
    @Lock(LockType.READ)
    public ArrayList<String> getTransactionsByAccount(String C_ID, String accNum, Date startDate, Date endDate)
    {
        TransactionDAO dao = new RDBTransactionDAO(connection);
        return dao.getTransactionsByAccount(C_ID, accNum, startDate, endDate);
    }

    @Override
    @Lock(LockType.READ)
    public ArrayList<String> getTransactionsByEmployee(String E_ID, Date startDate, Date endDate) 
    {
        TransactionDAO dao = new RDBTransactionDAO(connection);
        return dao.getTransactionsByEmployee(E_ID, startDate, endDate);
    }
    
     /**
     * Login function after getting user input
     * 
     * @param ID a valid user ID
     * @param PIN a valid user password correspondent to the ID
     * @return true if valid details have been verified in the database
     */
    @Override
    public boolean login(String ID, String PIN) 
    {
        if(ID.length() < 2)
            return false;
        
        switch (ID.charAt(0))
        {
            case DataConstants.EMPLOYEE_TYPE:
                EmployeeDAO eDao = new RDBEmployeeDAO(connection);
                Employee employee = eDao.readEmployee(ID, PIN);
                return (employee == null) ? false : true;
                
            case DataConstants.CUSTOMER_TYPE:
                CustomerDAO cDao = new RDBCustomerDAO(connection);
                Customer customer = cDao.readCustomer(ID, PIN);
                return (customer == null) ? false : true;
                
            default:
                return false;
        }
    }

    @Override
    public ArrayList<Account> getAllAccountObjects() {
        AccountDAO dao = new RDBAccountDAO(connection);
        ArrayList<Account> accounts = new  ArrayList<Account>();
        try
        {             
            accounts = dao.getAllAccounts();
        }        
        catch (IllegalAccessException ex) 
        {
            Logger.getLogger(ACMEPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return accounts;
    }

    @Override
    public boolean repayMoney(String C_ID, String accNum, String HL_acc, double amt) {          
        try
        {
            AccountDAO accDAO = new RDBAccountDAO(connection);
            Account acc = accDAO.readAccount(C_ID, accNum);            
            acc.withdraw(amt);
            accDAO.updateAccount(acc);            
            // Add transaction
            Transaction trans = DataFactory.buildRepayTransaction(accNum, amt, "Repay "+HL_acc);
            // hack
            TransactionDAO transDAO = new RDBTransactionDAO(connection);
            transDAO.createTransaction(trans);            
        }
        
        catch (IllegalAccessException ex)
        {
            Logger.getLogger(ACMEPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    @Override
    public ArrayList<Transaction> getTransactionsByCustomer(String C_ID) {        
        TransactionDAO dao = new RDBTransactionDAO(connection);
        return dao.getTransactions(C_ID);
    }

    @Override
    public ArrayList<Account> getCustomerAccountObjects(String C_ID) {        
        AccountDAO dao = new RDBAccountDAO(connection);
        ArrayList<Account> accounts = new  ArrayList<>();         
        try 
        {    
            accounts = dao.getCustomerAccounts(C_ID);
        } 
        
        catch (IllegalAccessException ex) 
        {
            System.err.println("Error occurred attempting to get Customer Accounts");
            //Logger.getLogger(ACMEPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return accounts;
    }

    @Override
    public int getCustomerDepositCount(String C_ID) 
    {
        TransactionDAO dao = new RDBTransactionDAO(connection);
        return dao.getNumDeposits(C_ID);
    }

    @Override
    public Customer getCustomer(String C_ID) 
    {
        CustomerDAO dao = new RDBCustomerDAO(connection);
        return dao.readCustomer(C_ID);
    }
}
