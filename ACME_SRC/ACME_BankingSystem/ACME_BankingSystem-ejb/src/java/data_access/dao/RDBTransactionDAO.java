package data_access.dao;

import data_access.DataConstants;
import data_access.orm.DataFactory;
import data_access.orm.Transaction;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JamesAlan
 */
public class RDBTransactionDAO implements TransactionDAO
{
    /*
     * SQL QUERIES
     */
    private static final String
        SQL_GET_TRANSACTIONS_BETWEEN = "SELECT T_ID, E_ID, ACCNUM, AMOUNT, DESCRIPTION, T_TIME FROM ACME.TRANSACTIONS WHERE T_TIME BETWEEN (?) AND (?)",
        SQL_GET_TRANSACTIONS_BY_ACCOUNT = "SELECT T_ID, E_ID, ACCNUM, AMOUNT, DESCRIPTION, T_TIME FROM ACME.TRANSACTIONS WHERE ACCNUM=(?) AND T_TIME BETWEEN (?) AND (?) AND EXISTS (SELECT C_ID, ACCNUM FROM ACME.ACCOUNTS WHERE C_ID=(?) AND ACCNUM=(?))",
        SQL_GET_TRANSACTIONS_BY_CUSTOMER = "SELECT T_ID, E_ID, ACCNUM, AMOUNT, DESCRIPTION, T_TIME FROM ACME.TRANSACTIONS WHERE ACCNUM IN (SELECT ACCNUM FROM ACME.ACCOUNTS WHERE C_ID=(?))",
        SQL_GET_TRANSACTIONS_BY_EMPLOYEE = "SELECT T_ID, E_ID, ACCNUM, AMOUNT, DESCRIPTION, T_TIME FROM ACME.TRANSACTIONS WHERE E_ID=(?) AND T_TIME BETWEEN (?) AND (?)",
        SQL_GET_TRANSACTION_BY_ID = "SELECT T_ID, E_ID, ACCNUM, AMOUNT, DESCRIPTION, T_TIME FROM ACME.TRANSACTIONS WHERE T_ID=(?)",
        SQL_DELETE_TRANSACTION = /*"DELETE FROM ACME.TRANSACTIONS WHERE T_ID=(?) AND (E_ID=(?) OR "
                + " EXISTS (SELECT E_ID FROM ACME.EMPLOYEES WHERE "
                + "E_ID=(?)))"*/
            "DELETE FROM ACME.TRANSACTIONS WHERE T_ID=(?)",
        SQL_INSERT_TRANSACTION = "INSERT INTO ACME.TRANSACTIONS(E_ID, ACCNUM, AMOUNT, DESCRIPTION, T_TIME) VALUES (?, ?, ?, ?, ?)",
        SQL_COUNT_DEPOSITS = "SELECT COUNT(T_ID) AS NUM_DEPOSITS FROM ACME.TRANSACTIONS WHERE DESCRIPTION='Deposit' AND ACCNUM IN (SELECT ACCNUM FROM ACME.ACCOUNTS WHERE C_ID=(?))";
    
    private Connection dbConnection = null;
    
    public RDBTransactionDAO(Connection connection)
    {
        this.dbConnection = connection;
    }
    
    @Override
    public void createTransaction(Transaction transaction)
    {
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement
            (
                SQL_INSERT_TRANSACTION, 
                Statement.RETURN_GENERATED_KEYS
            );
            sqlStatement.setInt(1, transaction.getNumericAuthEmpID());
            sqlStatement.setInt(2, transaction.getNumericAccID());
            sqlStatement.setDouble(3, transaction.getTransactionAmt());
            sqlStatement.setString(4, transaction.getTransDesc());
            sqlStatement.setDate(5, transaction.getTransDate());
            sqlStatement.executeUpdate();
            
            ResultSet result = sqlStatement.getGeneratedKeys();
            result.next();
            
            transaction.setID(result.getInt(1));
            System.out.println(result);
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not add new employee.");
            sqlException.printStackTrace();
        } 
        
        catch (IllegalAccessException ex) 
        {
            // should not go here
            //Logger.getLogger(RDBTransactionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Delete a transaction record from the database.
     * Note: This action should only be executed by Employees
     * 
     * @param TID The associated Transaction ID to be removed
     * @param EID A valid ID of the authorising employee 
     */
    @Override
    public void deleteTransaction(int TID)
    {
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_DELETE_TRANSACTION,
                Statement.RETURN_GENERATED_KEYS
            );
            sqlStatement.setInt(1, TID);
//            sqlStatement.setInt(2, EID);
//            sqlStatement.setInt(3, EID);
            sqlStatement.executeUpdate();
//            ResultSet result = sqlStatement.getGeneratedKeys();
//            result.next();
//            System.out.println(result);
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not delete transaction " + TID);
            sqlException.printStackTrace();
        }
    }
    
    /**
     * TODO REFACTOR OR REMOVE
     * @deprecated
     * @param ID represents a String ID where the first character identifies
     *           the type of ID, while the 
     * @return ArrayList containing String representations of the transactions.
     */
    @Override
    public ArrayList<Transaction> getTransactions(String ID)
    {
        PreparedStatement sqlStatement;
        ArrayList<Transaction> transactionList = null;
        
        try
        {
            switch (ID.charAt(0))
            {
                case DataConstants.ACCOUNT_TYPE:
                    sqlStatement = dbConnection.prepareStatement(
                        SQL_GET_TRANSACTIONS_BY_ACCOUNT
                    );
                    break;

                case DataConstants.CUSTOMER_TYPE:
                    sqlStatement = dbConnection.prepareStatement(
                        SQL_GET_TRANSACTIONS_BY_CUSTOMER
                    );
                    break;

                case DataConstants.EMPLOYEE_TYPE:
                    sqlStatement = dbConnection.prepareStatement(
                        SQL_GET_TRANSACTIONS_BY_EMPLOYEE
                    );
                    break;

                case DataConstants.TRANSACTION_TYPE:
                    sqlStatement = dbConnection.prepareStatement(
                        SQL_GET_TRANSACTION_BY_ID
                    );
                    break;

                default:
                    throw new IllegalAccessException();
            }

            sqlStatement.setInt(1, Integer.parseInt(ID.substring(1)));
            sqlStatement.execute();

            ResultSet result = sqlStatement.getResultSet();
            transactionList = new ArrayList<>();

            while(result.next())
                transactionList.add(parseTransaction(result));
        }
        
        catch (SQLException sqlEx)
        {
            System.err.println("Could not retrieve transaction list");
        }
        
        catch (IllegalAccessException ex)
        {
            System.err.println("Error parsing an ID string");
        }
        
        return transactionList;
    }
    
    private Transaction parseTransaction(ResultSet result) throws IllegalAccessException, SQLException
    {
        return DataFactory.buildTransaction(
            String.format(DataConstants.IDTYPE_SEVEN, DataConstants.TRANSACTION_TYPE, result.getInt("T_ID")),
            String.format(DataConstants.IDTYPE_SEVEN, DataConstants.EMPLOYEE_TYPE, result.getInt("E_ID")),
            String.format(DataConstants.IDTYPE_TEN, DataConstants.ACCOUNT_TYPE, result.getInt("ACCNUM")),
            result.getDouble("AMOUNT"),
            result.getString("DESCRIPTION"),
            result.getDate("T_TIME")
        );   
    }

    @Override
    public ArrayList<String> getTransactionsBetweenDate(Date startDate, Date endDate) 
    {
        //ArrayList<Transaction> transactionList;
        ArrayList<String> strs = null;
            
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement
            (
                SQL_GET_TRANSACTIONS_BETWEEN
            );
            sqlStatement.setDate(1, startDate);
            sqlStatement.setDate(2, endDate);
            sqlStatement.execute();
            
            ResultSet result = sqlStatement.getResultSet();
            //transactionList = new ArrayList<>();
            strs = new ArrayList<>();
            
            while(result.next())
                strs.add(parseTransaction(result).toString());
            
        } 
        
        catch (SQLException ex) 
        {
            System.err.println("Failed to retrieve transaction list");
            Logger.getLogger(RDBTransactionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        catch (IllegalAccessException ex)
        {
            System.err.println("Failed to retrieve transaction list");
            Logger.getLogger(RDBTransactionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return strs;
    }

    @Override
    public ArrayList<String> getTransactionsByAccount(String C_ID, String accNum, Date startDate, Date endDate) 
    {
        //ArrayList<Transaction> transactionList;
        ArrayList<String> strs = null;  
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement
            (
                SQL_GET_TRANSACTIONS_BY_ACCOUNT
            );
            sqlStatement.setInt(1, Integer.parseInt(accNum.substring(1)));
            sqlStatement.setDate(2, startDate);
            sqlStatement.setDate(3, endDate);
            sqlStatement.setInt(4, Integer.parseInt(C_ID.substring(1)));
            sqlStatement.setInt(5, Integer.parseInt(accNum.substring(1)));
            sqlStatement.execute();
            
            ResultSet result = sqlStatement.getResultSet();
            //transactionList = new ArrayList<>();
            strs = new ArrayList<>();
            
            while(result.next())
                strs.add(parseTransaction(result).toString());
            
        } 
        
        catch (SQLException ex) 
        {
            System.err.println("Failed to retrieve transaction list");
            Logger.getLogger(RDBTransactionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        catch (IllegalAccessException ex)
        {
            System.err.println("Failed to retrieve transaction list");
            Logger.getLogger(RDBTransactionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return strs;
    }

    @Override
    public ArrayList<String> getTransactionsByEmployee(String E_ID, Date startDate, Date endDate)
    {
        //ArrayList<Transaction> transactionList;
        ArrayList<String> strs = null;  
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement
            (
                SQL_GET_TRANSACTIONS_BY_EMPLOYEE
            );
            sqlStatement.setInt(1, Integer.parseInt(E_ID.substring(1)));
            sqlStatement.setDate(2, startDate);
            sqlStatement.setDate(3, endDate);
            sqlStatement.execute();
            
            ResultSet result = sqlStatement.getResultSet();
            //transactionList = new ArrayList<>();
            strs = new ArrayList<>();
            
            while(result.next())
                strs.add(parseTransaction(result).toString());
            
        } 
        
        catch (SQLException ex) 
        {
            System.err.println("Failed to retrieve transaction list");
            Logger.getLogger(RDBTransactionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        catch (IllegalAccessException ex)
        {
            System.err.println("Failed to retrieve transaction list");
            Logger.getLogger(RDBTransactionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return strs;
    }

    @Override
    public int getNumDeposits(String C_ID) 
    {
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement
            (
                SQL_COUNT_DEPOSITS
            );
            sqlStatement.setInt(1, Integer.parseInt(C_ID.substring(1)));
            sqlStatement.execute();
            
            ResultSet result = sqlStatement.getResultSet();
            result.next();
            
            return result.getInt("NUM_DEPOSITS");
        }
        
        catch (SQLException ex)
        {
            System.err.println("Failed to obtain transaction count");
            Logger.getLogger(RDBTransactionDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
}
