package data_access.dao;

import data_access.DataConstants;
import data_access.orm.Customer;
import data_access.orm.DataFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JamesAlan
 */
public class RDBCustomerDAO implements CustomerDAO 
{
    private Connection dbConnection = null;

    private static final String
        SQL_SELECT_CUSTOMER = "SELECT C_ID, FIRSTNAME, LASTNAME, DOB, ADDRESS, PIN FROM ACME.CUSTOMERS WHERE C_ID=(?)",
        SQL_SELECT_ALL_CUSTOMERS = "SELECT C_ID, FIRSTNAME, LASTNAME, DOB, ADDRESS, PIN FROM ACME.CUSTOMERS",
        SQL_READ_AND_LOGIN_CUSTOMER = "SELECT C_ID, FIRSTNAME, LASTNAME, DOB, ADDRESS, PIN FROM ACME.CUSTOMERS WHERE C_ID=(?) AND PIN=(?)",
        SQL_CREATE_NEW_CUSTOMER = "INSERT INTO ACME.CUSTOMERS(FIRSTNAME, LASTNAME, DOB, ADDRESS, PIN) VALUES (?, ?, ?, ?, ?)",
        SQL_UPDATE_CUSTOMER = "UPDATE ACME.CUSTOMERS SET FIRSTNAME=(?), LASTNAME=(?), DOB=(?), ADDRESS=(?), PIN=(?) WHERE C_ID=(?)",
        SQL_DELETE_CUSTOMER = "DELETE FROM ACME.CUSTOMERS WHERE C_ID=(?)";
    
    public RDBCustomerDAO(Connection connection) 
    {
        this.dbConnection = connection;
    }
    
    @Override
    public String createCustomer(Customer customer)
    {
        String generatedID="";
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_CREATE_NEW_CUSTOMER, 
                Statement.RETURN_GENERATED_KEYS
            );
            sqlStatement.setString(1, customer.getFirstName());
            sqlStatement.setString(2, customer.getLastName());
            sqlStatement.setDate(3, customer.getDateOfBirth());
            sqlStatement.setString(4, customer.getAddress());
            sqlStatement.setString(5, customer.getPIN());
            sqlStatement.executeUpdate();
            ResultSet result = sqlStatement.getGeneratedKeys();
            result.next();
            generatedID=String.format(DataConstants.IDTYPE_SEVEN, DataConstants.CUSTOMER_TYPE,result.getInt(1));            
            customer.setID(result.getInt(1));
            dbConnection.commit();
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not add new customer.");
            sqlException.printStackTrace();
            //dbConnection.rollback();
        }
        
        catch (IllegalAccessException ex)
        {
            // should not go here
        }
        
        return generatedID;
    }

    @Override
    public Customer readCustomer(String CID)
    {
        Customer customerObj = null;
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_SELECT_CUSTOMER
            );
            sqlStatement.setInt(1, Integer.parseInt(CID.substring(1)));
            sqlStatement.execute();
            ResultSet result = sqlStatement.getResultSet();
            result.next();
            
            customerObj = parseCustomer(result);
            
            //System.out.println(result);
        }
        
        catch (SQLException sqlException)
        {
            System.err.println("Could not read the customer.");
            sqlException.printStackTrace();
        } 
        
        catch (IllegalAccessException ex) 
        {
            // Should not go here
        }
        
        return customerObj;
    }

    @Override
    public void updateCustomer(Customer customer) 
    {
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_UPDATE_CUSTOMER, 
                Statement.RETURN_GENERATED_KEYS
            );
            sqlStatement.setString(1, customer.getFirstName());
            sqlStatement.setString(2, customer.getLastName());
            sqlStatement.setDate(3, customer.getDateOfBirth());
            sqlStatement.setString(4, customer.getAddress());
            sqlStatement.setString(5, customer.getPIN());
            sqlStatement.setInt(6, customer.getNumericID());
            sqlStatement.executeUpdate();
            //System.out.println(result);
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not update customer." + customer.getID());
            sqlException.printStackTrace();
        }
    }

    @Override
    public void deleteCustomer(String CID) 
    {
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement
            (
                SQL_DELETE_CUSTOMER
            );
            sqlStatement.setInt(1, Integer.valueOf(CID.substring(1)));
            sqlStatement.executeUpdate();
            sqlStatement.closeOnCompletion();
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not delete customer." + CID);
            sqlException.printStackTrace();
        }
    }
    
    /**
     * Retrieve all existing Customers in the database as a collection.
     * 
     * @return ArrayList collection of all Customer instances 
     */
    @Override
    public ArrayList<Customer> getAllCustomers()
    {
        ArrayList<Customer> customerList = null;
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_SELECT_ALL_CUSTOMERS
            );
            sqlStatement.execute();
            ResultSet result = sqlStatement.getResultSet();
            
            customerList = new ArrayList<>();
            
            while(result.next())
                customerList.add(parseCustomer(result));
            
        }
        
        catch (SQLException sqlException)
        {
            System.out.println("Could not read customers.");
            sqlException.printStackTrace();
        } 
        
        catch (IllegalAccessException ex)
        {
            // Should not go here
        }
        
        return customerList;
    }

    private Customer parseCustomer(ResultSet result) throws SQLException, IllegalAccessException
    {
        return DataFactory.buildCustomer(
            String.format(DataConstants.IDTYPE_SEVEN, DataConstants.CUSTOMER_TYPE,result.getInt("C_ID")),
            result.getString("FIRSTNAME"),
            result.getString("LASTNAME"),
            result.getDate("DOB"),
            result.getString("ADDRESS"),
            result.getString("PIN")
        );
    }

    @Override
    public Customer readCustomer(String ID, String PIN) {
        Customer customerObj = null;
        
        try 
        {
            PreparedStatement sqlStatement = dbConnection.prepareStatement(
                SQL_READ_AND_LOGIN_CUSTOMER,
                Statement.RETURN_GENERATED_KEYS
            );
            
            // print what was parsed
            System.out.printf("ID = %s", ID);
            System.out.printf("PIN = %s", PIN);

            sqlStatement.setInt(1, Integer.valueOf(ID.substring(1)));
            //sqlStatement.setInt(1, Integer.valueOf(ID));
            sqlStatement.setString(2, PIN);
            sqlStatement.execute();
            ResultSet result = sqlStatement.getResultSet();
            
            if (result.next()){
                customerObj = parseCustomer(result);
                return customerObj;
            }else {
                return null;
            }
        }
        
        catch (SQLException sqle)
        {
            System.out.println("Could not read the employee.");
            sqle.printStackTrace();
        }
        
        catch (IllegalAccessException ex) 
        {
            // should not go here
            System.err.println("There was a problem parsing the Employee");
            // Logger.getLogger(RDBEmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        catch (Exception e){
            System.out.println("Unhandled exception occurred");
            e.printStackTrace();
        }
        
        return customerObj;        
    }
}

