/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import data_access.orm.Account;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.PrePassivate;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import jpa.Homeloanaccounts;
import util.HomeLoanAccount;
import util.SavingsAccount;
import util.User;

/**
 *
 * @author Chris
 */
@Named(value = "webAccountsBean")
@ManagedBean
// available throughout the users session
@SessionScoped
public class WebAccountsBean implements Serializable {
    @Resource(name="jms/DBQueueFactory")
    private ConnectionFactory dbQueueFactory;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory clientQueueFactory;
    @Resource(name="jms/DBQueue")
    private Destination dbQueue;
    @Resource(name="jms/ClientQueue")
    private Destination clientQueue;
    // general form variables
    private String id, error;
    private boolean isAuth;
    private ArrayList<Account> sAccs;
    private ArrayList<Homeloanaccounts> hlAccs;
    
//    @EJB
//    private WebLoginBean loginBean;
    
    @EJB
    private CustomerSessionBeanRemote customerSessionBean;
    
    private static final Logger logger = Logger.getLogger(WebAccountsBean.class.getName());  
    
    /** GUI methods **/
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isIsAuth() {
        return isAuth;
    }
    
    public void setIsAuth(boolean isAuth) {
        this.isAuth = isAuth;
    }

    public ArrayList<Account> getsAccs() {
        return sAccs;
    }

    public void setsAccs(ArrayList<Account> sAccs) {
        this.sAccs = sAccs;
    }

    public ArrayList<Homeloanaccounts> getHlAccs() {
        return hlAccs;
    }

    public void setHlAccs(ArrayList<Homeloanaccounts> hlAccs) {
        this.hlAccs = hlAccs;
    }
    
    public void setID(String cid)
    {
        this.id = cid;
        
        sAccs = getSavingsAccounts();
        
        Collection<Homeloanaccounts> hlCol = customerSessionBean.getHomeLoanAccounts(this.id);
        
        hlAccs = new ArrayList<Homeloanaccounts>(hlCol);
        
        System.out.println("TEST: " + hlAccs.size() + " " + sAccs.size());
    }
    
    public ArrayList<String> getCustomerAccounts(String CID) {
        ArrayList<String> strList=new ArrayList<String>();
        JMSContext jmsContext=dbQueueFactory.createContext();
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();             
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("C:"+CID);
            producer.send(dbQueue,tm);
            JMSConsumer consumer = clientContext.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='C'){
                String[] strs=m.substring(2).split("||");
                strList.addAll(Arrays.asList(strs));
            }
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return strList;
    }
    
    public boolean repayment(String customerID, String accountID, String homeloanID, double amount) {
        boolean successful=false;
        JMSContext jmsContext=dbQueueFactory.createContext();
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();             
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("R:"+customerID+" "+accountID+" "+homeloanID+" "+amount);
            //messageProducer.send(tm);
            producer.send(dbQueue,tm);
            JMSConsumer consumer = clientContext.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            //System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='R' && m.charAt(2)=='T')
                successful=true;
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        }
        return successful;
    }
    
    /** Bean specific methods **/
    
    @PostConstruct
    public void initialize() 
    {
        sAccs = new ArrayList<Account>();
        hlAccs = new ArrayList<Homeloanaccounts>();
        
        // call session bean method to return 
//        sAccs = getSavingsAccounts();
//        
//        Collection<Homeloanaccounts> hlCol = customerSessionBean.getHomeLoanAccounts(loginBean.getId());
//        
//        hlAccs = new ArrayList<Homeloanaccounts>(hlCol);
//        
//        System.out.println("TEST: " + hlAccs.size() + " " + sAccs.size());
    }
    
    @PrePassivate
    @PreDestroy
    public void close()
    {
        
    }
    
        private ArrayList<Account> getSavingsAccounts()
    {
        JMSContext jmsContext=dbQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();            
        JMSConsumer consumer = jmsContext.createConsumer(clientQueue);
        
        try
        {            
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);
            tm.setText("C:"+this.id);
            producer.send(dbQueue,tm); 
            //TextMessage replyMessage = (TextMessage)consumer.receive();
            //ObjectMessage replyMessage = (ObjectMessage)consumer.receive();
            Message replyMessage = consumer.receive();
            if (replyMessage instanceof ObjectMessage)
            {
                //String m=replyMessage.getText();
                ArrayList<Account> accs = (ArrayList<Account>)((ObjectMessage)replyMessage).getObject();
                System.out.println("Receive num accounts: "+accs.size());
                return accs;
            } else
            {
                String m=((TextMessage)replyMessage).getText();
                return new ArrayList<Account>();
            }
        }
        
        catch(JMSException e1)
        {
            return null;
        }
        
    }
    
    /** GUI action methods **/
    
    public void validate(){
        
    }
    
    public void submit(){
        
    }
    
    
    
}
