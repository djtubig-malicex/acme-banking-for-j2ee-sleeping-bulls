/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import data_access.DataConstants;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.TextMessage;
import javax.xml.datatype.DatatypeConstants;
import jpa.Customerinfo;
import org.primefaces.event.FlowEvent;
import util.User;

/**
 *
 * @author Chris
 */
@Named(value = "webHomeLoanBean")
@ManagedBean
// available throughout the users session
@SessionScoped
public class WebHomeLoanBean implements Serializable {
    @Resource(name="jms/DBQueueFactory")
    private ConnectionFactory dbQueueFactory;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory clientQueueFactory;
    @Resource(name="jms/DBQueue")
    private Destination dbQueue;
    @Resource(name="jms/ClientQueue")
    private Destination clientQueue;
    
    @EJB
    private CustomerSessionBeanRemote customerSessionBean;
    
    // general form variables
    private String error;
    private boolean isAuth;
    private User user = new User();  
    private boolean skip;
    
    private static final Logger logger = Logger.getLogger(WebHomeLoanBean.class.getName());  
    
    /** GUI methods **/
    
    public User getUser() {  
        return user;  
    }  
  
    public void setUser(User user) {  
        this.user = user;
    }
    
    public boolean isSkip() {  
        return skip;  
    }  
  
    public void setSkip(boolean skip) {  
        this.skip = skip;  
    }  
      
    public String onFlowProcess(FlowEvent event) {  
        logger.info("Current wizard step:" + event.getOldStep());  
        logger.info("Next step:" + event.getNewStep());  
        
        if(skip) {  
            skip = false;   //reset in case user goes back  
            return "confirm";  
        }  
        else {  
            return event.getNewStep();  
        }  
    }  
    
    public boolean requestForHLAccountCreation(String CID) {
        boolean successful=false;
        JMSContext jmsContext=dbQueueFactory.createContext();
        JMSContext clientContext=clientQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();            
        try {
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("H:"+CID);
            producer.send(dbQueue,tm);
            JMSConsumer consumer = clientContext.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='H' && m.charAt(2)=='T')
            {
                successful=true;
            }
            
            if (!successful) FacesContext.getCurrentInstance().getExternalContext().redirect("noteligible.xhtml");
            
        } catch (JMSException ex) {
            System.err.println(ex.toString());
        } catch (IOException ex) {
            Logger.getLogger(WebHomeLoanBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return successful;
    }
    
    /** Bean specific methods **/
    
    @PostConstruct
    public void initialize() 
    {
        isAuth = false;
        error = "";
    }

    @PreDestroy
    public void close()
    {
        isAuth = false;
    }
    
    public void submit(String id)
    {
        if (user.acceptedTerms && user.loanAmount > 0)
        {
            // Save files
            user.setID(id);
            executePersist();
        } else
        {
            // TODO print an error on client page
        }
    }
    
    public String getError()
    {
        return this.error;
    }
    
    private void executePersist()
    {
        try {
            Customerinfo cust;
            cust = customerSessionBean.getCustomerInfo(user.getID());
            
            if (cust == null)
            {
                cust = new Customerinfo();
                cust.setCId(Integer.parseInt(user.getID().substring(1)));
            }
            
            // TODO alter original Customer table entry
            if (!user.companyAddress.isEmpty()) cust.setCompanyaddr(user.companyAddress);
            if (!user.companyName.isEmpty()) cust.setCompanyname(user.companyName);
            if (!user.email.isEmpty()) cust.setEmail(user.email);
            if (user.employmentStartDate != null) cust.setEmploymentdate(user.employmentStartDate);
            if (!user.empType.isEmpty()) cust.setEmploytype(user.empType.charAt(0));
            if (user.hoursPerWeek > 0) cust.setHoursperweek(user.hoursPerWeek);
            if (!user.jobTitle.isEmpty()) cust.setJobtitle(user.jobTitle);
            if (!user.phone.isEmpty()) cust.setPhone(user.phone);
            if (user.salary > 0) cust.setSalary((double)user.salary);
            if (!user.contactType.isEmpty()) cust.setPreferredcontact(user.contactType.charAt(0));
            
            System.out.println("TEST: " + cust.getEmail());
            
            if (//customerSessionBean.setCustomerInfo(cust) && 
                customerSessionBean.openNewHomeLoan(cust, user.loanAmount))
            {
                System.err.println("Passed, should redirect");
                //FacesContext.getCurrentInstance().getExternalContext().redirect("transactions.xhtml");
                FacesMessage msg = new FacesMessage("Successful", "Welcome :" + user.firstName);  
                FacesContext.getCurrentInstance().addMessage(null, msg);  
            } else
            {
                this.error = "There was a problem opening your new Home Loan account.";
                System.err.println(this.error);
                //FacesContext.getCurrentInstance().getExternalContext().redirect("noteligible.xhtml");
            }
            
        } catch (Exception ex) {
            Logger.getLogger(WebHomeLoanBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
