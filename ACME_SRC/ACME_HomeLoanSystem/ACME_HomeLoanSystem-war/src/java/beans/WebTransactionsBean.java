/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import data_access.orm.Account;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.PrePassivate;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import jpa.Homeloanaccounts;
import util.HomeLoanAccount;
import util.SavingsAccount;
import util.User;

/**
 *
 * @author Chris
 */
@Named(value = "webTransactionsBean")
@ManagedBean
// available throughout the users session
@SessionScoped
public class WebTransactionsBean implements Serializable {
    @Resource(name="jms/DBQueueFactory")
    private ConnectionFactory dbQueueFactory;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory clientQueueFactory;
    @Resource(name="jms/DBQueue")
    private Destination dbQueue;
    @Resource(name="jms/ClientQueue")
    private Destination clientQueue;
    
    // general form variables
    private String id, error;
    private boolean isAuth;
    private ArrayList<Account> sAccs;
    private ArrayList<Homeloanaccounts> hlAccs;
    private ArrayList<String> accList;
    private String toAccountID;
    private String fromAccountID;
    private double amount;
    
    private static final Logger logger = Logger.getLogger(WebAccountsBean.class.getName());  
    
    @EJB
    private CustomerSessionBeanRemote customerSessionBean;
    
    
    /** GUI methods **/
    public String getError() {
        return error;
    }
    
    public void setError(String error) {
        this.error = error;
    }
    
    public boolean isIsAuth() {
        return isAuth;
    }

    public void setIsAuth(boolean isAuth) {
        this.isAuth = isAuth;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public ArrayList<Account> getsAccs() {
        return sAccs;
    }

    public void setsAccs(ArrayList<Account> sAccs) {
        this.sAccs = sAccs;
    }

    
    public ArrayList<Homeloanaccounts> getHlAccs() {
        return hlAccs;
    }

    public void setHlAccs(ArrayList<Homeloanaccounts> hlAccs) {
        this.hlAccs = hlAccs;
    }

    public String getToAccountID() {
        return toAccountID;
    }

    public void setToAccountID(String toAccountID) {
        this.toAccountID = toAccountID;
    }

    public String getFromAccountID() {
        return fromAccountID;
    }

    public void setFromAccountID(String fromAccountID) {
        this.fromAccountID = fromAccountID;
    }
    
    public void setID(String CID)
    {
        this.id = CID;
        
        // hack
        sAccs = getSavingsAccounts();
        Collection<Homeloanaccounts> hlCol = customerSessionBean.getHomeLoanAccounts(this.id);
        hlAccs = new ArrayList<Homeloanaccounts>(hlCol);
        
        if (accList == null) accList = new ArrayList<String>();
        
        for(Account a : sAccs)
        {
            accList.add(String.format("Savings [%s] ($%.2f)", a.getID(), a.getBalance()));
        }
        
        for(Homeloanaccounts h : hlAccs)
        {
            accList.add(String.format("HomeLoan [H%04d] ($%.2f / $%.2f)", h.getHlAcc(), h.getAmountrepayed(), h.getAmountborrowed()));
        }
    }
    
    /** Bean specific methods **/
    
    private ArrayList<Account> getSavingsAccounts()
    {
        JMSContext jmsContext=dbQueueFactory.createContext();
        JMSProducer producer=jmsContext.createProducer();            
        JMSConsumer consumer = jmsContext.createConsumer(clientQueue);
        
        try
        {            
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);
            tm.setText("C:"+this.id);
            producer.send(dbQueue,tm); 
            //TextMessage replyMessage = (TextMessage)consumer.receive();
            //ObjectMessage replyMessage = (ObjectMessage)consumer.receive();
            Message replyMessage = consumer.receive();
            if (replyMessage instanceof ObjectMessage)
            {
                //String m=replyMessage.getText();
                ArrayList<Account> accs = (ArrayList<Account>)((ObjectMessage)replyMessage).getObject();
                System.out.println("Receive num accounts: "+accs.size());
                return accs;
            } else
            {
                String m=((TextMessage)replyMessage).getText();
                return new ArrayList<Account>();
            }
        }
        
        catch(JMSException e1)
        {
            return null;
        }
        
    }
    
    @PostConstruct
    public void initialize() 
    {
        //sAccs = new ArrayList<Account>();
        //hlAccs = new ArrayList<Homeloanaccounts>();
        
        // call session bean method to return 
        
        // -- moved to setID() - lifecycle hax - James
    }
    
    @PrePassivate
    @PreDestroy
    public void close()
    {
        // do any cleanup here
        
    }
    
    /** GUI action methods **/
    
    public void validate()
    {
        
        
    }
    
    public String submit() throws SystemException
    {
        // validate atomicity of transaction
        //boolean sufficientSavings = true;
        boolean homeloanpaid = false;
        
        // call method that makes changes in ACME_BankingSystem (but does not commit) - withdraws
        //sufficientSavings = isEnoughSavingsToPay();
        
        //if (!sufficientSavings){
            // display error message on page
            
        //    return "transactions";
        //}else {
        
        // call method to check if homeloan is paid off already and if not deduct amount using JPA
        homeloanpaid = isSelectedHomeLoanPaidOff();

        if (homeloanpaid){
            // display error message
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"You do not owe money on this account", "More info"));
            return "transactions";
        } else if(payDeposit())
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Deposit has been made!", "More info"));
            return "#";
        } else
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Insufficient funds", "More info"));
            return "#";
        }
        
    }

    private boolean isSelectedHomeLoanPaidOff() 
    {
        String hlAcc = this.getToAccountID().split(" ")[0];
        
        return customerSessionBean.isPaidOff(hlAcc);
        
    }
    
    public ArrayList<String> getAccList()
    {
        return accList;       
    }
    
    private boolean payDeposit() throws SystemException
    {
        String accNum = this.getFromAccountID().split(",")[0],
               hlAcc = this.getToAccountID().split(" ")[0];
        //EntityManagerFactory emf = Persistence.createEntityManagerFactory("ACME_HomeLoanSystem-ejbPU");
        //EntityManager em = emf.createEntityManager();
        //EntityTransaction tx = em.getTransaction();
        
        try
        {
            //tx.begin();
            
            // Perform withdraw on account (JMS)
            JMSContext jmsContext=dbQueueFactory.createContext();
            JMSProducer producer=jmsContext.createProducer();            
            JMSConsumer consumer = jmsContext.createConsumer(clientQueue);
                    
            TextMessage tm = jmsContext.createTextMessage();
            tm.setJMSReplyTo(clientQueue);
            tm.setText(String.format("R:%s %s %s %f", this.id, accNum, hlAcc, this.getAmount()));
            producer.send(dbQueue,tm); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            //ObjectMessage replyMessage = (ObjectMessage)consumer.receive();
            String m=replyMessage.getText();
            if (m.charAt(0)=='R' && m .charAt(2)=='T')
            {
                // Perform payment on homeloanaccount (EJB)
                customerSessionBean.repayMoney(this.id, accNum, hlAcc, this.getAmount());
                
                //tx.commit();
                // hack - refresh list
                //setID(this.id);
                
                return true;
            } else 
            {
                //tx.rollback();
                return false;
            }
            
        }
        
        catch (Exception e)
        {
            e.printStackTrace();
            //tx.rollback();
            return false;
        }
        
    }
        
}
