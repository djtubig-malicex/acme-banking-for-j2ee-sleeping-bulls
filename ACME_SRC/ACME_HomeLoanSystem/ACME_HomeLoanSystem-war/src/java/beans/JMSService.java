/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author Hubert
 */


@ManagedBean(name="webJMSService",eager=true)
// available throughout the users session
@SessionScoped
public class JMSService implements Serializable{        
    @Resource(name="jms/DBQueueFactory")
    private ConnectionFactory dbQueueFactory;
    @Resource(name="jms/ClientQueueFactory")
    private ConnectionFactory clientQueueFactory;
    @Resource(name="jms/DBQueue")
    private Destination dbQueue;
    @Resource(name="jms/ClientQueue")
    private Destination clientQueue;  
    
    private static Connection connection = null,clientConnection=null;
    private static Session session = null,clientSession=null;
    private static MessageProducer messageProducer=null;
    
    public JMSService(){
    }
    @PostConstruct
    private void initialize() 
    {
        try {
            connection=dbQueueFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            clientConnection=clientQueueFactory.createConnection();
            clientConnection.start();
            clientSession = clientConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);   
            messageProducer = session.createProducer(dbQueue);    
        } catch (JMSException ex) {
            Logger.getLogger(JMSService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @PreDestroy
    private void close()
    {
        try {
            session.close();
            clientSession.close();
            connection.close();
            clientConnection.close();
        } catch (JMSException ex) {
            Logger.getLogger(JMSService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public boolean jmsLogin(String CID, String pw){
        boolean loginSeccessful=false;
        try{            
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);
            tm.setText("L:"+CID+" "+pw);
            messageProducer.send(tm);
            
            MessageConsumer consumer = clientSession.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'");   
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='L' && m.charAt(2)=='T'){
                loginSeccessful=true;
            }
        }catch(JMSException e1){
        }
        return loginSeccessful;
    }
    public boolean login(String CID, String pw){
        boolean loginSeccessful=false;
        try{
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);
            tm.setText("L:"+CID+" "+pw);
            messageProducer.send(tm);
            
            MessageConsumer consumer = clientSession.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'");  
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='L' && m.charAt(2)=='T'){
                loginSeccessful=true;
            }
        }catch(JMSException e1){
        }
        return loginSeccessful;
    }
    
    public ArrayList<String> getCustomerAccounts(String CID) {
        ArrayList<String> strList=new ArrayList<String>();
        try {
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("C:"+CID);
            messageProducer.send(tm);
            MessageConsumer consumer = clientSession.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='C'){
                String[] strs=m.substring(2).split("||");
                for (String str : strs){
                    strList.add(str);
                }
            }
        } catch (JMSException ex) {
            Logger.getLogger(JMSService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return strList;
    }

    public boolean requestForHLAccountCreation(String CID) {
        boolean Successful=false;
        try {
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("H:"+CID);
            messageProducer.send(tm);
            MessageConsumer consumer = clientSession.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='H' && m.charAt(2)=='T')
                Successful=true;
        } catch (JMSException ex) {
            Logger.getLogger(JMSService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Successful;
    }

    public boolean repayment(String customerID, String accountID, String homeloanID, double amount) {
        boolean Successful=false;
        try {
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("R:"+customerID+" "+accountID+" "+homeloanID+" "+amount);
            messageProducer.send(tm);
            MessageConsumer consumer = clientSession.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='R' && m.charAt(2)=='T')
                Successful=true;
        } catch (JMSException ex) {
            Logger.getLogger(JMSService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Successful;
    }

    public double totalSavingsAccountsAmount() {
        double amount=0;
        try {
            TextMessage tm = session.createTextMessage();
            tm.setJMSReplyTo(clientQueue);     
            // create  and send the JMS message
            tm.setText("I:investor data");
            messageProducer.send(tm);
            MessageConsumer consumer = clientSession.createConsumer(clientQueue,"JMSCorrelationID='"+tm.getJMSMessageID()+"'"); 
            TextMessage replyMessage = (TextMessage)consumer.receive();
            String m=replyMessage.getText();
            System.out.println("Receive Feedback: "+m);
            if (m.charAt(0)=='I')
                amount=Double.parseDouble(m.substring(2));
        } catch (JMSException ex) {
            Logger.getLogger(JMSService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return amount;
    }
}
