
package jpa.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import jpa.Customerinfo;
import jpa.Homeloanaccounts;
import jpa.Homeloantransactions;

/**
 *
 * @author JamesAlan
 */
public class HomeLoanTransactionDAO
{
    private EntityManagerFactory emf;
    
    public HomeLoanTransactionDAO()
    {
        emf = Persistence.createEntityManagerFactory("ACME_HomeLoanSystem-ejbPU");
    }
    
    public boolean persist(Homeloantransactions newTrans)
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try
        {
            em.persist(newTrans);
            em.getTransaction().commit();
        } catch (Exception e)
        {
            em.getTransaction().rollback();
            System.out.println("Not persisted");
            return false;
        } finally
        {
            em.close();
        }
        
        System.out.println("persisted");
        return true;
    }

    public Homeloantransactions buildHLTransaction(int accNum,  Homeloanaccounts hl, double paid)
    {
        Homeloantransactions newHLTrans = new Homeloantransactions();
        
        newHLTrans.setFromaccnum(accNum);
        newHLTrans.setTohlAcc(hl);
        newHLTrans.setTransdate(new Date(System.currentTimeMillis()));
        newHLTrans.setAmount(paid);
        
        return newHLTrans;
    }
    /*
    public Homeloanaccounts getHomeLoanAccount(int HL_Acc)
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Homeloanaccounts hlacc = em.find(Homeloanaccounts.class, HL_Acc);
        
        em.close(); 
        
//        return (cust != null) ?
//            String.format("%d\t%s\t%s\n",
//                cust.getCId(),
//                cust.getEmail(),
//                cust.getPreferredcontact()
//            ) :
//            "-- not found --";
//      
        return hlacc;
    }
    */
    
    public Collection<Homeloantransactions> getTransactionList(int HL_Acc)
    {
        Collection<Homeloantransactions> transCollection;
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Query q = em.createNativeQuery("SELECT * FROM ACME.HOMELOANTRANSACTIONS WHERE TOHL_ACC="+HL_Acc);
        transCollection = q.getResultList();
        
        em.close(); 
        
        return transCollection;
    }
}
