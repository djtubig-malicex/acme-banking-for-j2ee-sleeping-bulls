/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import com.sun.xml.ws.tx.at.tube.TransactionalAttribute;
import data_access.orm.Account;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.persistence.PersistenceContext;

import jpa.Customerinfo;
import jpa.Homeloanaccounts;
import jpa.Homeloantransactions;
import jpa.controller.CustomerInfoDAO;
import jpa.controller.HomeLoanDAO;
import jpa.controller.HomeLoanTransactionDAO;

/**
 *
 * @author JamesAlan
 */
@Singleton
@PersistenceContext
//@TransactionManagement(javax.ejb.TransactionManagementType.CONTAINER)  
public class HomeLoanPersistenceBean implements HomeLoanPersistenceBeanRemote
{   
//    @PersistenceUnit(unitName="ACME_HomeLoanSystem-ejbPU")
//    private EntityManagerFactory emf;
//    
//    @Resource
//    private UserTransaction utx; 
    
    @PostConstruct
    public void initialize() 
    {        
        //emf = Persistence.createEntityManagerFactory("ACME_HomeLoanSystem-ejbPU");
    }
    
    @PreDestroy
    public void close()
    {
        // ??????
    }
    
    //@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED) 
    @Override
    public boolean createCustomerInfo(int C_ID, String phone, String email, String companyName, String companyAddr,
            char employType, int hoursPerWeek, String jobTitle, double salary, Date employmentDate, 
            char preferredContact)
    {
        // New Customerinfo row - assume C_ID exists/correpsonds to first database
        Customerinfo newCust = new Customerinfo(C_ID);
        
        // Set fields as necessary
        newCust.setPhone(phone);
        newCust.setEmail(email);
        newCust.setCompanyname(companyName);
        newCust.setCompanyaddr(companyAddr);
        newCust.setEmploytype(employType);
        newCust.setHoursperweek(hoursPerWeek);
        newCust.setEmploymentdate(employmentDate);
        newCust.setJobtitle(jobTitle);
        newCust.setSalary(salary);
        newCust.setPreferredcontact(preferredContact);
        
        // Persist
//        CustomerinfoJpaController newCustControl = new CustomerinfoJpaController(utx, emf);
//        try {
//            newCustControl.create(newCust);
//        } catch (PreexistingEntityException ex) {
//            Logger.getLogger(HomeLoanPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        } catch (RollbackFailureException ex) {
//            Logger.getLogger(HomeLoanPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        } catch (Exception ex) {
//            Logger.getLogger(HomeLoanPersistenceBean.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        }
        CustomerInfoDAO custInfCont = new CustomerInfoDAO();
        return custInfCont.persist(newCust);
        
        
        //return true;
    }
    
    @Override
    public boolean createHomeLoanAccount(Customerinfo cust, double amtBorrow)
    {
        HomeLoanDAO dao = new HomeLoanDAO();
        return dao.persist(dao.buildHomeLoanAccount(cust, amtBorrow));
    }
    
    @Override
    public boolean makeRepayment(int HL_Acc, double amt, int accNum)
    {
        return false;
    }
    
    @Override
    public Homeloanaccounts getHomeLoan(int HL_Acc)
    {
        //TODO
        return null;
    }

    public Customerinfo getCustomerInfo(int C_ID)
    {
        CustomerInfoDAO custInfCont = new CustomerInfoDAO();
        return custInfCont.getCustomerInfo(C_ID);
    }

    @Override
    public boolean setCustomerInfo(Customerinfo cust) 
    {
        CustomerInfoDAO custInfCont = new CustomerInfoDAO();
        if (getCustomerInfo(cust.getCId()) != null)
        {
            custInfCont.update(cust);
            return true;
        }
        else
            return custInfCont.persist(cust);
    }

    @Override
    public Collection<Homeloanaccounts> getHomeLoanAccounts(String C_ID)
    {
        //return this.getCustomerInfo(Integer.parseInt(C_ID.substring(1))).getHomeloanaccountsCollection();
        CustomerInfoDAO custInfCont = new CustomerInfoDAO();
        System.out.println("TEST: " + C_ID);
        
        Collection<Homeloanaccounts> accs = custInfCont.getCustomerInfo(Integer.parseInt(C_ID.substring(1))).getHomeloanaccountsCollection();
        accs.size();
        
        return accs;
    }

    @Override
    public boolean checkHomeLoanPaidOff(int hlAcc) {
        HomeLoanDAO dao = new HomeLoanDAO();
        
        Homeloanaccounts h = dao.getHomeLoanAccount(hlAcc);
        
        return h.isPaidOff();
        
        
    }

    @Override
    public void repayMoney(int CID, int accNum, int HL_Acc, double amount) 
    {
        //Deduct money
        HomeLoanDAO loanDao = new HomeLoanDAO();
        Homeloanaccounts h = loanDao.getHomeLoanAccount(HL_Acc);
        
        h.setAmountrepayed(h.getAmountrepayed() + amount);
        
        //Add transaction
        HomeLoanTransactionDAO transDAO = new HomeLoanTransactionDAO();
        
        CustomerInfoDAO custDAO = new CustomerInfoDAO();
        Customerinfo c = custDAO.getCustomerInfo(CID);
        
        Homeloantransactions trans = transDAO.buildHLTransaction(accNum, h, amount);
        h.getHomeloantransactionsCollection().add(trans);
        //transDAO.persist(trans);
        loanDao.update(h);
        
    }
    
}
